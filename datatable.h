/****************************************************************************
** Form interface generated from reading ui file 'aboutform.ui'
**
** Created: Wed May 25 11:16:53 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
****************************************************************************/

#ifndef DATATABLE_H
#define DATATABLE_H

#include <QVariant>
#include <QMainWindow>
#include "ui_datatable.h"
#include "mainform.h"
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QMessageBox>
//#include <iostream.h>


class DataTable : public QMainWindow, public Ui::DataTable
{
    Q_OBJECT

public:
    DataTable( MainForm* parent = 0, Qt::WindowFlags fl = Qt::Window );
    ~DataTable();

    QMenuBar *menubar;
    QMenu *fileMenu;
    QMenu *editMenu;
    QToolBar *toolBar;
    QAction* fileExitAction;
    QAction* editCopyAction;
    QAction* editPasteAction;


protected:
   MainForm * p;
   void closeEvent ( QCloseEvent * );
   
public slots:
  void on_okButton_clicked();
  void on_cancelButton_clicked();

  void editCopy();
  void editPaste();
  void on_dataTable_customContextMenuRequested();
  void on_dataTable_itemSelectionChanged();
};

#endif // ABOUTFORM_H
