
#include "deconvform.h"
#include "base.h"

#include <QVariant>
#include <QMessageBox>
#include <QDebug>

int method=LG_SUM;

LineGenerator::LineGenerator()
{
//	method=LG_SUM;
}

void LineGenerator::calculate(vector<double>& dest, vector<double> pars)
{
   size_t NLines=(pars.size()-2)/4;
   for (size_t i=dest.size(); i--;)
		dest[i]=0.0; //clean destination
   for (size_t i=0; i<NLines; i++) {
	double pos=pars[4*i];
	double ampl=pars[4*i+1];
	double width=pars[4*i+2];
	double lorgaus=pars[4*i+3];
	vector<double> line=create_lineshape(pos, ampl, width, lorgaus, xvector);
	for (size_t j=dest.size(); j--;)
		dest[j]+=line[j];
	}
//linear baseline correction
        double shift=pars[pars.size()-2];
	double slope=pars[pars.size()-1];
	vector<double> bl=create_baseline(shift,slope, xvector);
	for (size_t i=0; i<dest.size(); i++)
		dest[i]+=bl[i];
//	cout<<"max:"<<max(dest)<<endl;
}


vector<double> create_lineshape_sum (double pos, double ampl, double width, double lorgaus, vector<double> xvector) //create  a shape for single line (summation)
{
	vector<double> y;
//	if (!negint)
//		ampl=fabs(ampl);
//	width=fabs(width);
//	double lg=fabs(lorgaus);
//	lorgaus=lg;
//	if (lorgaus>1.0) {
//			cout<<"LG is "<<lorgaus<<endl;
//		lg=lorgaus-int(lorgaus);
//		lorgaus=fabs(lg);
//	}
	size_t pts=xvector.size();
// Special case of zero LW
	if (width==0.0) {
		for (size_t i=0; i<xvector.size(); i++)
			y.push_back(0.0);
		size_t imin=0;
		double dif=9e36;
		for (size_t i=xvector.size()-1; i--;) {
			if (fabs(xvector[i]-pos)<dif) {
				dif=fabs(xvector[i]-pos);
				imin=i;
			}
		}
		y[imin]=ampl;
		return y;
	}
	for (size_t i=0; i<pts; i++) {
		double delx=xvector[i]-pos;
		double gau=exp(-(delx/(0.600561*width))*(delx/(0.600561*width)));
		double lor=1+(delx/(0.5*width))*(delx/(0.5*width));
		y.push_back(ampl*(lorgaus/lor+(1-lorgaus)*gau));
	}
	return y;
}

double max(vector<double> x)
{
	double mx=-9e36;
	for (size_t i=0; i<x.size(); i++)
		if (x[i]>mx)
			mx=x[i];
	return mx;
}


double deconvFcn::operator()(const std::vector<double>& par) const 
{
//	deconvDialog->update_temp_lines(&par);
	std::vector<double> calcpoints;
	for (size_t i=0; i<theXvec.size();i++)
		calcpoints.push_back(0.0);
	vector<double> xvec;
	for (size_t i=0; i<theXvec.size(); i++)
		xvec.push_back(theXvec[i]);
	gen->setXvector(xvec);
	gen->calculate(calcpoints, par);
	double chi2 = 0.0;
	qApp->processEvents();
	for (size_t i=0; i<theMeasurements.size(); i++)
		chi2 +=(calcpoints[i]-theMeasurements[i])*(calcpoints[i]-theMeasurements[i])/(theError*theError);
	deconvDialog->chi2Label->setText(QString("Ch^2: %1").arg(chi2,0,'e',5));
	return chi2;
}

double sum(vector<double>& list)
{
	size_t n=list.size();
	double s=0;
	for (size_t i=1; i<n; i++)
		s+=list[i];
	return s;
}

 //Lineshape convolution
vector<double> create_lineshape_conv (double pos, double ampl, double width, double lorgaus, vector<double> xvector)
{
	size_t n=xvector.size();
	if (lorgaus>1.0) {
		double lg=lorgaus-int(lorgaus);
		lorgaus=fabs(lg);
	}
	vector<double> lor=create_lineshape_sum(pos, 1.0, lorgaus*width, 1.0, xvector);
	vector<double> gau;
	vector<double> product(n,0.0);
	for (int j=0; j<n; j++){
		gau=create_lineshape_sum(xvector[j], 1.0, (1-lorgaus)*width, 0.0, xvector);
		for (int i=0; i<n; i++) {
				product[j]+=lor[i]*gau[i];
		}
	}
	double sc=ampl/max(product);

	for (size_t j=0; j<n; j++)
		product[j]*=sc;
	return product;
}

vector<double> create_lineshape(double pos, double ampl, double width, double lorgaus, vector<double> xvector)
{
	vector<double> res;
	if (method==LG_SUM) 
		res=create_lineshape_sum(pos,ampl,width,lorgaus, xvector);
	else {
		res=create_lineshape_conv(pos,ampl,width,lorgaus, xvector);
	}
	return res;
}

vector<double> create_baseline(double offset, double slope, vector<double> xvector)
{	
	vector<double> baseline(xvector.size());
	for (size_t i=xvector.size(); i--;) {
		baseline[i]=xvector[i]*slope+offset;
	}
	return baseline;
}

DeconvForm::DeconvForm(MainForm *parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {
    setupUi(this);
    p=parent;
    data=p->data;
    setWindowTitle( "Deconvolution" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );

 editCopyShortcut = new QShortcut(QKeySequence(tr("Ctrl+C", "Edit|Copy")),this);
 editPasteShortcut = new QShortcut(QKeySequence(tr("Ctrl+V", "Edit|Paste")),this);


    connect( editCopyShortcut, SIGNAL( activated() ), this, SLOT( editCopy() ) );
    connect( editPasteShortcut, SIGNAL( activated() ), this, SLOT( editPaste() ) );
    connect( lgComboBox, SIGNAL(currentIndexChanged(int) ), this, SLOT( changeMixing(int) ));
    connect( linesTable, SIGNAL(itemChanged (QTableWidgetItem*)), this, SLOT( update_temp_lines()));
    connect(p->activePlot(), SIGNAL(leftbarSet()), this, SLOT(guessRightbarPos()));
	QStringList headers; //Table headers
        linesTable->setColumnCount(6);
        headers<<"name"<<"position"<<"amplitude"<<"width"<<"Lor/Gaus"<<"Integral";
	linesTable->setHorizontalHeaderLabels ( headers );
	headers.clear();
	headers<<"offset"<<"slope"; //Baseline table headers
	baselineTable->setHorizontalHeaderLabels ( headers );

	//inserting default values
	QTableWidgetItem* newItem = new QTableWidgetItem(QString("0.0"));
	newItem->setFlags(newItem->flags() | Qt::ItemIsUserCheckable );
	newItem->setCheckState(Qt::Checked);
	QTableWidgetItem* newItem2=new QTableWidgetItem(QString("0.0"));
	newItem2->setFlags(newItem->flags() | Qt::ItemIsUserCheckable );
	newItem2->setCheckState(Qt::Checked);
        baselineTable->setItem(0, 0, newItem);
	baselineTable->setItem(0, 1, newItem2);
	connect( baselineTable, SIGNAL(itemChanged (QTableWidgetItem*)), this, SLOT( update_temp_lines()));

//	connect( updateButton, SIGNAL( clicked() ), this, SLOT( update_temp_lines() ) );
	
//	model= new LineGenerator();
	readSettings();

	delButton->setEnabled(false);
	startButton->setEnabled(false);

//      temporary disabled feature
//	lgGroupBox->setVisible(false);
//	windowCheckBox->setVisible(false);
}

/*
 *  Destroys the object and frees any allocated resources
 */
DeconvForm::~DeconvForm()
{
	delete editCopyShortcut;
	delete editPasteShortcut;
	writeSettings();
    // no need to delete child widgets, Qt does it all for us
}

void DeconvForm::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("DeconvForm");
        resize(settings.value("size", QSize(800, 200)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
	methodComboBox->setCurrentIndex(settings.value("algorithm",0).toInt());
	initCheckBox->setChecked(settings.value("reuse",false).toBool());
        settings.endGroup();
}

void DeconvForm::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("DeconvForm");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
	settings.setValue("algorithm", methodComboBox->currentIndex());
	settings.setValue("reuse", initCheckBox->isChecked());
        settings.endGroup();
}

void DeconvForm::changeMixing(int i) {
	if (i>1 || i<0)
		return;
	method=i;
	update_temp_lines();
}

void DeconvForm::on_addButton_clicked() //not really optimised, adds lines from interactive input and shows it in plot
{
        disconnect( linesTable, SIGNAL(itemChanged (QTableWidgetItem*)), 0,0);
	if (data->get_odata(data->getActiveCurve())->rows()>1) {
		QMessageBox::warning(this, "Deconvolution error", "Deconvolution can't be applied on 2D dataset directly.\n Please, take any row first");
		return;
	}		
	if (!p->plot2D->is_leftbar || !p->plot2D->is_rightbar) {
		QMessageBox::warning(this, "Line creation error", "Please, set markers first.\nMain marker should be at the top of the line and secondary marker should specify the linewidth");
		return;
	}
	int Nlines=linesTable->rowCount(); //total number of lines in model
	linesTable->setRowCount(Nlines+1); //add one line
	QString name=(QString("line%1").arg(Nlines+1)); //create a name in form "line1", "line2", etc)
	QTableWidgetItem* newItem = new QTableWidgetItem(name);
        linesTable->setItem(Nlines, 0, newItem);
//	add position
	newItem = new QTableWidgetItem(QString("%1").arg(p->plot2D->leftbarpos.x()));
	newItem->setFlags(newItem->flags() | Qt::ItemIsUserCheckable );
	newItem->setCheckState(Qt::Checked);
	linesTable->setItem(Nlines, 1, newItem);
//	add intensity
	newItem = new QTableWidgetItem(QString("%1").arg(p->plot2D->leftbarpos.y()));
	newItem->setFlags(newItem->flags() | Qt::ItemIsUserCheckable );
	newItem->setCheckState(Qt::Checked);
	linesTable->setItem(Nlines, 2, newItem);
//	add width
	newItem = new QTableWidgetItem(QString("%1").arg(2.0*fabs(p->plot2D->leftbarpos.x()-p->plot2D->rightbarpos.x())));
	newItem->setFlags(newItem->flags() | Qt::ItemIsUserCheckable );
	newItem->setCheckState(Qt::Checked);
	linesTable->setItem(Nlines, 3, newItem);
//	add Lor/Gaus ratio in %
	newItem = new QTableWidgetItem(QString("%1").arg(1.0));
	newItem->setFlags(newItem->flags() | Qt::ItemIsUserCheckable );
	newItem->setCheckState(Qt::Unchecked);
	linesTable->setItem(Nlines, 4, newItem);
//      add Integrals (for viewing)
        newItem = new QTableWidgetItem(QString("%1").arg(1.0));
        newItem->setFlags(newItem->flags() & Qt::ItemIsEditable | Qt::ItemIsSelectable );
        linesTable->setItem(Nlines, 5, newItem);

	update_temp_lines();
	connect( linesTable, SIGNAL(itemChanged (QTableWidgetItem*)), this, SLOT( update_temp_lines()));

	delButton->setEnabled(true);
	startButton->setEnabled(true);
}

void DeconvForm::on_delButton_clicked()
{
	linesTable->removeRow(linesTable->currentRow());
	update_temp_lines();
}

void DeconvForm::closeEvent(QCloseEvent *)
{
	disconnect(p->activePlot(), SIGNAL(leftbarSet()), this, SLOT(guessRightbarPos()));
	delete_temp_lines();
} 

void DeconvForm::wheelEvent(QWheelEvent * e)
{
    bool ok;
    double value;
    int col;
    QTableWidgetItem* item;
    if (linesTable->hasFocus()){
        item = linesTable->currentItem();
        value= item->text().toDouble(&ok);
        if(!ok) return; //do nothing
        col = linesTable->currentColumn();
        if (col == 1) { //shift
            value += fabs(p->plot2D->xppt)*e->delta()/120;
        }
        else if (col == 2) { //amplitude
            value += fabs(p->plot2D->yppt)*e->delta()/120;
        }
        else if (col == 3) { //width
            value += fabs(p->plot2D->xppt)*0.1*e->delta()/120;
            if (value < 0.0)
                value=0.0;
        }
        else if (col == 4) { //lor/gauss
            value += 0.01*e->delta()/120;
            if (value < 0.0)
                value=0.0;
            if (value>1.0)
                value=1.0;
        }
        item->setText(QString("%1").arg(value));
    }
    if (baselineTable->hasFocus()) {
        item = baselineTable->currentItem();
        value= item->text().toDouble(&ok);
        if(!ok) return; //do nothing
        col = baselineTable->currentColumn();
        if (col == 0) { //offset
           value += fabs(p->plot2D->yppt)*e->delta()/120;
        }
        else if (col == 1){ //slope
           value += fabs(p->plot2D->yppt)/fabs(p->plot2D->xrange)*e->delta()/120;
        }
        item->setText(QString("%1").arg(value));
    }
}

void DeconvForm::on_closeButton_clicked()
{
	close();
}

void DeconvForm::update_temp_lines(const vector<double>* defpar)
{
        //disconnect( linesTable, SIGNAL(itemChanged (QTableWidgetItem*)), 0,0);
        delete_temp_lines();
	vector<double> pars;
	vector<size_t> which;
	QStringList names;
	if (!defpar)
		read_parameters(names, pars, which);
	else
		pars=*defpar;
	vector<double> empty_errs;
	for (size_t i=0; i<pars.size(); i++)
		empty_errs.push_back(0.0); //creates zero list width the same lenght as pars
	QList<data_entry> temp_lines;
        accumResults(data->getActiveCurve(), 0, temp_lines, pars, empty_errs, 0, names);
	for (size_t i=0; i<temp_lines.size(); i++)
		temp_lines[i].parent.isTempDeconv=true;

	showResults(data->getActiveCurve(), temp_lines);
       // connect( linesTable, SIGNAL(itemChanged (QTableWidgetItem*)), 0,0);
}

void DeconvForm::read_parameters(QStringList& names, vector<double>& paras, vector<size_t>& which)
{
	int NLines=linesTable->rowCount();
	for (size_t i=0; i<NLines; i++) {
		names<<linesTable->item(i,0)->text();
		for (size_t j=1; j<5; j++) {
			paras.push_back(linesTable->item(i,j)->text().toDouble());
			if (linesTable->item(i,j)->checkState()==Qt::Checked) which.push_back(i*4+j-1);
		}
	}
	paras.push_back(baselineTable->item(0,0)->text().toDouble()); //add baseline offset
	paras.push_back(baselineTable->item(0,1)->text().toDouble()); //add baseline slope

	if (baselineTable->item(0,0)->checkState()==Qt::Checked) which.push_back(paras.size()-2);
	if (baselineTable->item(0,1)->checkState()==Qt::Checked) which.push_back(paras.size()-1);
}

void DeconvForm::create_parameters(QStringList& names, MnUserParameters& upar, const vector<double> err)
{
	int NLines=linesTable->rowCount();
	for (size_t i=0; i<NLines; i++) {
		names<<linesTable->item(i,0)->text();
		QString s=QString(linesTable->item(i,0)->text()+"pos");
#ifdef USE_OLD_MINUIT
		upar.add(s.toAscii().data(),linesTable->item(i,1)->text().toDouble(),err[0]);
		if (linesTable->item(i,1)->checkState()==Qt::Unchecked)
			upar.fix(s.toAscii().data());
		s=QString(linesTable->item(i,0)->text()+"ampl");
		upar.add(s.toAscii().data(),linesTable->item(i,2)->text().toDouble(),err[1]);
		if (!negativeIntensitiesBox->isChecked())
			upar.setLowerLimit(s.toAscii().data(),0.0);

		if (linesTable->item(i,2)->checkState()==Qt::Unchecked)
			upar.fix(s.toAscii().data());
		s=QString(linesTable->item(i,0)->text()+"width");
		upar.add(s.toAscii().data(),linesTable->item(i,3)->text().toDouble(),err[2]);
		upar.setLowerLimit(s.toAscii().data(),0.0);
		if (linesTable->item(i,3)->checkState()==Qt::Unchecked)
			upar.fix(s.toAscii().data());
		s=QString(linesTable->item(i,0)->text()+"lg");
		upar.add(s.toAscii().data(),linesTable->item(i,4)->text().toDouble(),err[3],0.0,1.0);
		if (linesTable->item(i,4)->checkState()==Qt::Unchecked)
			upar.fix(s.toAscii().data());
	}
		upar.add("bl_offset",baselineTable->item(0,0)->text().toDouble(), err[4]);
		if (baselineTable->item(0,0)->checkState()==Qt::Unchecked)
			upar.fix("bl_offset");
		upar.add("bl_slope",baselineTable->item(0,1)->text().toDouble(), err[5]);
		if (baselineTable->item(0,1)->checkState()==Qt::Unchecked)
			upar.fix("bl_slope");
#else
        upar.Add(s.toLatin1().data(),linesTable->item(i,1)->text().toDouble(),err[0]);
		if (linesTable->item(i,1)->checkState()==Qt::Unchecked)
            upar.Fix(s.toLatin1().data());
		s=QString(linesTable->item(i,0)->text()+"ampl");
        upar.Add(s.toLatin1().data(),linesTable->item(i,2)->text().toDouble(),err[1]);
		if (!negativeIntensitiesBox->isChecked())
            upar.SetLowerLimit(s.toLatin1().data(),0.0);

		if (linesTable->item(i,2)->checkState()==Qt::Unchecked)
            upar.Fix(s.toLatin1().data());
		s=QString(linesTable->item(i,0)->text()+"width");
        upar.Add(s.toLatin1().data(),linesTable->item(i,3)->text().toDouble(),err[2]);
        upar.SetLowerLimit(s.toLatin1().data(),0.0);
		if (linesTable->item(i,3)->checkState()==Qt::Unchecked)
            upar.Fix(s.toLatin1().data());
		s=QString(linesTable->item(i,0)->text()+"lg");
        upar.Add(s.toLatin1().data(),linesTable->item(i,4)->text().toDouble(),err[3],0.0,1.0);
		if (linesTable->item(i,4)->checkState()==Qt::Unchecked)
            upar.Fix(s.toLatin1().data());
	}
		upar.Add("bl_offset",baselineTable->item(0,0)->text().toDouble(), err[4]);
		if (baselineTable->item(0,0)->checkState()==Qt::Unchecked)
			upar.Fix("bl_offset");
		upar.Add("bl_slope",baselineTable->item(0,1)->text().toDouble(), err[5]);
		if (baselineTable->item(0,1)->checkState()==Qt::Unchecked)
			upar.Fix("bl_slope");
#endif
}

void DeconvForm::on_startButton_clicked()
{
try{
        disconnect( linesTable, SIGNAL(itemChanged (QTableWidgetItem*)), 0,0);
        disconnect( baselineTable, SIGNAL(itemChanged (QTableWidgetItem*)), 0,0);
	delete_temp_lines(); 
	int k=data->getActiveCurve(); //store index of data should be fitted
	if (data->getParent(k).hasParent)
	if (data->getParent(k).isRow) {
		int do2D=QMessageBox::question ( this, QString("Select deconvolution type"), QString("Apply for all rows in corresponding 2D spectra ?"), QMessageBox::Yes, QMessageBox::No, QMessageBox::NoButton );
		if (do2D==QMessageBox::Yes) k=data->getParent(k).parentIndex;
	}
	QStringList names;
	vector<double> x=get_xvector(k);	

//start actual optimisation
	size_t ni=data->get_odata(k)->rows();
	if (ni<1) ni=1;
	QList<data_entry> deconv_results;
	QList<Array_> lines_arrays;
	MnUserParameters upar;
	vector<double> data;
	vector<double> err;

	QProgressDialog progress("Fitting...", "Abort", 0, ni, p);
	for (size_t i=0; i<ni; i++) {  //main optimisation
	        progress.setValue(i);
        	qApp->processEvents();
            	if (progress.wasCanceled())
                	break;
		data.clear();
		data=get_yvector(k,i);

		const double maxint=max(data);
		double noise=0.02*maxint;

		err.clear();
		//position error
		err.push_back(fabs(x[0]-x[x.size()-1])*0.001);
 		//amplitude error
		err.push_back(maxint*0.01);
		//width error
		err.push_back(fabs(x[0]-x[x.size()-1])*0.001);
		//lor/gaus error
		err.push_back(0.01);
		//offset error
		err.push_back(maxint*0.001); //offset error
		//slope error
		err.push_back(fabs(x[0]-x[x.size()-1]*0.001)); //slope error

		if (!i) //just for the first pass read parameters from the table
			create_parameters(names,upar,err);
		deconvFcn theFCN(data,x,noise);
		theFCN.deconvDialog=this;
//		theFCN.negint=negativeIntensitiesBox->isChecked();

		MnApplication* alg;
		int userSelectedMethod=methodComboBox->currentIndex();

		switch (userSelectedMethod) {
		case 0: //MIGRAD method
			alg=new MnMigrad(theFCN, upar, MnStrategy(1));
			break;
		case 1:
			alg=new MnSimplex(theFCN, upar, MnStrategy(1));
			break;
		case 2:
			alg=new MnMinimize(theFCN, upar, MnStrategy(1));
			break;
		}

		FunctionMinimum min=(*alg)();
		delete alg;
#ifdef USE_OLD_MINUIT
		if (!min.isValid()) {
#else
		if (!min.IsValid()) {
#endif
			QMessageBox::warning(this, "Optimisation failed", QString("Fitting at row %1 didn't converge properly").arg(i+1));
		}
#ifdef USE_OLD_MINUIT
			double ksi=min.fval();
			cout<<"Chi2="<<ksi<<endl;
			vector<double> pars=min.userParameters().params();
			vector<double> sigmas=min.userParameters().errors();
			if (!initCheckBox->isChecked()) 
				upar=min.userParameters(); //use optimised parameters for the next row
#else
			double ksi=min.Fval();
//			cout<<"Chi2="<<ksi<<endl;
			vector<double> pars=min.UserParameters().Params();
			vector<double> sigmas=min.UserParameters().Errors();
			if (!initCheckBox->isChecked()) 
				upar=min.UserParameters(); //use optimised parameters for the next row
#endif
	
		//here should be saving data
			accumResults(k, i, deconv_results, pars, sigmas, ksi, names);
			vector<double> newpars=convert_pars(pars);
			rewrite_linestable(newpars);


	}
	progress.setValue(ni);
	showResults(k, deconv_results);
	p->plot2D->update();
	p->plot3D->cold_restart(p->data);

	//here should be a procudure to create visual results
        connect( linesTable, SIGNAL(itemChanged (QTableWidgetItem*)), this, SLOT(update_temp_lines()));
	connect( baselineTable, SIGNAL(itemChanged (QTableWidgetItem*)), this, SLOT( update_temp_lines()));
}
catch (MatrixException exc) {
	QMessageBox::critical(this, "Deconvolution error", exc.what());
}
}

void DeconvForm::accumResults(size_t k, size_t nrow, QList<data_entry>& results, vector<double> pars, vector<double> errs, double chi_val, QStringList names)
{
	size_t NLines=names.size();//(pars.size()-2)/4;

	if (results.size()!=NLines+1) { //initialisation

		size_t ni=data->get_odata(k)->rows();
		if (ni<1) 
			ni=1;
		for (size_t q=0; q<NLines+1; q++) {
			data_entry x;
			results.push_back(x);
		}

		for (size_t i=0; i<NLines; i++) {
			results[i].info  =*(data->get_info(k));
			results[i].info.filename=names.at(i);
			results[i].parent=data->getParent(k);
			results[i].axis.xvector=fromvector_tolist(get_xvector(k));
//			results[i].info.np=results[i].axis.xvector.size();
			results[i].spectrum.create(ni, results[i].axis.xvector.size(), complex(0.0));
			results[i].info.sw=results[i].info.sw*results[i].axis.xvector.size()/data->get_odata(k)->cols();
			if (results[i].info.type==FD_TYPE_SPE) {
				double centre=fabs(results[i].axis.xvector(0)-results[i].axis.xvector(results[i].axis.xvector.size()-1))/2.0;
				results[i].info.ref=(results[i].axis.xvector(0)<results[i].axis.xvector(results[i].axis.xvector.size()-1))? results[i].axis.xvector(0)+centre : results[i].axis.xvector(results[i].axis.xvector.size()-1)+centre;
				if (data->global_options.xfrequnits==UNITS_PPM)
                                        results[i].info.ref*=results[i].info.sfrq+results[i].info.ref/1e6;
				else if (data->global_options.xfrequnits==UNITS_KHZ)
					results[i].info.ref*=1000.0;
			}
			results[i].axis.yvector=data->get_y(k);
			if (ni>1)
				results[i].display.isVisible=false;

			results[i].arrays.push_back(Array_(names[i]+QString("_pos")));
			results[i].arrays.push_back(Array_(names[i]+QString("_ampl")));
			results[i].arrays.push_back(Array_(names[i]+QString("_width")));
			results[i].arrays.push_back(Array_(names[i]+QString("_Lor_Gauss")));
			results[i].arrays.push_back(Array_(names[i]+QString("_integral")));
			results[i].arrays.push_back(Array_(names[i]+QString("_pos_error")));
			results[i].arrays.push_back(Array_(names[i]+QString("_ampl_error")));
			results[i].arrays.push_back(Array_(names[i]+QString("_width_error")));
			results[i].arrays.push_back(Array_(names[i]+QString("_L/G_error")));
		}
			results[NLines].info  =*(data->get_info(k));
			results[NLines].info.filename=QString("Deconv_sum");
			results[NLines].parent=data->getParent(k);
			results[NLines].axis.xvector=fromvector_tolist(get_xvector(k));

//			results[NLines].info.np=results[NLines].axis.xvector.size();
			results[NLines].axis.yvector=data->get_y(k);
			results[NLines].spectrum.create(ni, results[NLines].axis.xvector.size(), complex(0.0));
			results[NLines].info.sw=results[NLines].info.sw*results[NLines].axis.xvector.size()/data->get_odata(k)->cols();
			if (ni>1)
				results[NLines].display.isVisible=false;
			if (results[NLines].info.type==FD_TYPE_SPE) {
				double centre=fabs(results[NLines].axis.xvector(0)-results[NLines].axis.xvector(results[NLines].axis.xvector.size()-1))/2.0;
				results[NLines].info.ref=(results[NLines].axis.xvector(0)<results[NLines].axis.xvector(results[NLines].axis.xvector.size()-1))? results[NLines].axis.xvector(0)+centre : results[NLines].axis.xvector(results[NLines].axis.xvector.size()-1)+centre;
				if (data->global_options.xfrequnits==UNITS_PPM)
                                        results[NLines].info.ref*=results[NLines].info.sfrq+results[NLines].info.ref/1e6;
				else if (data->global_options.xfrequnits==UNITS_KHZ)
					results[NLines].info.ref*=1000.0;
			}
			results[NLines].arrays.push_back(Array_(QString("pos")));
			results[NLines].arrays.push_back(Array_(QString("ampl")));
			results[NLines].arrays.push_back(Array_(QString("width")));
			results[NLines].arrays.push_back(Array_(QString("Lor_Gauss")));
			results[NLines].arrays.push_back(Array_(QString("integral")));
			results[NLines].arrays.push_back(Array_(QString("pos_error")));
			results[NLines].arrays.push_back(Array_(QString("ampl_error")));
			results[NLines].arrays.push_back(Array_(QString("width_error")));
			results[NLines].arrays.push_back(Array_(QString("L/G_error")));
			results[NLines].arrays.push_back(Array_(QString("chi^2")));
	}

	vector<double> xvector=get_xvector(k);
	List<double> xv=fromvector_tolist(xvector);

	vector<double> baseline=create_baseline(pars[pars.size()-2], pars[pars.size()-1], xvector);
	vector<double> npars=convert_pars(pars);
        //calculate a sum of all integrals
        double integ_sum=0.0;
        for (int j=0; j<NLines; j++)
            integ_sum+=npars[4*j+1]*npars[4*j+2]*(0.5078*npars[4*j+3]+1.0645);
	for (size_t i=0; i<NLines; i++) {//do this for accumulation
		vector<double> yvec=create_lineshape(npars[4*i], npars[4*i+1], npars[4*i+2], npars[4*i+3], xvector);

		results[i].arrays[0].data.push_back(npars[4*i]); //position
		results[i].arrays[1].data.push_back(npars[4*i+1]); //amplitude
		results[i].arrays[2].data.push_back(npars[4*i+2]); //width
		results[i].arrays[3].data.push_back(npars[4*i+3]); //gau/lor fraction
                double integ_value=npars[4*i+1]*npars[4*i+2]*(0.5078*npars[4*i+3]+1.0645); //integral intensity
                results[i].arrays[4].data.push_back(integ_value);

                linesTable->blockSignals(true);
                linesTable->item(i,5)->setText(QString("%1 (%2\%)").arg(integ_value).arg(integ_value/integ_sum*100.0,0,'f',1));
                linesTable->blockSignals(false);
                results[i].arrays[5].data.push_back(errs[4*i]); //position error
		results[i].arrays[6].data.push_back(errs[4*i+1]); //ampl error
		results[i].arrays[7].data.push_back(errs[4*i+2]); //width error
		results[i].arrays[8].data.push_back(errs[4*i+3]); //gau/lor error

//save the same data for the sum
		results[NLines].arrays[0].data.push_back(npars[4*i]); //position
		results[NLines].arrays[1].data.push_back(npars[4*i+1]); //amplitude
		results[NLines].arrays[2].data.push_back(npars[4*i+2]); //width
		results[NLines].arrays[3].data.push_back(npars[4*i+3]); //gau/lor fraction
                results[NLines].arrays[4].data.push_back(npars[4*i+1]*npars[4*i+2]*(0.5078*npars[4*i+3]+1.0645)); //integral intensity
		results[NLines].arrays[5].data.push_back(errs[4*i]); //position error
		results[NLines].arrays[6].data.push_back(errs[4*i+1]); //ampl error
		results[NLines].arrays[7].data.push_back(errs[4*i+2]); //width error
		results[NLines].arrays[8].data.push_back(errs[4*i+3]); //gau/lor error

		for (size_t j=results[i].axis.xvector.size(); j--;) {
			results[i].spectrum(nrow,j)=complex(yvec[j],0.0)+baseline[j];
			results[NLines].spectrum(nrow,j)+=complex(yvec[j],0.0);
			}
	}
	results[NLines].arrays[9].data.push_back(chi_val);
	for (size_t j=results[NLines].axis.xvector.size(); j--;) 
		results[NLines].spectrum(nrow,j)+=baseline[j]; //correct sum spectrum to baseline
}

vector<double> DeconvForm::convert_pars(vector<double> pars)
{
	vector<double> newpars(pars.size(),0.0);
	int NLines=(pars.size()-2)/4;
	for (size_t i=0; i<NLines; i++) {
		newpars[4*i]=pars[4*i];
		newpars[4*i+1]=pars[4*i+1];
		if (!negativeIntensitiesBox->isChecked())
			newpars[4*i+1]=fabs(pars[4*i+1]);
		newpars[4*i+2]=fabs(pars[4*i+2]);
		
		double lorgaus=fabs(pars[4*i+3]);
		newpars[4*i+3]=lorgaus;
	}
	newpars[pars.size()-2]=pars[pars.size()-2];
	newpars[pars.size()-1]=pars[pars.size()-1];
	return newpars;
}


void DeconvForm::rewrite_linestable(vector<double> pars)
{
	int NLines=(pars.size()-2)/4;
	QTableWidgetItem * item;
	for (size_t i=0; i<NLines; i++) {
		for (size_t j=1; j<5; j++) {
			item=linesTable->item(i,j); 
			item->setText(QString("%1").arg(pars[(j-1)+4*i]));
			linesTable->setItem(i,j,item);
		}
	}
	item=baselineTable->item(0,0);
	item->setText(QString("%1").arg(pars[pars.size()-2]));
	baselineTable->setItem(0,0,item);

	item=baselineTable->item(0,1);
	item->setText(QString("%1").arg(pars[pars.size()-1]));
	baselineTable->setItem(0,1,item);
}

void DeconvForm::showResults(size_t k, QList<data_entry>& results)
{
        for (size_t i=0; i<results.size(); i++)
                data->add_entry(results.at(i));
        p->curveSelected(k);
        p->updateFilesList();
}

vector<double> DeconvForm::get_xvector(size_t k)
{
	vector<double> xvector;
	p->plot2D->fromreal_toindex(k, QPointF(p->plot2D->xwinmin,0), xstart);
	p->plot2D->fromreal_toindex(k, QPointF(p->plot2D->xwinmax,0), xend);
	if (xend<xstart) 
		std::swap(xstart,xend);
	for (size_t i=xstart; i<xend; i++)
		xvector.push_back(data->get_x(k,i));
	return xvector;
}

vector<double> DeconvForm::get_yvector(const size_t k, const size_t row)
{
	vector<double> yvector;
	for (size_t i=xstart; i<xend; i++) //xstart and x end should be predefined, i.e. call get_xvectro first
		yvector.push_back(data->get_value(k,row,i));
	return yvector;
}

void DeconvForm::delete_temp_lines()
{
	size_t k=data->getActiveCurve();
	size_t N=data->size();	
	for (int i=N-1; i>=0; i--) {
		if (data->getParent(i).isTempDeconv) {
			p->fileDelete(i);
			if (i<k) k--;
			p->updateFilesList();
		}
	}
	if (k<data->size()) p->curveSelected(k);
}

void DeconvForm::editCopy()
{
fromTableToClipboard(linesTable);
}

void DeconvForm::editPaste()
{
	cout<<"Paste doesn't work yet\n";
}

List<double> DeconvForm::fromvector_tolist(vector<double> v)
{
	List<double> l;
	for (size_t i=0; i<v.size(); i++)
		l.push_back(v[i]);
	return l;
}

void DeconvForm::guessRightbarPos()
{
//	qDebug()<<"Start search";
	size_t ind0;
	size_t k=data->getActiveCurve();
	p->plot2D->fromreal_toindex(k,p->plot2D->leftbarpos,ind0);
	int leftPos=searchHalfWay(ind0,-1);
	int rightPos=searchHalfWay(ind0,1);
	size_t goodPoint=ind0;
	if ((leftPos<0) && (rightPos<0))
		return;
	else if (leftPos<0)
		goodPoint=rightPos;
	else if (rightPos<0)
		goodPoint=leftPos;
	else
		goodPoint=(fabs(leftPos-int(ind0))<fabs(rightPos-int(ind0)))? leftPos:rightPos;
//	qDebug()<<"Good point"<<goodPoint;
	p->plot2D->rightbarpos=QPointF(data->get_x(k,goodPoint), data->get_value(k,0,goodPoint));
	p->plot2D->is_rightbar=true;
	p->plot2D->update();
	if (autoAddButton->isChecked())
		on_addButton_clicked();
}

int DeconvForm::searchHalfWay(int ind0, int incr)
{
	const size_t k=data->getActiveCurve();
	int wp=ind0;
	double halfM=p->data->get_value(k,0,ind0)/2.0;
//	qDebug()<<"Half intensity"<<halfM;
	while ((wp+incr>0)&&(wp+incr<data->get_odata(k)->cols())) {
		wp+=incr;
		if (p->data->get_value(k,0,wp)<=halfM) {
//			qDebug()<<"HalfIndex"<<wp;
			return size_t(wp);
		}
	}
//	qDebug()<<"Half intensity lost";
	return -1;
}
