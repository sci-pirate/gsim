#ifndef PLOT3DWODGET_H
#define PLOT3DWODGET_H

#define PROJFIELD 80

#include <QProgressDialog>
#include "plot2dwidget.h"

class point3D{ ///3D point
	public:
		double x,y,z;
		point3D(double, double, double);
};

class Plot3DWidget : public Plot2DWidget
{
public:
	Plot3DWidget( QWidget *parent=0); //, const char *name=0 );
	QList<double> levels;///List with level values, sorted in incrementing order
	virtual void		draw_picture(QPainter *);///main drawing function
	virtual QColor 		get_color(size_t);///returns colour which to given 2D data
	void cold_restart(DataStore *);///recaclutes levels, contours, etc.
	void hot_restart();///see Plot2DWidget
	void Update();///see Plot2DWidget
	size_t nlevels;///number of levels (doubled if pos_neg is used)
	int pos_neg;///0-positive, 1-bipolar, 2-negative only
	bool colourContours; ///Are contours coloured according to the levels?
	double floor, ceiling, multiplier;
	double projection_scaler;
   	size_t qstepx; ///step on x
	size_t qstepy; ///step on y (bigger step, less quality)
	bool isRaster;///use raster graphics?
	bool hasProjections;///display projections
	bool hasLegend;//is legend need to be drawn?
	bool smartSideviews; ///controls either the sideviews are always projections or proj/slices
	void createProjections(); ///creates projections which can be displayed in the sideviews
	void createSlicesInsteadOfProjections(); ///creates sideviews contents in the smart sideviews mode
	void deleteProjections(); ///delete contents of the sideviews
	List<QLineF>		countours; ///contours as a collection of the individual lines
	QVector<bool>		neg_pos;///for each contour stores is level neg (0) or pos (1)
	QVector<size_t>		dataset_index;///for  each contour (raster plot) stores the index of data
	QVector<QPointF>	rectangles; ///stores the position of the rectangles
	QVector<QColor>		recColour; ///stores the colours of the rectangles or contours
	QVector<QPointF>	recWidth; ///width of the rectangles for each dataset (not each point);
//	QList<QPixmap>		raster_pix;///list of raster pictures
//	QVector<size_t>		dataset_index_pix;
	bool dirtyContours; 		///true when contours calculation in progress, otherwise false
public slots:
	void 			initialise_levels(); ///calculates level values
    const void 			calculate_countours();///calculates contours and stores them in 'contours' list
//	void 			calculate_countours_mc(size_t);///using marching cube
//	void 			calculate_countours_tr(size_t);///using custom (slow) function
    const void			calculate_countours_tr_opt(size_t);///optimised contouring
    const bool			check_line(QPointF&, point3D&, point3D&,  const double&);//checks interception for a line
	void 			calculate_raster();///calculates rasters and stores them in
//	void 			calculate_raster_pix();///calculates rasters and stores them as pixmaps
	void 			fromreal_toindex(size_t,QPointF,size_t&, size_t&, bool with_shifts=true);///specify curve, position, x index, y index, wether the shifts/scaling should be taken into account 
	virtual void		fromreal_toscreen (double, double, QPointF&);
	virtual void 	     	fromreal_toscreen (double, double, QSize&, QPointF&);
	virtual void 	     	fromreal_toscreen (double, double, QSize&, QPointF&, size_t); ///for particular spectrum with its visual scaling and shifts
	virtual void 	     	fromreal_toscreen (double, double, QSize&, QPoint&, size_t); ///for particular spectrum with its visual scaling and shifts
private:	
    const bool 			check_triangle(QLineF&, point3D&, point3D&, point3D&, const double&, int&);
    const QPointF 		intercept(point3D&, point3D&, const double&);
	virtual void 		set_global_range();
	virtual void       	draw_bars(QPainter *);
   	virtual void 		init_settings();

    const void 			draw_countours(QPainter*);
	void			draw_integrals(QPainter*);
	void			draw_legend(QPainter*);
	QProgressDialog*		progressDialog;
	void			combineLines(QPointF& p1, QPointF& p2, QColor colour);
	QList<QPolygonF>	combined_lines;
	QList<QColor>		combined_colours;
	void			drawRaster(QPainter *);
//	void 			drawRasterPix(QPainter *);
	bool 			willMarkBePrinted(QPoint);
	void 			drawScalings(QPainter *);
    void 			draw_projections(QPainter* );
	void			hprojection_toscreen (double, double, QPointF &, size_t);
	void 			vprojection_toscreen (double, double, QPointF & ,size_t);

    List< List<double> > vprojection;
    List< List<double> > hprojection;
    List<double> vprojectionmax, vprojectionmin, hprojectionmax, hprojectionmin, vpt, hpt;
protected:
	void			wheelEvent(QWheelEvent *);
	void			keyPressEvent ( QKeyEvent *);
};

#endif
