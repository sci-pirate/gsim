#include "plot3dwidget.h"
#include <QRegion>
#include <QProgressDialog>
#include <QApplication>
#include <QDebug>

#include <QTime>

//QColor colour2[] = { Qt::blue, Qt::magenta, Qt::green, QColor(0,0,102), QColor(0,102,0),QColor(102,0,102), QColor(0,128,128), QColor(153,0,0), QColor(214,0,147), QColor(0,204,0), QColor(255,102,0), QColor(255,0,102), QColor(0,204,0), QColor(0,128,128), QColor(51,153,255), QColor(51,51,0), QColor(102,102,153), QColor(255,51,153), QColor(0,0,255)}; //stupid but doesn't work otherwise

///Colour scheme used in raster mode
//QColor rastercolor[] = { Qt::darkRed, Qt::red, Qt::yellow, Qt::darkYellow, Qt::green, Qt::darkGreen, Qt::blue, Qt::darkBlue, Qt::cyan, Qt::darkCyan, Qt::magenta, Qt::darkMagenta,Qt::lightGray, Qt::gray, Qt::darkGray,Qt::darkRed, Qt::red, Qt::yellow, Qt::darkYellow, Qt::green, Qt::darkGreen, Qt::blue, Qt::darkBlue, Qt::cyan, Qt::darkCyan, Qt::magenta, Qt::darkMagenta,Qt::lightGray, Qt::gray, Qt::darkGray}; 

///simple 3D point
point3D::point3D(double x_, double y_, double z_)
{
	x=x_;
	y=y_;
	z=z_;
}

Plot3DWidget::Plot3DWidget( QWidget *parent ): Plot2DWidget( parent)
{
	nlevels=10;
	pos_neg=0;
	floor=10.0;
	ceiling=100.0;
	multiplier=2.0;
	qstepx=1;
	qstepy=1;
	projection_scaler=1.0;
	isRaster=true;
	hasProjections=false;
	hasLegend=false;
	smartSideviews=false;
	dirtyContours=false;
	colourContours=false;
}

void Plot3DWidget::hot_restart()
{
	init_settings();
	set_global_range();
	double x1=xglobmin;
	double x2= xglobmax;
	double y1=yglobmin;
	double y2=yglobmax;

	if (hasProjections) {
		createProjections();
		double xadd=(xglobmax-xglobmin)*PROJFIELD/width();
		double yadd=(yglobmax-yglobmin)*PROJFIELD/height();
		if (is_xaxis_inverted)
			x1-=xadd;
		else
			x2+=xadd;
		if (is_yaxis_inverted)
			y1-=yadd;
		else
			y2+=yadd;
	}
	set_window_range(x1-(x2-x1)*0.1,x2+(x2-x1)*0.1,y1-(y2-y1)*0.1,y2+(y2-y1)*0.1);
	update();
}

void Plot3DWidget::cold_restart(DataStore* p)
{
    if (!p) cerr<<"Hot_restart:data is empty\n";
    data=p;
    QFont f;
    f.fromString(data->global_options.axisFont);
    setFont(f);
    if (!p->size()) {
	update(); 
	return;
    }
    is_leftbar=false;
    is_rightbar=false;
    markerMovie1->stop();
    markerMovie2->stop();
    markerLabel1->hide();
    markerLabel2->hide();
    initialise_levels();
    hot_restart();
}

void Plot3DWidget::Update()
{
	initialise_levels();
	update();
}

/*
void Plot3DWidget::drawRaster(QPainter *p)
{
//	QTime time;
//	time.start();
	QSize win=size();
	size_t nlev=levels.size();
	QPen pen;
	QBrush brush;
	brush.setStyle(Qt::SolidPattern);
	size_t ncurves=data->size(); 
	size_t i,j;
	bool is_drawable;
	double x1=0, x2=0, y1=0, y2=0;
	QPointF leftdown, rightup;
	for (size_t m=0; m<ncurves; m++) {
//		const gsimFD* info=data->get_info(m);
		if (!data->get_display(m)->isVisible) continue;
		if (data->get_odata(m)->rows()>1) {
			size_t startx, starty, endx, endy;
			fromreal_toindex(m, QPointF(xwinmin,ywinmin),startx, starty);
			fromreal_toindex(m, QPointF(xwinmax,ywinmax),endx, endy);
			if (endx<startx) 
				swap(startx,endx);
			if (startx>1) 
				startx-=2;
			if (endx<data->get_odata(m)->cols()-1)
				endx++;
			if (endy<starty) 
				swap(starty,endy);
			if (starty>1) 
				starty-=2;
			if (endy<data->get_odata(m)->rows()-1)
				endy++;
			int stepx=(endx-startx)/width();
			if (!stepx)
				stepx=1;
			int stepy=(endy-starty)/height();
			if (!stepy)
				stepy=1;

//			qDebug()<<"startx"<<startx<<"endx"<<endx;
//			qDebug()<<"starty"<<starty<<"endy"<<endy;
//			qDebug()<<"stepx"<<stepx<<"stepy"<<stepy;
			i=0; j=0;
			x1=data->get_x(m,0);
			x2=data->get_x(m,stepx);
			y1=data->get_y(m,0);
			y2=data->get_y(m,stepy);
			fromreal_toscreen(x1,y1,win,leftdown,m);
			fromreal_toscreen(x2,y2,win,rightup,m);
			int w=int(rightup.x()-leftdown.x());
			int h=int(rightup.y()-leftdown.y());
			if (!w)	w=1;
			if (!h)	h=1;
//			pen.setWidth(abs(h));
			QPointF dpoint; //point for start drawing
			QPointF old(0.0,0.0); //previous point
			for (i=startx; i<endx; i+=stepx) 
				for (j=starty; j<endy; j+=stepy) {
					double mean=data->get_value(m,j,i);
					is_drawable=false;

//		cout<<"Lev: ";
//		for (size_t hh=0; hh<levels.size(); hh++)
//			cout<<levels.at(hh)<<":";
//		cout<<endl;
//				if (mean>levels.at(0) && mean<=levels.last()) { //search only for points between boundaries

					for (size_t q=0; q<nlev-1; q++) {
						if (levels.at(q)<0 && levels.at(q+1)>0) continue; //if pos/neg don't analyse the zone around zero
//						if (mean>levels.at(q) && mean<=levels.at(q+1)) {
						if (mean>levels.at(q) && mean<=levels.at(q+1)) {
							is_drawable=true;
							pen.setColor(rastercolor[q]);
 						brush.setColor(rastercolor[q]);
							break;
						}
					}
				if (is_drawable) {
					p->setPen(pen);
					p->setBrush(brush);
					x1=data->get_x(m,i);
					y1=data->get_y(m,j);
					fromreal_toscreen(x1,y1,win,dpoint,m);
//					if (dpoint!=old) {
//						if (w==1 && h==1)  //tried, doesn't improve anything
//							p->drawPoint(int(dpoint.x()),int(dpoint.y()));
//						else if (w==1)
//							p->drawLine(int(dpoint.x()),int(dpoint.y()), int(dpoint.x()), int(dpoint.y())+h);
//						else if (h==1)
//							p->drawLine(int(dpoint.x()),int(dpoint.y()), int(dpoint.x())+w, int(dpoint.y()));
//						else
					p->drawRect(int(dpoint.x()),int(dpoint.y()),w,h);
//						old=dpoint;
//					}
				}
//				}
				}
		}
	}
brush.setStyle(Qt::NoBrush);
p->setBrush(brush);
drawScalings(p);
//qDebug("Raster operation: Time elapsed: %d ms", time.elapsed());
}
*/

void Plot3DWidget::calculate_raster()
{
	dirtyContours=true;
//        qDebug()<<"Raster calcultaion";
	dataset_index.clear();
	rectangles.clear();
	recColour.clear();
	recWidth.clear();
	size_t nlev=levels.size();
	size_t ncurves=data->size();
	recWidth.resize(ncurves);
	recWidth.fill(QPointF(1,1));

	size_t i,j;
	for (size_t m=0; m<ncurves; m++) {
//		const gsimFD* info=data->get_info(m);
		const size_t ni=data->get_odata(m)->rows();
		const size_t np=data->get_odata(m)->cols();
//		qDebug()<<"ni"<<ni<<"np"<<np;
		if (!data->get_display(m)->isVisible) continue;
		if (ni>1) {
			double w=fabs(data->get_x(m,0)-data->get_x(m,qstepx));
			double h=fabs(data->get_y(m,0)-data->get_y(m,qstepy));
			recWidth[m]=QPointF(w,h);
			for (j=0; j<=ni-qstepy; j+=qstepy) 
				for (i=0; i<=np-qstepx; i+=qstepx) {
//			for (i=0; i<=info->np-qstepx; i+=qstepx) 
//				for (j=0; j<=info->ni-qstepy; j+=qstepy) {
                                        //qDebug()<<"Look for"<<j<<i;
					double mean=data->get_value(m,j,i);
					for (size_t q=0; q<nlev-1; q++) {
						if (levels.at(q)<0 && levels.at(q+1)>0) 
							continue; //if pos/neg don't analyse the zone around zero
						if (mean>levels.at(q) && mean<=levels.at(q+1)) {
							recColour.push_back(colourScheme.plot3DRasterColour.at(q));
                                                        rectangles.push_back(QPointF(data->get_x(m,i),data->get_y(m,j)));
							dataset_index.push_back(m);
							break;
						}
					}
				}
				}
		}
//qDebug()<<"Raster calculation finished";
	dirtyContours=false;
}

/*
void Plot3DWidget::calculate_raster_pix()
{
	dirtyContours=true;
//	qDebug()<<"Raster calcultaion";
	raster_pix.clear();
	dataset_index_pix.clear();
	size_t nlev=levels.size();
	size_t ncurves=data->size();

	size_t i,j;
//	qDebug()<<"Preparation passed";
	for (size_t m=0; m<ncurves; m++) {
		const size_t ni=data->get_odata(m)->rows();
		const size_t np=data->get_odata(m)->cols();
		if (!data->get_display(m)->isVisible) continue;
		if (ni>1) {
			QPixmap pix(np,ni);
			pix.fill(Qt::transparent);
			QPainter paint(&pix);
			dataset_index_pix.push_back(m);
			for (j=0; j<ni; j++) {
				for (i=0; i<np; i++) {
					double mean=data->get_value(m,j,i);
					for (size_t q=0; q<nlev-1; q++) {
						if (levels.at(q)<0 && levels.at(q+1)>0) 
							continue; //if pos/neg don't analyse the zone around zero
						if (mean>levels.at(q) && mean<=levels.at(q+1)) {
							paint.setPen(colourScheme.plot3DRasterColour.at(q));
							paint.drawPoint(ni-i-1,j);
							break;
						}
					}
				}
			}
			paint.end();
			raster_pix.push_back(pix);
		}
	}
//qDebug()<<"Raster calculation finished";
	dirtyContours=false;
}
*/



//worth trying to use QPixmap with the resolution = NPxNI, drawRaster would rescale the image (problem - it is not editable)
void Plot3DWidget::drawRaster(QPainter *p)
{
//	QTime t;
//	t.start();
//	qDebug()<<"Draw raster started";
	int drawn=0;
	int omitted=0;
	QSize win=size();
	QPen pen;
	QBrush brush;
	brush.setStyle(Qt::SolidPattern);
	size_t len=rectangles.size();
	int kold=-1;
	QRectF rect(0,0,0,0);
//	QRectF winrect=contentsRect(); //rectangle with windows size
	QPointF dpointold(-10,-10);
	QRectF winrect=QRectF(xwinmin, ywinmin, xrange, yrange).normalized();
	for (size_t i=0; i<len; i++)
	{
		if (!data->get_display(dataset_index.at(i))->isVisible)
			continue;
		size_t k=dataset_index.at(i);
		if (!data->global_options.mixFidSpec)
			if ((data->get_info(k)->type!=data->get_info(data->getActiveCurve())->type) || (data->get_info(k)->type1!=data->get_info(data->getActiveCurve())->type1))
				continue;
		if (!winrect.intersects(QRectF(rectangles.at(i).x(),rectangles.at(i).y(),recWidth.at(k).x()*data->get_display(k)->xscale,recWidth.at(k).y()*data->get_display(k)->vscale).normalized())) {
			omitted++;
			continue; }


		QPointF dpoint;
		fromreal_toscreen(rectangles.at(i).x(),rectangles.at(i).y(),win,dpoint,k);
		if (k!=kold) {
			rect.setWidth(fabs(recWidth.at(k).x()/xppt*data->get_display(k)->xscale));
			rect.setHeight(recWidth.at(k).y()/yppt*data->get_display(k)->vscale);
			kold=k;
		}
		dpoint.setY(dpoint.y()-rect.height()/2.0);//move rectangle 1point up to ensure visula appearence synchronised with marker picker
		dpoint.setX(dpoint.x()-rect.width()/2.0);
		if (dpoint==dpointold){
//			qDebug()<<"Too dense data";
			continue;
		}
		dpointold=dpoint;
		rect.moveTo(dpoint);
//		if (!winrect.intersects(rect)) {
//			omitted++;
//			continue;
//		}
		drawn++;
		pen.setColor(recColour.at(i));
		brush.setColor(recColour.at(i));
		p->setPen(pen);
		p->setBrush(brush);
//		qDebug()<<"Rect width"<<rect.width();
		if (rect.width()<1.0 && rect.height()<1.0)
			p->drawPoint(dpoint);
		else if (rect.width()<1.0)
			p->drawLine(rect.topLeft(),rect.bottomLeft());
		else if (rect.height()<1.0)
			p->drawLine(rect.bottomLeft(),rect.bottomRight());
		else
			p->drawRect(rect);
	}
	p->resetMatrix();
drawScalings(p);
//qDebug()<<"Draw raster finished";
}

/*
void Plot3DWidget::drawRasterPix(QPainter *p)
{
	QSize win=size();
	size_t len=raster_pix.size();
	for (size_t i=0; i<len; i++)
	{
		if (!data->get_display(dataset_index_pix.at(i))->isVisible)
			continue;
		size_t k=dataset_index_pix.at(i);
		if (!data->global_options.mixFidSpec)
			if ((data->get_info(k)->type!=data->get_info(data->getActiveCurve())->type) || (data->get_info(k)->type1!=data->get_info(data->getActiveCurve())->type1))
				continue;
		QPoint p1;
		size_t ni=data->get_odata(k)->rows();
		double w=fabs(data->get_x(k,0)-data->get_x(k,1));
		double h=fabs(data->get_y(k,0)-data->get_y(k,1));
		QMatrix mat;
		mat.scale(w/xppt,h/yppt);
		qDebug()<<"Transformation:"<<w/xppt<<h/yppt;
		fromreal_toscreen(data->get_x(k,0),data->get_y(k,ni-1),win,p1,k);
		p->drawPixmap(p1,raster_pix.at(i).transformed(mat));
	}
drawScalings(p);
//qDebug()<<"Draw raster finished";
}
*/
//Slower than expected!

const void Plot3DWidget::draw_countours(QPainter *p)
{
	QSize win=size();
	QPointF p1,p2;
	int h,s,v;
	int k;
	int kold=-1;
	QColor posColour, negColour;
	size_t len=countours.size();
	QPen pen;
	if (for_print)
		pen.setWidth(data->global_options.printLineWidth);
	QLineF oldline(-10,-10,-20,-20);
//	if (for_print) {
//		progressDialog = new QProgressDialog("Preparing contour plot...", "Dummy", 0, len, this);
//		progressDialog->setWindowModality(Qt::WindowModal);
//	}
	for (size_t i=0; i<len; i++)
	{
		k=dataset_index.at(i);
		if (!data->get_display(k)->isVisible)
			continue;
		if (!data->global_options.mixFidSpec)
			if ((data->get_info(k)->type!=data->get_info(data->getActiveCurve())->type) || (data->get_info(k)->type1!=data->get_info(data->getActiveCurve())->type1))
				continue;
		if (!QRectF(xwinmin, ywinmin, xrange, yrange).normalized().contains(QRectF(countours(i).x1(),countours(i).y1(),countours(i).dx(),countours(i).dy()).normalized()),false)
			continue;


		if (k!=kold) {
			posColour=get_color(k);
			negColour=pen.color();
			negColour.getHsv(&h,&s,&v);
			s=100;
			v=200;
			negColour.setHsv(h,s,v);
			kold=k;
		}
		if (!colourContours) {
			if (!neg_pos[i]) 
				pen.setColor(negColour);
			else
				pen.setColor(posColour);
		}
		else {
			pen.setColor(recColour[i]);
		}
		p->setPen(pen);
		fromreal_toscreen(countours(i).x1(),countours(i).y1(),win,p1, k);
		fromreal_toscreen(countours(i).x2(),countours(i).y2(),win,p2,k);
		if (QLineF(p1,p2)==oldline) {
			continue;
		}
		else {
			if (!for_print)
				p->drawLine(p1,p2);
			else 	{
				combineLines(p1,p2,pen.color().toRgb());
//				if (i%100) {
//					progressDialog->setValue(i);
//				}
			}
			oldline=QLineF(p1,p2);
		}
	}
if (for_print) {
	for (int i=0; i<combined_lines.size(); i++) {
		pen.setColor(combined_colours.at(i));
		pen.setWidthF(0.1); //draw bolder lines
		p->setPen(pen);
		p->drawPolyline(combined_lines.at(i));
	}
//	qDebug()<<"Total number of line"<<len<<"is reduced to"<<combined_lines.size()<<"polygons";
//	progressDialog->setValue(len);
//	for_print=false;
//	delete progressDialog;
	combined_lines.clear();
	combined_colours.clear();
}
drawScalings(p);
//qDebug()<<"Contour elapsed:"<<t.elapsed();
}

//TODO If a lot of lines are present, a program freese for long period of time during contours recombination.
void Plot3DWidget::combineLines(QPointF& p1, QPointF& p2, QColor colour)
{
	const double tol=0.1;
	bool added=false;
	for (int i=0; i<combined_lines.size(); i++) {
		if (colour!=combined_colours.at(i))
			continue;
		int polsize=combined_lines.at(i).size();
		if (QLineF(p1,combined_lines.at(i).at(0)).length()<=tol) {
			combined_lines[i].prepend(p2);
			added=true;
		}
		else if (QLineF(p2,combined_lines.at(i).at(0)).length()<=tol) {
			combined_lines[i].prepend(p1);
			added=true;
		}
		else if (QLineF(p1,combined_lines.at(i).at(polsize-1)).length()<=tol) {
			combined_lines[i].push_back(p2);
			added=true;
		}
		else if (QLineF(p2,combined_lines.at(i).at(polsize-1)).length()<=tol) {
			combined_lines[i].push_back(p1);
			added=true;
		}
	}
	if (!added) {
		combined_colours.push_back(colour);
		QPolygonF newpoly;
		newpoly<<p1<<p2;
		combined_lines.push_back(newpoly);
	}
}

void Plot3DWidget::drawScalings(QPainter *p)
{
	size_t k=data->getActiveCurve();
	const Display_* display=data->get_display(k);
	if (data->get_odata(k)->rows()>1)
	if (display->xscale!=1.0 || display->xshift!=0 || display->vshift!=0 || display->vscale!=1.0) {
		p->drawText(2*width()/3,p->fontMetrics().height()+labeloffset,QString("Shift X: %1  Y: %2").arg(display->xshift).arg(display->vshift));
		p->drawText(2*width()/3,p->fontMetrics().height()*2+labeloffset,QString("Scale X: %1  Y: %2 ").arg(display->xscale).arg(display->vscale));
	}
}

void Plot3DWidget::draw_integrals(QPainter* p)
{
	if (for_print)
		p->setPen(data->global_options.printLineWidth);
	foreach (size_t k, data->selectedCurves) {
		if (data->get_odata(k)->rows()<2)
			continue;
		if (!data->integrals_size(k))
			continue;
		if (!data->get_display(k)->isVisible)
			continue;
//searching integrals list
		Array_ intar("empty");
		for (size_t i=0; i<data->get_arrays(k).size(); i++)
		if (data->get_arrays(k).at(i).name==QString("IntegList"))
			intar=data->get_arrays(k)[i];
		if (intar.name==QString("empty"))
			throw Failed("IntegList not found");
		for (size_t q=0; q<data->integrals_size(k); q++) { //for each integral
//			double rwidth=data->get_integral_x(k,q,1)-data->get_integral_x(k,q,0);
//			double rheight=data->get_integral_x(k,q,1)-data->get_integral_x(k,q,0);
//			QRectF rect=QRectF(data->get_integral_x(k,q,0),data->get_integral_value(k,q,0),rwidth,rheight).normalised;
			QPointF pt1,pt2;
			fromreal_toscreen(data->get_integral_x(k,q,0),data->get_integral_value(k,q,0),pt1);
			fromreal_toscreen(data->get_integral_x(k,q,1),data->get_integral_value(k,q,1),pt2);
			QRectF rect=QRectF(pt1,pt2).normalized();
			QPolygonF poly;
			poly<<rect.topLeft()<<rect.bottomLeft()<<rect.bottomRight()<<rect.topRight();
			double val=intar.data(q)*data->get_integral_scale(k);
			QString label=QString("%1").arg(val,0,'g',3);
			p->drawText(rect.topLeft(),label);
			if (p->fontMetrics().width(label)<rect.width())
				poly<<QPointF(rect.left()+p->fontMetrics().width(label)+1,rect.top());
//				p->drawLine(rect.topRight(),QPointF(rect.left()+p->fontMetrics().width(label),rect.top()));
			p->drawPolyline(poly);
		}
	}
}

void Plot3DWidget::draw_legend(QPainter* p)
{
	if (!hasLegend)
		return;
	const size_t k = data->getActiveCurve();
	if (data->get_odata(k)->rows()<2)
		return;
	int plotstep=fontMetrics().height();
	int ypos=(hasProjections)? PROJFIELD+10 : 10;//initial y-position of the legend on the plot
	for (size_t i=0; i<levels.size(); i++) { //the main loop over the number of levels
		bool show_level=true;
//		graph_object line;
//		line.type=GRAPHLINE;
		p->setBrush(colourScheme.plot3DRasterColour.at(i));
		p->setPen(colourScheme.plot3DRasterColour.at(i));
		//		line.colour=plot3D->colourScheme.plot3DRasterColour.at(i);
//		graph_object label;
//		label.type=GRAPHTEXT;
//		label.colour=plot3D->colourScheme.plot3DRasterColour.at(i);
		QString label;
		if (isRaster) {
			if (i==levels.size()-1)
				show_level=false;
			else if (levels.at(i)<0 && levels.at(i+1)>0)
				show_level=false;
			else
//				label.maindata=QString("%1 -- %2").arg(plot3D->levels.at(i)).arg(plot3D->levels.at(i+1));
				label=QString("%1 -- %2").arg(levels.at(i)).arg(levels.at(i+1));
		}
		else {
			colourContours=true;
			label=QString("%1").arg(levels.at(i));
		}
//		double x1,x2,y1,y2,tx,ty;
		int init_x=(hasProjections)? width()-PROJFIELD-200 : width()-200;
		if (show_level) {
//			plot3D->fromscreen_toreal(QPoint(100,ypos),x1,y1);
//			plot3D->fromscreen_toreal(QPoint(140,ypos),x2,y2);
//			plot3D->fromscreen_toreal(QPoint(160,ypos+plotstep/2),tx,ty);
			p->drawRect(init_x,ypos,40,plotstep);
			p->drawText(QPoint(init_x+50,ypos+plotstep),label);
			ypos+=plotstep;
//			line.pos=QLineF(x1,y1,x2,y2);
//			label.pos=QPointF(tx,ty);
//			graphics.push_back(line);
//			graphics.push_back(label);
		}
	}
//	data->set_graphics(k,graphics);
	update();
}


const void Plot3DWidget::calculate_countours()
{
	dirtyContours=true;
    QTime t;
    t.start();
	countours.clear();
	neg_pos.clear();
	recColour.clear();
	dataset_index.clear();
	size_t ncurves=data->size(); 
	for (size_t m=0; m<ncurves; m++) {
//		if (data->get_info(m)->ni * data->get_info(m)->np > 65536)
//			calculate_countours_mc(m);
//		else
			calculate_countours_tr_opt(m);
//			calculate_countours_tr(m);
	}
qDebug()<<"Contour elapsed:"<<t.elapsed();
	dirtyContours=false;
}

#define LL data->get_x(m,j), (data->get_y(m,i+qstepy)+data->get_y(m,i))/2.0
#define RR data->get_x(m,j+qstepx), (data->get_y(m,i+qstepy)+data->get_y(m,i))/2.0
#define TT (data->get_x(m,j+qstepx)+data->get_x(m,j))/2.0, data->get_y(m,i+qstepy)
#define BB (data->get_x(m,j+qstepx)+data->get_x(m,j))/2.0, data->get_y(m,i)

//#define LL data->get_x(m,j), (levels.at(k)-data->get_value(m,i,j))/(data->get_value(m,i+qstepy,j)-data->get_value(m,i,j))*(data->get_y(m,i+qstepy)-data->get_y(m,i))+data->get_y(m,i)
//#define RR data->get_x(m,j+qstepx), (levels.at(k)-data->get_value(m,i,j))/(data->get_value(m,i+qstepy,j)-data->get_value(m,i,j))*(data->get_y(m,i+qstepy)-data->get_y(m,i))+data->get_y(m,i)
//#define TT (levels.at(k)-data->get_value(m,i,j))/(data->get_value(m,i,j+qstepx)-data->get_value(m,i,j))*(data->get_x(m,j+qstepx)-data->get_x(m,j))+data->get_x(m,j), data->get_y(m,i+qstepy)
//#define BB (levels.at(k)-data->get_value(m,i,j))/(data->get_value(m,i,j+qstepx)-data->get_value(m,i,j))*(data->get_x(m,j+qstepx)-data->get_x(m,j))+data->get_x(m,j), data->get_y(m,i)


/*
void Plot3DWidget::calculate_countours_mc(size_t m)
{
	size_t nlev=levels.size();
	QLineF line;
//		const gsimFD* info=data->get_info(m);
		const size_t ni=data->get_odata(m)->rows();
		const size_t np=data->get_odata(m)->cols();
		if (ni>1) {
			size_t startx=0;//fromrealx_toindex(m, xwinmin);
			size_t endx=np-1;//fromrealx_toindex(m,xwinmax);
			size_t starty=0;//fromrealy_toindex(m, ywinmin);
			size_t endy=ni-1;//fromrealy_toindex(m,ywinmax);
			QProgressDialog progress("Calculating contours...", "Abort", 0, endy-qstepy, this);
        		for (size_t i=starty; i<=endy-qstepy; i+=qstepy){
				progress.setValue(i);
				QCoreApplication::instance()->processEvents();
				if (progress.wasCanceled())
					break;
				for (size_t j=startx; j<=endx-qstepx; j+=qstepx) {
					for (size_t k=0; k<nlev; k++){
//Check cube using marching cube algorithm

int conCase=(data->get_value(m,i,j)>levels.at(k))+2*(data->get_value(m,i+qstepy,j)>levels.at(k))+4*(data->get_value(m,i,j+qstepx)>levels.at(k))+8*(data->get_value(m,i+qstepy,j+qstepx)>levels.at(k));

if(conCase==0) //means all points are above current level and therefore below all further levels
	break;

switch (conCase) {
	case 1: //only left bottom
	case 14:
		countours.push_back(QLineF(LL,BB));
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		break;
	case 2: //only left top
	case 13:
		countours.push_back(QLineF(LL,TT));
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		break;
	case 3: //both left points
	case 12:
		countours.push_back(QLineF(TT,BB));
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		break;
	case 4: // only right bottom
	case 11:
		countours.push_back(QLineF(BB,RR));
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		break;
	case 5: //both bottom
	case 10:
		countours.push_back(QLineF(LL,RR));
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		break;
	case 6: //left top and bottom right
		countours.push_back(QLineF(LL,TT));
		countours.push_back(QLineF(BB,RR));
		neg_pos.push_back(levels.at(k)>0);
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		dataset_index.push_back(m);
		break;
	case 7: // without right top
	case 8:
		countours.push_back(QLineF(TT,RR));
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		break;
	case 9:
		countours.push_back(QLineF(LL,BB));
		countours.push_back(QLineF(TT,RR));
		neg_pos.push_back(levels.at(k)>0);
		neg_pos.push_back(levels.at(k)>0);
		dataset_index.push_back(m);
		dataset_index.push_back(m);
		break;
	default:
		break;
}
				}//cycle on k
			}//cycle on j
		}//cycle on i
progress.setValue(endy-qstepy);
		}//if 2D
}
*/

/*
void Plot3DWidget::calculate_countours_tr(size_t m)
{
	size_t nlev=levels.size();
	QLineF line;
//		const gsimFD* info=data->get_info(m);
		const size_t ni=data->get_odata(m)->rows();
		const size_t np=data->get_odata(m)->cols();
		if (ni>1) {
			size_t startx=0;//fromrealx_toindex(m, xwinmin);
			size_t endx=np-1;//fromrealx_toindex(m,xwinmax);
			size_t starty=0;//fromrealy_toindex(m, ywinmin);
			size_t endy=ni-1;//fromrealy_toindex(m,ywinmax);
			int what=0;
			QProgressDialog progress("Calculating contours...", "Abort", 0, nlev, this);	
			for (size_t k=0; k<nlev; k++){
				progress.setValue(k);
				QCoreApplication::instance()->processEvents();
				if (progress.wasCanceled())
					break;
        			for (size_t i=starty; i<=endy-qstepy; i+=qstepy)
					for (size_t j=startx; j<=endx-qstepx; j+=qstepx) {
						point3D a(data->get_x(m,j), data->get_y(m,i), data->get_value(m,i,j));
						point3D b(data->get_x(m,j+qstepx), data->get_y(m,i), data->get_value(m,i,j+qstepx));
						point3D c(data->get_x(m,j), data->get_y(m,i+qstepy), data->get_value(m,i+qstepy,j));
						point3D d(data->get_x(m,j+qstepx), data->get_y(m,i+qstepy), data->get_value(m,i+qstepy,j+qstepx));
						if (check_triangle(line, a,b,c, levels.at(k),what)){
							neg_pos.push_back(levels.at(k)>0);
							dataset_index.push_back(m);
							countours.push_back(line);
						}
        					if (check_triangle(line, d,b,c, levels.at(k),what)){
							neg_pos.push_back(levels.at(k)>0);
							dataset_index.push_back(m);
							countours.push_back(line);
						}
				}
			}
			progress.setValue(nlev);
		}
}
*/

//optimised procedure for contour calculation
const void Plot3DWidget::calculate_countours_tr_opt(size_t m)
{
	size_t nlev=levels.size();
	const size_t ni=data->get_odata(m)->rows();
	const size_t np=data->get_odata(m)->cols();
	if (ni>1) {
		size_t startx=0;//fromrealx_toindex(m, xwinmin);
		size_t endx=np-1;//fromrealx_toindex(m,xwinmax);
		size_t starty=0;//fromrealy_toindex(m, ywinmin);
		size_t endy=ni-1;//fromrealy_toindex(m,ywinmax);
		size_t smax=(endx-startx+1)/qstepx;//maximal length of points in dir dimension
		List<QPointF> bottomLinePt(smax);//intrcepting points at bottom
		List<bool> bottomLine(smax);//is bottom border contains point
		QPointF leftPt;//point at left
		bool left; //is left border contains pt?
		int what=0; //contains what kind if interception is found by check_triangle
		QPointF wpt, diagpt; //working point
		QLineF wline;//working line
		bool toptr, diagl, chekr;//is top-right triangle contains interception? The same for diagonal. Plus check
		QProgressDialog progress("Calculating contours...", "Abort", 0, nlev, this);	
		for (size_t k=0; k<nlev; k++){
			progress.setValue(k);
			QCoreApplication::instance()->processEvents();
			if (progress.wasCanceled())
				break;
        		for (size_t i=starty; i<=endy-qstepy; i+=qstepy) {
				if (i==starty) { //fill bottom line
					for (size_t jj=startx, s=0; jj<=endx-qstepx; jj+=qstepx, s++) {
						point3D a(data->get_x(m,jj), data->get_y(m,i), data->get_value(m,i,jj));
						point3D b(data->get_x(m,jj+qstepx), data->get_y(m,i), data->get_value(m,i,jj+qstepx));
						chekr=check_line(wpt,a,b,levels.at(k));//checks line for interceptions
						bottomLine(s)=chekr;
						bottomLinePt(s)=wpt;
					}
				}
				for (size_t j=startx, s=0; j<=endx-qstepx; j+=qstepx,s++) {
					if (j==startx) { //if first column then left lineshould be checked first
						point3D a(data->get_x(m,j), data->get_y(m,i), data->get_value(m,i,j));
						point3D b(data->get_x(m,j), data->get_y(m,i+qstepy), data->get_value(m,i+qstepy,j));
						left=check_line(leftPt,a,b,levels.at(k));
					}
					point3D a(data->get_x(m,j), data->get_y(m,i+qstepy), data->get_value(m,i+qstepy,j));
					point3D c(data->get_x(m,j+qstepx), data->get_y(m,i), data->get_value(m,i,j+qstepx));
					point3D b(data->get_x(m,j+qstepx), data->get_y(m,i+qstepy), data->get_value(m,i+qstepy,j+qstepx));

					toptr=check_triangle(wline,a,b,c,levels.at(k),what);//check top-right triangle. Resulting line will be added to contours later to preseree some order which is needed for efficient line combination

					if ((!toptr)&&(!left)&&(!bottomLine(s))) { //no interceptions whatsoever
						left=false;
						bottomLine(s)=false;
						continue;
					}

					diagl=check_line(diagpt,a,c,levels.at(k));//check diagonal
					if (left&&bottomLine(s)){ //interception between left side and bottom
						neg_pos.push_back(levels.at(k)>0);
						recColour.push_back(colourScheme.plot3DRasterColour.at(k));
						dataset_index.push_back(m);
						countours.push_back(QLineF(bottomLinePt(s),leftPt));
					}

					if (left&&diagl) {
						neg_pos.push_back(levels.at(k)>0);
						recColour.push_back(colourScheme.plot3DRasterColour.at(k));
						dataset_index.push_back(m);
						countours.push_back(QLineF(leftPt,diagpt));
					}
					if (bottomLine(s)&&diagl) {
						neg_pos.push_back(levels.at(k)>0);
						recColour.push_back(colourScheme.plot3DRasterColour.at(k));
						dataset_index.push_back(m);
						countours.push_back(QLineF(bottomLinePt(s),diagpt));
					}

					if (toptr){ //add line, found for top-right triangle
						neg_pos.push_back(levels.at(k)>0);
						recColour.push_back(colourScheme.plot3DRasterColour.at(k));						
						dataset_index.push_back(m);
						countours.push_back(wline);
					}
					
					//update left and bottom caches according to check on top-right triangle
					if (!what) {
						left=false;
						bottomLine(s)=false;
					}
					else if (what==1) {
						left=true;
						leftPt=wline.p2();
						bottomLine(s)=true;
						bottomLinePt(s)=wline.p1();
					}
					else if (what==2) {
						left=false;
						bottomLine(s)=true;
						bottomLinePt(s)=wline.p1();
					}
					else if (what==3){
						left=true;
						leftPt=wline.p2();
						bottomLine(s)=false;
					}
				}
			}
			}
			progress.setValue(nlev);
	}
}

/*
inline bool Plot3DWidget::check_line(QPointF& point, point3D& a, point3D& b,  const double& level)
{
	char check=2*(a.z<level)+(b.z<level);
	if (check==2 || check==1) {
			point=intercept(a,b,level);
			return true;
	}
	else
		return false;
}


bool Plot3DWidget::check_triangle(QLineF& line, point3D& a, point3D& b, point3D& c, const double& level, int& what)
{
	char check=4*(a.z<level)+2*(b.z<level)+(c.z<level);
	if (!check || (check==7)) {
		what=0;
		return false;
	}	
    if ((check==4) || (check==3)){
		line=QLineF(intercept(a,b,level), intercept(a,c,level));
		what=2;
		return true;
	}
	if ((check==2) || (check==5)){
		line=QLineF(intercept(b,a,level),intercept(b,c,level));
		what=1;
		return true;
	}
	else{
		line=QLineF(intercept(c,a,level), intercept(c,b,level));
		what=3;
		return true;
	}
	return false;
}
*/

const inline bool Plot3DWidget::check_line(QPointF& point, point3D& a, point3D& b,  const double& level)
{
    char check=2*(a.z<level)+(b.z<level);
    if (check==1) {
            point=intercept(a,b,level);
            return true;
    }
    else if (check==2){
        point=intercept(a,b,level);
        return true;
    }
    else
        return false;
}

const bool Plot3DWidget::check_triangle(QLineF& line, point3D& a, point3D& b, point3D& c, const double& level, int& what)
{
    char check=4*(a.z<level)+2*(b.z<level)+(c.z<level);
    if (!check) {
        what=0;
        return false;
    }
    else if (check==7) {
        what=0;
        return false;
    }
    else if (check==4){
        line=QLineF(intercept(a,b,level), intercept(a,c,level));
        what=2;
        return true;
    }
    else if (check==3){
        line=QLineF(intercept(a,b,level), intercept(a,c,level));
        what=2;
        return true;
    }
    else if (check==2){
        line=QLineF(intercept(b,a,level),intercept(b,c,level));
        what=1;
        return true;
    }
    else if (check==5){
        line=QLineF(intercept(b,a,level),intercept(b,c,level));
        what=1;
        return true;
    }
    else{
        line=QLineF(intercept(c,a,level), intercept(c,b,level));
        what=3;
        return true;
    }
    return false;
}

const inline QPointF Plot3DWidget::intercept(point3D &a, point3D& b, const double& level)
{
	QPointF point;
	double fr=(level-a.z)/(b.z-a.z);
	if (a.x==b.x)
		point.setX(a.x);
	else{
		point.setX( fr*(b.x-a.x)+a.x );
	}
	if (a.y==b.y)
		point.setY(a.y);
	else{
		point.setY( fr*(b.y-a.y)+a.y );
	}
return point;
}

void Plot3DWidget::set_global_range()
{
	xglobmin=yglobmin=9e36;
	xglobmax=yglobmax=-9e36;
	size_t ncurves=data->size();
	for (size_t k=0; k<ncurves; k++)
	{
		if (!data->get_display(k)->isVisible)
			continue;
		if (!data->global_options.mixFidSpec)
			if ((data->get_info(k)->type!=data->get_info(data->getActiveCurve())->type) || (data->get_info(k)->type1!=data->get_info(data->getActiveCurve())->type1))
				continue;
//		const gsimFD* info=data->get_info(k);
		const size_t ni=data->get_odata(k)->rows();
		const size_t np=data->get_odata(k)->cols();
		if (ni<2)
			continue;
		double xgmincurve, xgmaxcurve, ygmincurve, ygmaxcurve;

		xgmincurve=data->get_x(k,0);
		xgmaxcurve=data->get_x(k,np-1);
		if (xgmincurve>xgmaxcurve)
			std::swap(xgmincurve,xgmaxcurve);
		ygmincurve=data->get_y(k,0);
		ygmaxcurve=data->get_y(k,ni-1);
		if (ygmincurve>ygmaxcurve)
			std::swap(ygmincurve,ygmaxcurve);
		if (xgmincurve<xglobmin)
			xglobmin=xgmincurve;
		if (ygmincurve<yglobmin)
			yglobmin=ygmincurve;
		if (xgmaxcurve>xglobmax)
			xglobmax=xgmaxcurve;
		if (ygmaxcurve>yglobmax)
			yglobmax=ygmaxcurve;
	}
}

		
//create initial levels
void Plot3DWidget::initialise_levels()
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	bool has2D=false;
	levels.clear();
	size_t ncurves=data->size();
	double max=-9e36;
	double min=9e36;
	for (size_t k=0; k<ncurves; k++){
		if (!data->get_display(k)->isVisible)
			continue;
		const size_t ni=data->get_odata(k)->rows();
		const size_t np=data->get_odata(k)->cols();

		if (ni>1){
			has2D=true;	
			for (size_t i=ni; i--;)
			for (size_t j=np; j--;) {
				if (data->get_value(k,i,j)>max) max=data->get_value(k,i,j);
				if (data->get_value(k,i,j)<min) min=data->get_value(k,i,j);
			}
		}
	}
	if (!has2D) {
		QApplication::restoreOverrideCursor();
		return;
	}
	double start=0, end=0, range=0;
	double pol=0;
	for (int r=0; r<nlevels-1; r++)
		pol+=std::pow(multiplier, r);
	double smstep;
	switch (pos_neg) {
		case 0://positive only
			start=floor/100.0*max;
			end=ceiling/100.0*max;
			range=end-start;
			smstep=range/pol;
			levels.push_back(start);
			for (int i=1; i<nlevels; i++){
				start+=smstep*pow(multiplier,i-1);
				levels.push_back(start);
			}
			break;
		case 1: //positive/negative
			if (fabs(min)>fabs(max)){
				start=ceiling/100.0*min;
				end=floor/100.0*min;
        			range=end-start;
				smstep=range/pol;
				levels.push_back(end);
				for (int i=1; i<nlevels; i++){
					end-=smstep*pow(multiplier,i-1);
					levels.push_back(end);
				}
				for (size_t i=0; i<nlevels; i++)
					levels.push_back(-levels.at(i));
				}
			else {
				start=floor/100.0*max;
				end=ceiling/100.0*max;
				range=end-start;
				smstep=range/pol;
				levels.push_back(start);
				for (int i=1; i<nlevels; i++){
					start+=smstep*pow(multiplier,i-1);	
					levels.push_back(start);
				}
				for (size_t i=0; i<nlevels; i++)
					levels.push_back(-levels.at(i));
				}
			qSort(levels.begin(), levels.end());
			break;
		case 2://negative only
			start=ceiling/100.0*min;
			end=floor/100.0*min;
			range=end-start;
			smstep=range/pol;
			levels.push_back(end);
			for (int i=1; i<nlevels; i++){
				end-=smstep*pow(multiplier,i-1);
//			double val=log(-start)+range*double(i)/double(nlevels-1.0);
				levels.push_back(end);
			}
			break;
	}

	if (!isRaster) 
		calculate_countours();
	else
		calculate_raster();
	if (hasProjections) createProjections();
	QApplication::restoreOverrideCursor();
}

void Plot3DWidget::fromreal_toscreen (double x, double y, QPointF & point)
{
	QSize s=size();
	fromreal_toscreen(x,y,s,point);
}


void Plot3DWidget::fromreal_toscreen (double x, double y, QSize& win, QPointF & point) //so far the same as for 2D plot
{
	if (is_xaxis_inverted)
		point.setX(win.width()-(x-xwinmin)/xppt);
	else
		point.setX((x-xwinmin)/xppt);
	if (is_yaxis_inverted) 
		point.setY((y-ywinmin)/yppt);
	else
		point.setY(win.height()-(y-ywinmin)/yppt);
}

void Plot3DWidget::fromreal_toscreen (double x, double y, QSize& win, QPoint & point, size_t k) //include shifts and scalings
{
	const Display_* display=data->get_display(k);
	if (is_xaxis_inverted)
		point.setX(int(win.width()-(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt)));
	else
		point.setX(int(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt));
	
	if (is_yaxis_inverted)
		point.setY(int(((y*display->vscale-ywinmin)/yppt)+display->vshift/yppt));
	else
		point.setY(int(win.height()-(((y*display->vscale-ywinmin)/yppt)+display->vshift/yppt)));
}

void Plot3DWidget::fromreal_toscreen (double x, double y, QSize& win, QPointF & point, size_t k) //include shifts and scalings
{
	const Display_* display=data->get_display(k);
	if (is_xaxis_inverted)
		point.setX(win.width()-(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt));
	else
		point.setX(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt);
	
	if (is_yaxis_inverted)
		point.setY(((y*display->vscale-ywinmin)/yppt)+display->vshift/yppt);
	else
		point.setY(win.height()-(((y*display->vscale-ywinmin)/yppt)+display->vshift/yppt));
}

/*
QMatrix Plot3DWidget::indTransMatrix (const size_t k)
{
	const Display_* display=data->get_display(k);
	double xslope, xshift, yslope, yshift;
	if (is_xaxis_inverted) {
		xslope=(-1.0*display->xscale)/xppt;
		xshift=width()+(xwinmin-display->xshift)/xppt;
	}
	else {
		xslope=display->xscale/xppt;
		xshift=(display->xshift-xwinmin)/xppt;
	}

	if (is_yaxis_inverted) {
		yslope=(display->vscale)/yppt;
		yshift=(display->vshift-ywinmin)/yppt;
	}
	else {
		yslope=(-1.0*display->vscale)/yppt;
		yshift=height()+(ywinmin-display->vshift)/yppt;
	}
	QMatrix matrix;
	matrix.translate(xshift, yshift);
	matrix.scale(xslope,yslope);
	return matrix;
}
*/

void Plot3DWidget::draw_picture(QPainter *paint)
{
	if (data->size()) {
		init_settings();
//draw axis
		if (hasProjections){ //restrict drawing area if projections are present
			paint->setClipping(true);
			paint->setClipRegion(QRegion(0,PROJFIELD,width()-PROJFIELD,height()-PROJFIELD));
			}
//Draw actual spectra/FIDs
		draw_extra_graph(paint);
                if (!dirtyContours) {
			if (isRaster) 
				drawRaster(paint);
			else  
                            draw_countours(paint);}
		draw_bars(paint);
		draw_projections(paint);
                draw_axis(paint);
                draw_yaxis(paint);
		draw_integrals(paint);
		draw_legend(paint);
		paint->setClipping(false);
	}
	else
		draw_logo(paint);
}

void Plot3DWidget::fromreal_toindex(size_t k, QPointF pos, size_t& xi, size_t& yi, bool with_shifts)
{ //pos in real coordinates
	if (!data->size()) 
		return;
	size_t xsize=data->get_odata(k)->cols();
	size_t ysize=data->get_odata(k)->rows();
	if (with_shifts) {
		pos.setX((pos.x()-data->get_display(k)->xshift)/data->get_display(k)->xscale);
		pos.setY((pos.y()-data->get_display(k)->vshift)/data->get_display(k)->vscale);
	}
	if (ysize<=1)
		return;  //1D spectrum is selected, not a 2D
	int xi_=lround ((pos.x()-data->get_x(k,0))/(data->get_x(k,xsize-1)-data->get_x(k,0))*(xsize-1));
	int yi_=lround (( pos.y()-data ->get_y(k,0)) / (data->get_y(k,ysize-1) - data->get_y(k,0)) *(ysize-1));	
	if (xi_<0) xi_=0;
	if (yi_<0) yi_=0;
	xi=size_t(xi_);
	yi=size_t(yi_);
	if (xi>=xsize) xi=xsize-1;
	if (yi>=ysize) yi=ysize-1;
}

bool Plot3DWidget::willMarkBePrinted(QPoint pp)
{
	return pp.y()>fontMetrics().height()*2+hasProjections*PROJFIELD && pp.y()<height()-AXOFFSET-TICSLEN-fontMetrics().height();
}

void Plot3DWidget::draw_bars(QPainter *paint)
{
	QSize win=size();
	size_t xi1, xi2, yi1, yi2;
	QPen pen;
	pen.setColor(colourScheme.markerColour);
	paint->setPen(pen);
	if (is_leftbar) {
		draw_bar_lines(paint, leftbarpos);
		fromreal_toindex(data->getActiveCurve(),leftbarpos,xi1,yi1,false);
		paint->drawText(hoffset+50,paint->fontMetrics().height()+labeloffset,QString("Main X:%1 (Col: %2); Y:%3 (Row: %4);  Z:%5").arg(leftbarpos.x()).arg(xi1+1).arg(leftbarpos.y()).arg(yi1+1).arg(data->get_value(data->getActiveCurve(),yi1,xi1)));
		if (!markerLabel1->isVisible() && data->global_options.animatedMarkers) {
			markerMovie1->start();
			markerLabel1->setVisible(true);
		}
	}
	else if (markerLabel1->isVisible() && data->global_options.animatedMarkers) {
		markerMovie1->stop();
		markerLabel1->setVisible(false);
	}
	if (is_rightbar) {
		fromreal_toindex(data->getActiveCurve(),rightbarpos,xi2,yi2,false);
		pen.setColor(colourScheme.markerColour);
		pen.setStyle(Qt::DashLine);
		paint->setPen(pen);
		draw_bar_lines(paint, rightbarpos);
		paint->drawText(hoffset+50,paint->fontMetrics().height()*2+labeloffset,QString("Secondary X:%1 (Col:%2); Y:%3 (Row:%4);  Z:%5").arg(rightbarpos.x()).arg(xi2+1).arg(rightbarpos.y()).arg(yi2+1).arg(data->get_value(data->getActiveCurve(),yi2,xi2)));
		if (is_leftbar)
			paint->drawText(hoffset+50,paint->fontMetrics().height()*3+labeloffset,QString("%5 X:%1 (Rows:%2); Y:%3 (Cols:%4)").arg(leftbarpos.x()-rightbarpos.x()).arg(int(xi1)-int(xi2)).arg(leftbarpos.y()-rightbarpos.y()).arg(int(yi1)-int(yi2)).arg(QChar(0x0394)));
		if (!markerLabel2->isVisible() && data->global_options.animatedMarkers) {
			markerMovie2->start();
			markerLabel2->setVisible(true);
		}
	}
	else if (markerLabel2->isVisible() && data->global_options.animatedMarkers) {
		markerMovie2->stop();
		markerLabel2->setVisible(false);
	}
}

void Plot3DWidget::init_settings()
{
	Plot2DWidget::init_settings();
	size_t k=data->getActiveCurve();
	if (data->get_info(k)->type1==FD_TYPE_FID) {
		is_yaxis_inverted=false;
		switch (data->global_options.ytimeunits) {
			case UNITS_US:
				yaxis_label=QString("us");
				break;
			case UNITS_MS:
				yaxis_label=QString("ms");
				break;
			case UNITS_S:
				yaxis_label=QString("s");
				break;
			case UNITS_TPTS:
                                yaxis_label=QString("pts");
				break;
			default:
				yaxis_label=QString("");
				break;
		}
	}
	else {
		is_yaxis_inverted=true;

		switch (data->global_options.yfrequnits) {
			case UNITS_PPM:
				yaxis_label=QString("ppm");
				break;
			case UNITS_HZ:
				yaxis_label=QString("Hz");
				break;
			case UNITS_KHZ:
				yaxis_label=QString("kHz");
				break;
			case UNITS_FPTS:
				yaxis_label=QString("pts (frq)");
				break;
			default:
				yaxis_label=QString("");
				break;
		}
	}
}

QColor Plot3DWidget::get_color(size_t i)
{
	if (i==data->getActiveCurve())
		return colourScheme.axisColour;
	else
		return colourScheme.plot3DContourColour.at(i);
}


void Plot3DWidget::createProjections()
{
	size_t len=data->size();
	vprojection.create(len);
	hprojection.create(len);
	hprojectionmax.create(len,0.0);
	hprojectionmin.create(len,0.0);
	vprojectionmax.create(len,0.0);
	vprojectionmin.create(len,0.0);
	vpt.create(len,0.0);
	hpt.create(len,0.0);


	for (size_t k=0; k<len; k++) {
		if (data->get_odata(k)->rows()<2) 
			continue;
		if (data->global_options.useSkyline) { //projections are skylines
			vprojection(k).create(data->get_odata(k)->rows(),-9e38);
			hprojection(k).create(data->get_odata(k)->cols(),-9e38);
			for (size_t i=0; i<data->get_odata(k)->rows(); i++)
			for (size_t j=0; j<data->get_odata(k)->cols(); j++) {
				if (data->get_value(k,i,j)>hprojection(k)(j))
					hprojection(k)(j)=data->get_value(k,i,j);
				if (data->get_value(k,i,j)>vprojection(k)(i))
					vprojection(k)(i)=data->get_value(k,i,j);
			}
		}
		else { //projections are sum
			vprojection(k).create(data->get_odata(k)->rows(),0.0);
			hprojection(k).create(data->get_odata(k)->cols(),0.0);
			for (size_t i=0; i<data->get_odata(k)->rows(); i++)
			for (size_t j=0; j<data->get_odata(k)->cols(); j++) {
				hprojection(k)(j)+=data->get_value(k,i,j);
				vprojection(k)(i)+=data->get_value(k,i,j);
			}
		}
		hasProjections=true;
		hprojectionmax(k)=max(hprojection(k));
		hprojectionmin(k)=min(hprojection(k));
		vprojectionmax(k)=max(vprojection(k));
		vprojectionmin(k)=min(vprojection(k));
		vpt(k)=(vprojectionmax(k)-vprojectionmin(k))/PROJFIELD;
		hpt(k)=(hprojectionmax(k)-hprojectionmin(k))/PROJFIELD;
		labeloffset=PROJFIELD;
	}
}

void Plot3DWidget::createSlicesInsteadOfProjections()
{
	size_t len=data->size();
	for (size_t k=0; k<len; k++) {
		if (data->get_odata(k)->rows()<2) 
			continue;
		vprojection(k).create(data->get_odata(k)->rows(),0.0);
		hprojection(k).create(data->get_odata(k)->cols(),0.0);
		size_t r,c;
		fromreal_toindex(k,leftbarpos,c,r);
			
		if (!is_rightbar) { //slices
			for (size_t i=0; i<data->get_odata(k)->rows(); i++)
				vprojection(k)(i)+=data->get_value(k,i,c);
			for (size_t j=0; j<data->get_odata(k)->cols(); j++)
				hprojection(k)(j)+=data->get_value(k,r,j);
			hprojectionmax(k)=max(real(*data->get_odata(k)));
			hprojectionmin(k)=min(real(*data->get_odata(k)));
			vprojectionmax(k)=max(real(*data->get_odata(k)));
			vprojectionmin(k)=min(real(*data->get_odata(k)));
		}
		else {//partial projections
			size_t c1,r1;
			fromreal_toindex(k,rightbarpos,c1,r1);
			if (c>c1)
				std::swap(c,c1);
			if (r>r1)
				std::swap(r,r1);
			if (data->global_options.useSkyline) {
				vprojection(k).create(data->get_odata(k)->rows(),-9e38);//recreate projections with minimal data
				hprojection(k).create(data->get_odata(k)->cols(),-9e38);
				for (size_t i=0; i<data->get_odata(k)->rows(); i++)
				for (size_t j=c; j<=c1; j++)
					if (data->get_value(k,i,j)>vprojection(k)(i))
						vprojection(k)(i)=data->get_value(k,i,j);
				for (size_t j=0; j<data->get_odata(k)->cols(); j++)
				for (size_t i=r; i<=r1; i++)
					if (data->get_value(k,i,j)>hprojection(k)(j))
						hprojection(k)(j)=data->get_value(k,i,j);
			}
			else {
				for (size_t i=0; i<data->get_odata(k)->rows(); i++)
				for (size_t j=c; j<=c1; j++)
					vprojection(k)(i)+=data->get_value(k,i,j);
				for (size_t j=0; j<data->get_odata(k)->cols(); j++)
				for (size_t i=r; i<=r1; i++)
					hprojection(k)(j)+=data->get_value(k,i,j);
			}
			hprojectionmax(k)=max(hprojection(k));
			hprojectionmin(k)=min(hprojection(k));
			vprojectionmax(k)=max(vprojection(k));
			vprojectionmin(k)=min(vprojection(k));
		}
		vpt(k)=(vprojectionmax(k)-vprojectionmin(k))/PROJFIELD;
		hpt(k)=(hprojectionmax(k)-hprojectionmin(k))/PROJFIELD;
	}	
	hasProjections=true;
	labeloffset=PROJFIELD;
}

void Plot3DWidget::deleteProjections() {
		vprojection.clear();
		hprojection.clear();
		hasProjections=false;
		labeloffset=0;
}


void Plot3DWidget::hprojection_toscreen (double x, double y, QPointF & point, size_t k)
{
    const Display_* display=data->get_display(k);
    if (is_xaxis_inverted)
        point.setX(width()-(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt));
    else
	point.setX(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt);
    point.setY(PROJFIELD-(projection_scaler*y-hprojectionmin(k))/hpt(k));
}

void Plot3DWidget::vprojection_toscreen (double x, double y, QPointF & point, size_t k)
{
    const Display_* display=data->get_display(k);

	if (is_yaxis_inverted)
		point.setY(((x*display->vscale-ywinmin)/yppt)+display->vshift/yppt);
	else
		point.setY(height()-(((x*display->vscale-ywinmin)/yppt)+display->vshift/yppt));
    point.setX(width()-PROJFIELD+(projection_scaler*y-vprojectionmin(k))/vpt(k));
}

void Plot3DWidget::draw_projections(QPainter* paint)
{
if (!hasProjections) 
	return;
size_t ncurves=data->size();
//Precheck for 2D data without projections
for (size_t k=0; k<ncurves; k++) {
	if (data->get_odata(k)->rows()>1) {
	if ((hprojection.size()<=k) || (vprojection.size()<=k)) {
		createProjections();
		break;
	}
	if ((!hprojection(k).size()) || ( !vprojection(k).size())) {
		createProjections();
		break;}
	}
}

if (smartSideviews && !mousePressed) { //don't recalculate during simple movement
	if (is_leftbar) 
		createSlicesInsteadOfProjections();
	else
		createProjections();
}

int hprojoffset=hoffset*data->global_options.hasYaxis;
QPen pen=paint->pen();
pen.setColor(colourScheme.axisColour);
if (for_print)
	pen.setWidth(data->global_options.printLineWidth);
paint->setPen(pen);
paint->drawLine(hprojoffset,PROJFIELD,width()-PROJFIELD-1,PROJFIELD);
paint->drawLine(width()-PROJFIELD-1, PROJFIELD, width()-PROJFIELD-1, height()-fontMetrics().height()-TICSLEN-3);


QPointF point;
for (size_t k=0; k<ncurves; k++)
{
if (!data->get_display(k)->isVisible)
	continue;
if (!data->global_options.mixFidSpec)
	if ((data->get_info(k)->type!=data->get_info(data->getActiveCurve())->type) || (data->get_info(k)->type1!=data->get_info(data->getActiveCurve())->type1))
			continue;
//const gsimFD* info=data->get_info(k);
const size_t ni=data->get_odata(k)->rows();

if (ni>1) {
size_t len1=hprojection(k).size();
size_t len2=vprojection(k).size();
    paint->setPen(get_color(k));
    size_t startx, endx, starty, endy;
    fromreal_toindex(k, QPointF(xwinmin,ywinmin),startx,starty);
    fromreal_toindex(k, QPointF(xwinmax,ywinmax),endx, endy);
    if (endx<startx) 
	swap(startx,endx);
    if (startx>1) 
	startx-=2;
    if (endx<len1-1)
    	endx++;
    if (endx>=len1) {
	cerr<<"Endx uups\n";
	return;
	}

    if (endy<starty) 
	swap(starty,endy);
    if (starty>1) 
	starty-=2;
    if (endy<len2-1)
    	endy++;
    if (endy>=len2) {
	cerr<<"Endy uups\n";
	return;
	}

	QPointF old(0,0);
	QPolygonF polygon;
	for (size_t i=startx; i<endx; i++)
    	{
		hprojection_toscreen(data->get_x(k,i), hprojection(k)(i), point, k);
		if (point!=old) {
			polygon<<point;
			old=point;
		}
	}
	paint->setClipRegion(QRegion(hprojoffset,0,width()-hprojoffset-PROJFIELD,PROJFIELD));

	paint->drawPolyline(polygon);
        if (smartSideviews && k==data->getActiveCurve()){
            if (is_leftbar){
			if (is_rightbar) {
				if (data->global_options.useSkyline)
					paint->drawText(QPoint(hprojoffset,paint->fontMetrics().height()),"Partial projections (skyline)");
				else
					paint->drawText(QPoint(hprojoffset,paint->fontMetrics().height()),"Partial projections (sum)");
			}
			else
                            paint->drawText(QPoint(hprojoffset,paint->fontMetrics().height()),"Slices");
                    }
		else	{
			if (data->global_options.useSkyline)
				paint->drawText(QPoint(hprojoffset,paint->fontMetrics().height()),"Projections (skyline)");
			else
				paint->drawText(QPoint(hprojoffset,paint->fontMetrics().height()),"Projections (sum)");
			}
        }
	polygon.clear();
	for (size_t i=starty; i<endy; i++)
    	{
		vprojection_toscreen(data->get_y(k,i), vprojection(k)(i), point, k);
		if (point!=old) {
			polygon<<point;
			old=point;
		}
	}
	paint->setClipRegion(QRegion(width()-PROJFIELD,PROJFIELD,PROJFIELD,height()-PROJFIELD));
	paint->drawPolyline(polygon);
	}
    }
paint->setClipRegion(QRegion(0,PROJFIELD,width()-PROJFIELD,height()-PROJFIELD));
}

void Plot3DWidget::wheelEvent(QWheelEvent *event)
{
	if (!hasProjections)
		Plot2DWidget::wheelEvent(event);
	else
		projection_scaler*=event->delta()/120.0*0.1+1.0;
	update();
}

void Plot3DWidget::keyPressEvent ( QKeyEvent * e)
{
	if (e->key()==Qt::Key_C) {
		if (colourContours) 
			colourContours=false;
		else
			colourContours=true;
		update();
	}

	if (hasProjections) {
		if (e->key()==Qt::Key_PageUp)
			projection_scaler*=1.1;
		else if (e->key()==Qt::Key_PageDown)
			projection_scaler*=0.9;
		update();
	}
	Plot2DWidget::keyPressEvent(e);
}

