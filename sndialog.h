#ifndef SNDIALOG_H
#define SNDIALOG_H

#include <QDialog>

namespace Ui {
class SNDialog;
}

class SNDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SNDialog(QWidget *parent = 0);
    ~SNDialog();
    
private:
    Ui::SNDialog *ui;
};

#endif // SNDIALOG_H
