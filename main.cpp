#include <QApplication>
#include "mainform.h"
#include "version.h"
#include <QMessageBox>
#include <QDebug>
#include <QMdiSubWindow>

//using namespace libcmatrix;
using namespace std;

int main( int argc, char ** argv )
{
try {
     QApplication a( argc, argv );	
	 QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());
     MainForm w;
     int i;
     QString procList;
     QString saveAsSimpson;
     QString saveAsSpinsight;
     QString saveAsMatlab;
     QStringList openFiles;
     for ( i = 1; i < argc; i++ ){  // Command line argument processing
        if (QString(argv[i])==QString("-p")) {
			i++;
            procList=argv[i];
		}
        else if(QString(argv[i])==QString("-ssimplot")) {
			i++;
            saveAsSimpson=argv[i];
		}
        else if(QString(argv[i])==QString("-sspinsight")) {
			i++;
            saveAsSpinsight=argv[i];
		}
        else if(QString(argv[i])==QString("-smatlab")) {
			i++;
            saveAsMatlab=argv[i];
		}
                else if (QString(argv[i])==QString("-v")) {
                    cout<<"GSim version:"<<VERSION<<" compiled on "<<__DATE__<<endl;
                        return 1;
                }
		else
            openFiles<<argv[i];
     }

foreach (QString s, openFiles) { //open requested files
     if (QFile::exists(s))
     	w.fileOpen(s);
     else 
	QMessageBox::critical(0,"Read failed",QString("File %1 doesn't exist").arg(s));
     }
//check attached processing
	QStringList attData, procData;
	w.readAttachedProcessing(attData, procData);
	w.checkAttachedProcessing(attData, procData,openFiles);

if (!procList.isEmpty()) {
	for (size_t i=0; i<w.filesListWidget->count(); i++)
		w.filesListWidget->item(i)->setSelected(true);
	w.loadProcFile(procList);
	w.on_processButton_clicked();
	}
if (!saveAsSimpson.isEmpty()) {
	QString s=saveAsSimpson; //otherwise fileSave changes filename
	w.fileSave(s,QString("SIMPSON spectra or FID"));
}
if (!saveAsSpinsight.isEmpty()) {
	QString s=saveAsSpinsight;
	w.fileSave(s,QString("SPINSIGHT spectra or FID"));
}
if (!saveAsMatlab.isEmpty()) {
	QString s=saveAsMatlab;
	w.fileSave(s,QString("MATLAB files"));
}
    if (saveAsSpinsight.isEmpty() && saveAsSimpson.isEmpty() && saveAsMatlab.isEmpty()) {
    	w.show();
	w.mdiArea->activeSubWindow()->showMaximized(); //bug in Qt when subwindow controls in a strange place? see fileNewWindow for other part
	a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
	int res;
	try {
		res=a.exec();
		return res;
	}
	catch(MatrixException exc){
		QMessageBox::critical(0,"GSim crash", QString("GSim found an unexpected error and will be closed:\n %1").arg(exc.what()));
		return 0;
	}	
	}
    else
	return 0;
}
catch(MatrixException exc){
	QMessageBox::critical(0,"GSim crash", QString("GSim found an unexpected error and will be closed:\n %1").arg(exc.what()));
}
return 1;
}
