#include "plotarea.h"
#include <QMdiArea>
#include <QMessageBox>
#include <QDebug>

PlotArea::PlotArea(QWidget* parent, Qt::WindowFlags fl)
	:QWidget(parent, fl) {
    setupUi(this);
    data= new DataStore(this);
    data->setObjectName("data");
    plot2D->cold_restart(data);
    plot3D->cold_restart(data);
}


PlotArea::~PlotArea()
{
//    delete data;
    // no need to delete child widgets, Qt does it all for us
}
