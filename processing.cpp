#include "processing.h"
#include "mainform.h"
#include <QMessageBox>
#include <QInputDialog>
#include <QDialogButtonBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QtDebug>
#include <QFileDialog>

BaseProcessingFunc::BaseProcessingFunc(DataStore* d, QString n="", QString o="<empty>", QString c="") : QObject()
{
	data=d; //set pointers
	name=n;
	command=c;
	optionString=o;
	option.clear();
	table=NULL;
	row=-1;
	plot2D=NULL;
	plot3D=NULL;
	toTable=true;
	menuAction=new QAction(this);
	menuAction->setText(getName()+" ["+getCommand()+"]");
	menuAction->setIcon(icon());
	connect(menuAction, SIGNAL( triggered() ), this, SLOT( menuAsksOptions()));
}

BaseProcessingFunc::~BaseProcessingFunc()
{
}

//void BaseProcessingFunc::plotsUpdate()
//{
//	cout<<"You forgot to implement plotsUpdate() for this class\n";
//}

void BaseProcessingFunc::setPlots(Plot2DWidget * p2, Plot3DWidget * p3, MainForm* m)
{
	plot2D=p2;
	plot3D=p3;
	mainwindow=m;
}

QIcon BaseProcessingFunc::icon()
{
	QString s=getName();
	QPixmap pix(32,32);
	pix.fill(Qt::transparent);
	QPainter painter;
	painter.begin(&pix);
	if (s.contains("(dir)")) {
		painter.setPen(Qt::darkBlue);
		painter.drawLine(1,29,30,29);
		painter.drawLine(25,27,30,29);
		painter.drawLine(25,31,30,29);
	}
	else if (s.contains("(indir)")) {
		painter.setPen(Qt::darkMagenta);
		painter.drawLine(2,30,2,1);
		painter.drawLine(0,6,2,1);
		painter.drawLine(4,6,2,1);
	}
	int wordCount=s.split(" ", QString::SkipEmptyParts).size();
	if (s.contains("(dir)") || s.contains("(indir)"))
		wordCount-=1;
	s.remove(QChar('a'), Qt::CaseSensitive);
	s.remove(QChar('e'), Qt::CaseSensitive);
	s.remove(QChar('u'), Qt::CaseSensitive);
	s.remove(QChar('i'), Qt::CaseSensitive);
	s.remove(QChar('o'), Qt::CaseSensitive);
	s.remove(QChar('-'), Qt::CaseInsensitive);
	s.remove(QChar('/'), Qt::CaseInsensitive);
	QString ss;
	if (wordCount==1) {
		s.truncate(3);
		ss=s;
	}
	else if (wordCount==2) {
		QStringList l=s.split(" ", QString::SkipEmptyParts);
		l[0].truncate(2);
		l[1].truncate(1);
		ss=l[0]+l[1];
	}
	else if (wordCount>2){
		QStringList l=s.split(" ", QString::SkipEmptyParts);
		l[0].truncate(1);
		l[1].truncate(1);
		l[2].truncate(1);
		ss=l[0]+l[1]+l[2];
	}
	painter.setFont(QFont("Courier",12));
	painter.drawText(QPoint(4,20),ss);
	painter.end();
	return QIcon(pix);
}

void BaseProcessingFunc::placeToTable(QTableWidget* t, const int r)
{
	table=t;
	row=r;
	QTableWidgetItem* newItem = new QTableWidgetItem;
	newItem->setFlags(newItem->flags() | Qt::ItemIsUserCheckable );
	newItem->setCheckState(Qt::Unchecked);
	table->setItem(row,0,newItem);
	QPushButton* applyButton= new QPushButton;
	applyButton->setText(name);
//	applyButton->setIcon(icon());
	QToolButton* interactiveButton = new QToolButton;
//	interactiveButton->setText(QString("..."));
	interactiveButton->setIcon(QIcon(":/images/exec.png"));
	interactiveButton->setMinimumWidth(16);
	table->setCellWidget(row, 1, applyButton);
	table->setCellWidget(row,3, interactiveButton);
	setOptions(optionString);
	connect (applyButton, SIGNAL(clicked()), this, SLOT (applyU()));
	connect (interactiveButton, SIGNAL(clicked()), this, SLOT (prepDoInteractive()));
}

void BaseProcessingFunc::setOptions (QString options)
{
	if (toTable) {
		QTableWidgetItem* newItem = new QTableWidgetItem(options);
		table->setItem(row,2,newItem);
	}
	else
		optionString=options;
	emit stringIsReady();
}

QString BaseProcessingFunc::previousOptionString()
{
    QString string;
    if (toTable)
        string=table->item(row,2)->text();
    else
        string=optionString;
    return string;
}

void BaseProcessingFunc::applyU()
{
	int q=-1;
	QWidget* pressedButton=qobject_cast<QWidget*> (sender());
	if (pressedButton==NULL) {
		qDebug()<<"Non-identified pressed button";
		return;
	}
	for (size_t i=0; i<table->rowCount(); i++)
		if (pressedButton==table->cellWidget(i,1))
			q=i;
	if (q<0) {
		qDebug()<<"Pressed button is not in the table";
		return;
	}
//	qDebug()<<"Row set to"<<q;
	row=q;
//	row=table->rowAt(table->mapFromGlobal(QCursor::pos()).y()); //update current row position, needed if several similar objects exist
	makeBackUp();
//	plot3D->deleteProjections();
	try {
		data->mutex.lock();
		applyInt(row);
		data->mutex.unlock();
		plotsUpdate();
	}
	catch (MatrixException exc) {
		data->mutex.unlock();
		QApplication::restoreOverrideCursor();
		data->undo();
		QMessageBox::critical(0, name, exc.what());
	}
}

void BaseProcessingFunc::applyInt(int r)
{
	row=r;
	readOptions();
//	qDebug()<<"row"<<r;
//	qDebug()<<"1"<<optionString;
	foreach (int k, data->selectedCurves) {
		apply(k);
//		qDebug()<<"2"<<optionString;
		gsimFD info=*(data->get_info(k));
		if (!info.processing.isEmpty())
			info.processing+=" + ";
		info.processing +=command;
		info.processing +=" ";
		info.processing +=optionString;
		data->set_info(k,info);
	}
}

void BaseProcessingFunc::menuAsksOptions()
{
	toTable=false;
	connect (this, SIGNAL(stringIsReady()), this, SLOT(applyThroughMenu()));
	try {
		doInteractive();
	}
	catch (MatrixException exc) {
		disconnect (this, SIGNAL(stringIsReady()), this, SLOT(applyThroughMenu()));
		QMessageBox::critical(0, name, exc.what());
	}
}

void BaseProcessingFunc::applyThroughMenu()
{
//	cout<<"entered applyThroughMenu\n";
	disconnect (this, SIGNAL(stringIsReady()),0,0);
	QString opt=optionString;
	if (opt.isEmpty())
		return;
	opt.remove(QChar(' '));
	option=opt.split(";", QString::SkipEmptyParts);
	try {
		makeBackUp();
		data->mutex.lock();
		foreach (int k, data->selectedCurves){
			apply(k);
			gsimFD info=*(data->get_info(k));
			if (!info.processing.isEmpty())
				info.processing+=" + ";
			info.processing +=command;
			info.processing +=" ";
			info.processing +=optionString;
			data->set_info(k,info);
		}
		data->mutex.unlock();
		plotsUpdate();
	}
	catch (MatrixException exc) {
		data->mutex.unlock();
		QApplication::restoreOverrideCursor();
		data->undo();
		QMessageBox::critical(0, name, exc.what());
	}
	toTable=true;
}


void BaseProcessingFunc::plotsUpdate()
{
	plot2D->cold_restart(data);
	plot3D->cold_restart(data);
	emit changeInfo();
}

void BaseProcessingFunc::readOptions()
{
	if (table->item(row,2)==NULL)
		return;
	QString opt=table->item(row,2)->text();
	optionString=opt; //save a copy in the global optionString (needed for info about applied processing)
	if (opt.isEmpty())
		return;
	opt.remove(QChar(' '));
	option=opt.split(";", QString::SkipEmptyParts);
}

double BaseProcessingFunc::getDouble(size_t i, bool& ok)
{
	double value=option.at(i).toDouble(&ok);
	if (!ok)
		optionError(QString("In function %1 option no. %2 should be a float number").arg(name).arg(i+1));
	return value;
}

double BaseProcessingFunc::getDouble(size_t i, bool& ok, int units, size_t k, bool direct, bool deltaonly)
{
        if (i>=option.size())
            qDebug()<<"BaseProcessingFunc::getDouble index i out of option.size";
        QString s=option.at(i);
        double value, oldval;
        const gsimFD* info=data->get_info(k);
        size_t np;
        int oldunits;
        if (direct)
            np=data->get_odata(k)->cols();
        else
            np=data->get_odata(k)->rows();
        if (s.endsWith("pts")){
            s.chop(3);
            oldunits=UNITS_TPTS;
        }
        else if(s.endsWith("Hz")){
            s.chop(2);
            oldunits=UNITS_HZ;
        }
        else if (s.endsWith("kHz")){
            s.chop(3);
            oldunits=UNITS_KHZ;
        }
        else if (s.endsWith("ppm")){
            s.chop(3);
            oldunits=UNITS_PPM;
        }
        else if (s.endsWith("us")){
            s.chop(2);
            oldunits=UNITS_US;
        }
        else if (s.endsWith("ms")){
            s.chop(2);
            oldunits=UNITS_MS;
        }
        else if (s.endsWith("s")){
            s.chop(1);
            oldunits=UNITS_S;
        }
        else
            oldunits=units;
        oldval=s.toDouble(&ok);
        if (!ok)
                optionError(QString("In function %1 option no. %2 should be a float number").arg(name).arg(i+1));
        value=convertUnits(oldval,oldunits,units,info,np,direct,deltaonly);
        return value;
}

int BaseProcessingFunc::getInt(size_t i, bool& ok)
{
	int res= zToInt(option.at(i), &ok);
	if (!ok)
		optionError(QString("In function %1 option no. %2 should be an integer number").arg(name).arg(i+1));
	return res;
}

void BaseProcessingFunc::makeBackUp()
{
	data->makeBackup();
}

void BaseProcessingFunc::prepDoInteractive()
{
	int q=-1;
	QWidget* pressedButton=qobject_cast<QWidget*> (sender());
	if (pressedButton==NULL) {
		qDebug()<<"Non-identified pressed button";
		return;
	}
	for (size_t i=0; i<table->rowCount(); i++)
		if (pressedButton==table->cellWidget(i,3))
			q=i;
	if (q<0) {
		qDebug()<<"Pressed button is not in the table";
		return;
	}
//	qDebug()<<"Row set to"<<q;
	row=q;
//	row=table->rowAt(table->mapFromGlobal(QCursor::pos()).y()); //update current row position, needed if several similar objects exist (is not stable under Exceed
	try {
		doInteractive();
	}
	catch (MatrixException exc) {
		QMessageBox::critical(0, name, exc.what());
	}
}

void BaseProcessingFunc::doInteractive()
{
	if (toTable)
		QMessageBox::information(0,"Interactive editing", "This function doesn't require any options");
	setOptions("<none>");
}

void BaseProcessingFunc::apply(int)
{
	QMessageBox::critical(0,"Apply function fault", "This function is not implemented");
}


void BaseProcessingFunc::optionError(QString message)
{
	QString s=QString("Wrong options: ")+message;
    throw Failed(s.toLatin1().data());
}

void SetSizeDirProc::apply(int k)
{
	if (!data->size()) 
		return;
//	size_t k=data->getActiveCurve();
//	gsimFD info=*(data->get_info(k));
	const size_t ni=data->get_odata(k)->rows();
	if (option.size()!=1)
		optionError("Wrong number of options");
	bool ok=true;
	size_t hor=getInt(0,ok);
	if (!ok) return;	
	size_t ver=ni;
	if (!ver)
		ver=1;
	QApplication::setOverrideCursor(Qt::WaitCursor);
	data->resize(k,ver,hor);
	QApplication::restoreOverrideCursor();
}


void SetSizeDirProc::doInteractive()
{
	size_t k=data->getActiveCurve();
	bool ok;
	int s1=data->get_odata(k)->cols();
        int size1 = QInputDialog::getInt(0, tr("Resize"),
                                         tr("New size in direct dimension"), s1, 0, 99999999, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");
	optionString=QString("%1").arg(size1);
	setOptions(optionString);
}

void SetSizeIndirProc::apply(int k)
{
	if (!data->size()) 
		return;
//	size_t k=data->getActiveCurve();
//	gsimFD info=*(data->get_info(k));
	const size_t np=data->get_odata(k)->cols();
	if (option.size()!=1)
		optionError("Wrong number of options");
	bool ok=true;
	size_t ver=getInt(0,ok);
	if (!ok) return;
	size_t hor=np;
	QApplication::setOverrideCursor(Qt::WaitCursor);
	data->resize(k,ver,hor);
	QApplication::restoreOverrideCursor();
	
}

void SetSizeIndirProc::doInteractive()
{
	size_t k=data->getActiveCurve();
	bool ok;
	int s1=data->get_odata(k)->rows();
        int size1 = QInputDialog::getInt(0, tr("Resize"),
                                         tr("New size in indirect dimension"), s1, 0, 99999999, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");
	optionString=QString("%1").arg(size1);
	setOptions(optionString);
}

void SetSizeIndirProc::plotsUpdate()
{
	BaseProcessingFunc::plotsUpdate();
	emit changeFilesList();
}

void DirShiftProc::apply(int k)
{
	if (!data->size()) return;
//	size_t k=data->getActiveCurve();
	size_t np=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if (ni<1) ni=1;

	if (option.size()!=1)
		optionError("Wrong number of options");
	bool ok=true;
	size_t shift=getInt(0,ok);
	if (shift>np)
		optionError("Option (size) should be less then np");
	doShift(shift,k);
}

void DirShiftProc::doShift(int shift, int k)
{
//	size_t k=data->getActiveCurve();
	size_t np=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if (ni<1) ni=1;
	QApplication::setOverrideCursor(Qt::WaitCursor);
	QList<complex> temp;
	for (size_t i=0; i<ni; i++) { //for each column...
	for (size_t j=0; j<shift; j++)
		temp.append(data->get_odata(k,i,j)); //copy starting points to temp;
	for (size_t j=shift; j<np; j++)
		data->set_odata(k,i,j-shift,data->get_odata(k,i,j));
	for (size_t j=np-shift; j<np; j++)
		data->set_odata(k,i,j, temp.at(j-np+shift));
	temp.clear();
	if (data->hasHyperData(k)){
		for (size_t j=0; j<shift; j++)
			temp.append(data->get_hdata(k,i,j)); //copy starting points to temp;
		for (size_t j=shift; j<np; j++)
			data->set_hdata(k,i,j-shift,data->get_hdata(k,i,j));
		for (size_t j=np-shift; j<np; j++)
			data->set_hdata(k,i,j, temp.at(j-np+shift));
		temp.clear();	
	}
	}
	QApplication::restoreOverrideCursor();
}

void DirShiftProc::plotsUpdate()
{
	plot2D->Update();
	plot3D->Update();	
}

void IndirShiftProc::apply(int k)
{
	if (!data->size()) return;
	size_t np=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if (ni<2) 
		optionError("Shift in indirect dimension is possible for 2D data only");
	if (option.size()!=1)
		optionError("Wrong number of options");
	bool ok=true;
	size_t shift=getInt(0,ok);
	if (shift>ni)
		optionError("Option (size) should be less then ni");
	QApplication::setOverrideCursor(Qt::WaitCursor);
	QList<complex> temp;
	for (size_t i=0; i<np; i++) { //for each row...
	for (size_t j=0; j<shift; j++)
		temp.append(data->get_odata(k,j,i)); //copy starting points to temp;
	for (size_t j=shift; j<ni; j++)
		data->set_odata(k,j-shift,i,data->get_odata(k,j,i));
	for (size_t j=ni-shift; j<ni; j++)
		data->set_odata(k,j,i, temp.at(j-ni+shift));
	temp.clear();
	if (data->hasHyperData(k)){
		for (size_t j=0; j<shift; j++)
			temp.append(data->get_hdata(k,j,i)); //copy starting points to temp;
		for (size_t j=shift; j<ni; j++)
			data->set_hdata(k,j-shift,i,data->get_hdata(k,j,i));
		for (size_t j=ni-shift; j<ni; j++)
			data->set_hdata(k,j,i, temp.at(j-ni+shift));
		temp.clear();
	}
	}
	QApplication::restoreOverrideCursor();
}

void DirShiftProc::doInteractive()
{
	size_t k=data->getActiveCurve();
	bool ok;
	int s1=data->get_odata(k)->cols();
        int size1 = QInputDialog::getInt(0, tr("Shift"),
                                         tr("Number of points for shift"), s1, 0, s1, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");
	optionString=QString("%1").arg(size1);
	setOptions(optionString);
}

void DirLBProc::apply(int k) {
	if (!data->size()) return;

	if (option.size()<1 || option.size()>3)
		optionError("Wrong number of options");

	QString type=option.at(0);
	if (!(type==QString("lorentz") || type==QString("gauss") || type==QString("shift_gauss") || type==QString("shift_lorentz") || type==QString("bruker_lorentz") || type==QString("bruker_gauss") || type==QString("sinebell") || type==QString("sq_sinebell")))
		optionError("Option 1 (type) is not supported");
	bool ok=true;
	double coeff=getDouble(1,ok);
	double coeff2=0.0;
	if (type==QString("shift_gauss") ||type==QString("shift_lorentz") )
		coeff2=getDouble(2,ok);
	if ((type!=QString("sinebell")) && (type!=QString("sq_sinebell"))){
		if (data->global_options.xtimeunits==UNITS_MS) {
			coeff*=1e3;
			coeff2/=1e3;
		}
		if (data->global_options.xtimeunits==UNITS_S) {
			coeff*=1e6;
			coeff2/=1e6;
		}
	}
	else
                coeff*=M_PI/180.0;// to radians for sinebell
	QApplication::setOverrideCursor(Qt::WaitCursor);
	size_t rows=data->get_odata(k)->rows();
	if (!rows) rows=1;
	size_t pts=data->get_odata(k)->cols();
	double scaler=1.0;
	double shift_t=0.0;
	if ((type==QString("bruker_lorentz")) || type==QString("bruker_gauss")) {
		shift_t=1e6*findShift(k)/data->get_info(k)->sw;
                //cout<<"Shift in us:"<<shift_t<<endl;
		if (data->global_options.xtimeunits==UNITS_MS)
			shift_t/=1e3;
		if (data->global_options.xtimeunits==UNITS_S)
		shift_t/=1e6;
                //cout<<"Shift in current units:"<<shift_t<<endl;
	}
	for (size_t m=0; m<rows; m++)
	for (size_t i=0; i<pts; i++){
		if (type=="lorentz")
			scaler = exp(data->get_x(k,i)/1e6*coeff*M_PI*(-1.0)); 
		else if (type=="gauss")
			scaler = exp((data->get_x(k,i)/1e6*coeff)*(data->get_x(k,i)/1e6*coeff)*M_PI*(-1.0));
		else if (type=="shift_gauss")
			scaler = exp(((data->get_x(k,i)-coeff2)/1e6*coeff)*((data->get_x(k,i)-coeff2)/1e6*coeff)*M_PI*(-1.0));
		else if (type=="shift_lorentz")
			scaler = exp(fabs(coeff2-data->get_x(k,i))/1e6*coeff*M_PI*(-1.0));
		else if (type=="bruker_lorentz")
			scaler = exp(fabs(shift_t-data->get_x(k,i))/1e6*coeff*M_PI*(-1.0));
		else if (type=="bruker_gauss")
			scaler = exp(((data->get_x(k,i)-shift_t)/1e6*coeff)*((data->get_x(k,i)-coeff2)/1e6*coeff)*M_PI*(-1.0));
		else if (type=="sinebell")
			scaler = sin(coeff+(M_PI-coeff)*i/(pts-1));
		else if (type=="sq_sinebell")
			scaler = sin(coeff+(M_PI-coeff)*i/(pts-1))*sin(coeff+(M_PI-coeff)*i/(pts-1));
		data->set_odata(k,m,i, data->get_odata(k,m,i)*scaler);
};
	QApplication::restoreOverrideCursor();
}


double autoBruker::findShift(int k)
{
QHash< int,QHash< int,double > > t; //table t[DSPFVS][DECIM]=shift
//see http://sbtools.uchc.edu/help/nmr/nmr_toolkit/bruker_dsp_table.asp
t[10][2]=44.75;
t[10][3]=33.5;
t[10][4]=66.625;
t[10][6]=59.0833;
t[10][8]=68.5625;
t[10][12]= 60.375;
t[10][16]=69.5313;
t[10][24]= 61.0208;
t[10][32]= 70.0156;
t[10][48]= 61.3438;
t[10][64]= 70.2578;
t[10][96]= 61.5052;
t[10][128]= 70.3789;
t[10][192]= 61.5859;
t[10][256]= 70.4395;
t[10][384]= 61.6263;
t[10][512]= 70.4697;
t[10][768]= 61.6465;
t[10][1024]= 70.4849;
t[10][1536]= 61.6566;
t[10][2048]= 70.4924;
t[11][2]= 46.0;
t[11][3]= 36.5;
t[11][4]= 48.0;
t[11][6]= 50.1667;
t[11][8]= 53.25;
t[11][12]= 69.5;
t[11][16]=72.25;
t[11][24]= 70.1667;
t[11][32]= 72.75;
t[11][48]= 70.5;
t[11][64]= 73.0;
t[11][96]= 70.6667;
t[11][128]= 72.5;
t[11][192]= 71.3333;
t[11][256]= 72.25;
t[11][384]= 71.6667;
t[11][512]= 72.125;
t[11][768]= 71.8333;
t[11][1024]= 72.0625;
t[11][1536]= 71.9167;
t[11][2048]= 72.0313;
t[12][2]= 46.311;
t[12][3]= 36.53;
t[12][4]= 47.87;
t[12][6]= 50.229;
t[12][8]= 53.289;
t[12][12]= 69.551;
t[12][16]= 71.6;
t[12][24]= 70.184;
t[12][32]= 72.138;
t[12][48]= 70.528;
t[12][64]= 72.348;
t[12][96]= 70.7;
t[12][128]= 72.524;
t[13][2]= 2.75;
t[13][3]= 2.833;
t[13][4]= 2.875;
t[13][6]= 2.917;
t[13][8]= 2.938;
t[13][12]= 2.958;
t[13][16]=2.969;
t[13][24]=2.979;
t[13][32]=2.984;
t[13][48]=2.989;
t[13][64]=2.992;
t[13][96]=2.995;
//experimentally determined from the newer Bruker consoles
t[20][2496]=68;
t[22][40]=20;
t[22][245]=20;
	int q=-1;
	for (size_t i=0; i<data->get_info(k)->parameterFiles.size(); i++)
		if (data->get_info(k)->parameterFiles.at(i)==QString("acqus"))
			q=i;
	if (q<0) {
		QMessageBox::warning(0,"autoBruker","Parameters from 'acqus' file are not found");
		return 0;
	}
//	qDebug()<<"q found:"<<q;
	QHash<QString, QString> par=data->get_info(k)->allParameters[q];
//	qDebug()<<"ok";
        double grpdly=double(par.value("GRPDLY").toDouble());
        if (grpdly>0.0){
            return grpdly;
        }
	int decim=int(par.value("DECIM").toDouble());
//	qDebug()<<"DECIM:"<<decim;
	if (!decim) {
		QMessageBox::warning(0, "autoBruker","DECIM is not found");
		return 0;
	}
	int dspfvs=par.value("DSPFVS").toInt();
	if (!dspfvs) {
		QMessageBox::warning(0,"autoBruker","DSPFVS is not found");
		return 0;
	}
//	qDebug()<<"DSPFVS:"<<dspfvs;
	if (!t[dspfvs][decim]) {
		QMessageBox::warning(0,"autoBruker",QString("A combination of DSPFVS=%1 and DECIM=%2 from 'acqus' is unsupported\nUse Shift(dir) to correct Bruker FID manually").arg(dspfvs).arg(decim));
		return 0;
	}
//	qDebug()<<"Shift found:"<<t[dspfvs][decim];
	return t[dspfvs][decim];
}


/* //Simplified, but not working properly
double autoBruker::findShift(int k)
{
//int dec[]={2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512, 768, 1024, 1536, 2048};
//double ds10[]={44.75, 33.5, 66.625, 59.0833, 68.5625, 60.375, 69.5313, 61.0208, 70.0156, 61.3438, 70.2578, 61.5052, 70.3789, 61.5859, 70.4395, 61.6263, 70.4697, 61.6465, 70.4849, 61.6566, 70.4924};
//double ds11[]= {46.0, 36.5, 48.0, 50.1667, 53.25, 69.5, 72.25, 70.1667, 72.75, 70.5, 73.0, 70.6667, 72.5, 71.3333, 72.25, 71.6667, 72.125, 71.8333, 72.0625, 71.9167, 72.0313};
//double ds12[]= {46.311, 36.53, 47.87, 50.229, 53.289, 69.551, 71.6, 70.184, 72.138, 70.528, 72.348, 70.7, 72.524};

//	size_t k=data->getActiveCurve();
		int q=-1;
	for (size_t i=0; i<data->get_info(k)->parameterFiles.size(); i++)
		if (data->get_info(k)->parameterFiles.at(i)==QString("acqus"))
			q=i;
	if (q<0) {
		QMessageBox::warning(0,"autoBruker","Parameters from 'acqus' file are not found");
		return 0;
	}
	qDebug()<<"q found:"<<q;
	QHash<QString, QString> par=data->get_info(k)->allParameters[q];
	qDebug()<<"ok";
	int decim=par.value("DECIM").toInt();
	qDebug()<<"DECIM:"<<decim;
	if (!decim) {
		QMessageBox::warning(0, "autoBruker","DECIM is not found");
		return 0;
	}
	int dspfvs=par.value("DSPFVS").toInt();
	if (!dspfvs) {
		QMessageBox::warning(0,"autoBruker","DSPFVS is not found");
		return 0;
	}
	qDebug()<<"DSPFVS:"<<dspfvs;

	bool is_power_of2;
	double x=double(decim);
	while (x>1.0) x/=2.0;
	if (x==1.0) is_power_of2=true;
	else is_power_of2=false;

	double points=(is_power_of2)? (70.5-15.5/decim) : (185.0/3.0-15.5/decim);

	qDebug()<<"Shift found:"<<points;
	return points;
}
*/

void autoBruker::apply(int k)
{
	if (!data->size())
		return;
//	size_t k=data->getActiveCurve();
	if (data->get_info(k)->file_filter->name()!=QString("Bruker XWIN-NMR FID"))
		return;
	int shift=(int) round(findShift(k));
	if (!shift)
		QMessageBox::warning(0,"autoBruker","DECIM from 'acqus' has unsupported value");
        //cout<<"Shift determined:"<<shift<<endl;
	doShift(shift,k);
}

void autoBruker::doInteractive()
{
	BaseProcessingFunc::doInteractive();
}

void DirLBProc::doInteractive()
{
	QString optString;
	QStringList items;
	if (getName()=="LB (dir)")
        	items << tr("lorentz")<<tr("gauss")<<tr("shift_lorentz")<<tr("shift_gauss")<<tr("bruker_lorentz")<<tr("bruker_gauss")<<tr("sinebell")<<tr("sq_sinebell");
	else
		items<<tr("lorentz")<<tr("gauss")<<tr("sinebell")<<tr("sq_sinebell");

	promptDialog* d=new promptDialog(plot2D);
	d->setWindowTitle("Linebroadening parameters");
	QComboBox* typeBox=new QComboBox(d);
	typeBox->addItems(items);
	
	QDoubleSpinBox* broadBox=new QDoubleSpinBox(d);
	broadBox->setValue(100.0);
	broadBox->setMaximum(1e10);
	broadBox->setMinimum(-1e10);
	broadBox->setSuffix(" Hz");
	broadBox->setDecimals(1);
	
	QDoubleSpinBox* shiftBox=new QDoubleSpinBox(d);
	shiftBox->setValue(100.0);
	shiftBox->setMaximum(1e10);
	shiftBox->setMinimum(-1e10);
	shiftBox->setSuffix(" us");
	shiftBox->setDecimals(1);

	d->putLayout(hBlock(d,"Apodisation function:",typeBox));
	lbLayout=hBlock(d,"Linebroadening",broadBox);
	d->putLayout(lbLayout);
	shiftLayout=hBlock(d,"Shift",shiftBox);
	d->putLayout(shiftLayout);

	for (size_t i=0; i<shiftLayout->count(); i++)
		shiftLayout->itemAt(i)->widget()->setVisible(false);
	
	connect(typeBox, SIGNAL(currentIndexChanged(QString)),this,SLOT(typeChanged(QString)));
	
	int res=d->exec();
	if (res==QDialog::Rejected)
		throw Failed("Interupted by user");

	QString item=typeBox->currentText();
	optString=item+QString(";");
	double hz=broadBox->value();
	optString+=QString("%1").arg(hz);
	double shift=shiftBox->value();
	if (item==QString("shift_gauss") ||item==QString("shift_lorentz") )
		optString+=QString(";%1").arg(shift);
	setOptions(optString);
	delete d;
}

void DirLBProc::typeChanged(QString type)
{
	QLabel* l=qobject_cast<QLabel*>(lbLayout->itemAt(0)->widget());
	QDoubleSpinBox* b=qobject_cast<QDoubleSpinBox*>(lbLayout->itemAt(1)->widget());
	if ((type=="sinebell")||(type=="sq_sinebell")){
		l->setText("Phase angle");
		b->setSuffix(" deg");
		b->setValue(0.0);
	}
	else {
		l->setText("Linebroadening");
		b->setSuffix(" Hz");
		b->setValue(100.0);
	}
	if ((type=="shift_lorentz") || (type=="shift_gauss")) 
		for (size_t i=0; i<shiftLayout->count(); i++)
			shiftLayout->itemAt(i)->widget()->setVisible(true);
	else
		for (size_t i=0; i<shiftLayout->count(); i++)
			shiftLayout->itemAt(i)->widget()->setVisible(false);
}

void IndirLBProc::apply(int k) {
	if (!data->size()) return;
	const size_t ni=data->get_odata(k)->rows();
	const size_t np=data->get_odata(k)->cols();

	if (option.size()!=2)
		optionError("Wrong number of options");
	if (ni<2)
		optionError("Linebroadening in indirect dimension is possible for 2D data only");
	QString type=option.at(0);
	if (!(type==QString("lorentz")|| type==QString("gauss") ||type==QString("sinebell") || type==QString("sq_sinebell"))) 
		optionError("Option 1 (type) is not supported");
	bool ok=true;
	double coeff=getDouble(1,ok);
	if (!ok) 
		return;
	if ((type!=QString("sinebell"))&&(type!=QString("sq_sinebell"))) {
		if (data->global_options.ytimeunits==UNITS_MS) {
			coeff*=1e3;
		}
		if (data->global_options.ytimeunits==UNITS_S) {
			coeff*=1e6;
		}
	}
	else
		coeff*=M_PI/180.0;
	QApplication::setOverrideCursor(Qt::WaitCursor);
	size_t rows=ni;
	if (!rows)
		optionError("Indirect LB cannot be applied on 1D dataset");
	size_t pts=np;
	double scaler=1;

	for (size_t m=0; m<pts; m++)
	for (size_t i=0; i<rows; i++){
		if (type=="lorentz")
			scaler = exp(data->get_y(k,i)/1e6*coeff*M_PI*(-1.0));
		else if (type=="gauss")
			scaler = exp((data->get_y(k,i)/1e6*coeff)*(data->get_y(k,i)/1e6*coeff)*M_PI*(-1.0));
		else if (type=="sinebell") 
			scaler = sin(coeff+(M_PI-coeff)*i/(rows-1));
		else if (type=="sq_sinebell")
			scaler = sin(coeff+(M_PI-coeff)*i/(rows-1))*sin(coeff+(M_PI-coeff)*i/(rows-1));
		data->set_odata(k,i,m, data->get_odata(k,i,m)*scaler);
}
	QApplication::restoreOverrideCursor();
}


size_t DirFtProc::next_power2(const size_t old)
{
	size_t result=16;//minimal 
	while(old>result)
		result*=2;
	return result;
}

void DirFtProc::FourierTrans(int dim, FT_MODE mode, int k)
{
	if (!data->size()) return;
	if ((!dim) && (!is_power2(data->get_odata(k)->cols()))) //direct dimension is not power of n
		data->resize(k,data->get_odata(k)->rows(),next_power2(data->get_odata(k)->cols()));
	if ((dim==1) && (!is_power2(data->get_odata(k)->rows()))) //indirect dimension is not power of n
		data->resize(k,next_power2(data->get_odata(k)->rows()),data->get_odata(k)->cols());

	gsimFD info=*(data->get_info(k));
	Display_ disp=*(data->get_display(k));
	const size_t n=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if(!ni) ni=1;
	if (ni<2 && dim==1) return; //don't do indirect FT for 1D spectra
	double scaler=1.0;
	int ft_direction;

	switch (dim) {

	case 0:
		ft_direction=(info.type==FD_TYPE_FID)? FT_FORWARD : FT_BACKWARD;
		if (ft_direction==FT_BACKWARD)
			halfshift(*data->get_odata(k));
		if (abs(data->get_odata(k,0,0))) //prevent division on zero (can causes problems)
			scaler=(abs(data->get_odata(k,0,0))+abs(data->get_odata(k,0,n-1)))/(2*abs(data->get_odata(k,0,0)));
		else
			scaler=1.0;
		fft_ip(*data->get_odata(k), ft_direction, scaler);
		if (ft_direction==FT_FORWARD)
			halfshift(*data->get_odata(k));
		if (info.file_filter)
			if (info.file_filter->invertAfterFT())
				invert(*(data->get_odata(k)), true);

		if (info.type==FD_TYPE_FID) {
			info.type=FD_TYPE_SPE;
		}
		else {
			info.type=FD_TYPE_FID;
		}
	break;

	case 1:
		if (mode==FT_ECHOANTIECHO) {
			EchoAntiechoResort(k); //resort data
			mode=FT_STATES; //continue as if STATES, see http://rmn.iqfr.csic.es/guide/nmr/smanual/8_4_proc2d.html#Echo-Antiecho
		}
		if ((mode==FT_STATES) || (mode==FT_STATESTPPI)) {//resort data if States mode is required
			StatesResort(k);
			info=*(data->get_info(k));
			ni=data->get_odata(k)->rows();
			if(!ni) ni=1;
		}
		if (mode==FT_STATESTPPI) {
			if (!ni%2)
				throw Failed("NI should be an integer of 2 for STATES_TPPI");
			for (size_t i=1; i<ni;i+=2)
				for (size_t j=0; j<data->get_odata(k)->cols(); j++) {
					complex a=-1*data->get_odata(k,i,j);
					data->set_odata(k,i,j,a);
					if (data->hasHyperData(k)) {
						complex b=-1*data->get_hdata(k,i,j);
						data->set_hdata(k,i,j,b);
					}
				}
		}
		if (mode==FT_TPPI)
			ZeroImag(k);
		(info.type1==FD_TYPE_FID)? ft_direction=FT_FORWARD : ft_direction=FT_BACKWARD;
		if (abs(data->get_odata(k,0,0)))
			scaler=(abs(data->get_odata(k,0,0))+abs(data->get_odata(k,ni-1,0)))/(2*abs(data->get_odata(k,0,0)));
		else
			scaler=1.0;
		(*data->get_odata(k)).transpose();
		fft_ip(*data->get_odata(k),ft_direction, scaler);
		halfshift(*data->get_odata(k));
		(*data->get_odata(k)).transpose();

		if (data->hasHyperData(k)) { //transforming hypercomplex data
			//cout<<"Hypercomplex data will be transformed\n";
			if (abs(data->get_hdata(k,0,0)))
				scaler=(abs(data->get_hdata(k,0,0))+abs(data->get_hdata(k,ni-1,0)))/(2*abs(data->get_hdata(k,0,0)));
			else
				scaler=1.0;
			(*data->get_hdata(k)).transpose();
			fft_ip(*data->get_hdata(k),ft_direction, scaler);
			halfshift(*data->get_hdata(k));
			(*data->get_hdata(k)).transpose();
		}

		if (mode==FT_TPPI) { //in case of TPPI remove half of data and update info
			data->resize(k, ni/2,n);
		info=*(data->get_info(k));
		ni=data->get_odata(k)->rows();
		if(!ni) ni=1;
		}
		if (info.file_filter)
			if (info.file_filter->invertAfterFT()) {
				invert(*(data->get_odata(k)), false);
				if (data->hasHyperData(k))
					invert(*(data->get_hdata(k)), false);
		}
		if (info.type1==FD_TYPE_FID)
			info.type1=FD_TYPE_SPE;
		else 
			info.type1=FD_TYPE_FID;
	break;
	}
	data->set_info(k, info);
	data->set_display(k,disp);
	data->updateAxisVectors();
}

void DirFtProc::apply(int k)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	FourierTrans(0, FT_COMPLEX, k);
	QApplication::restoreOverrideCursor();
}

//void DirFtProc::plotsUpdate()
//{
//     plot2D->cold_restart(data);
//     plot3D->cold_restart(data);
//     emit changeInfo();
//}

bool DirFtProc::is_power2(size_t n)
{
	double x=double(n);
	while (x>1.0) x/=2.0;
	if (x==1.0) return true;
	return false;
}

void DirFtProc::StatesResort(size_t k)
{
     size_t n=data->get_odata(k)->cols();
     size_t ni=data->get_odata(k)->rows();
     cmatrix newm(ni/2,n,complex(0.0,0.0));
     cmatrix hyperm(ni/2,n,complex(0.0,0.0));
     for (size_t j=0; j<n; j++)
     for (size_t i=0; i<ni/2; i++){
         //newm(i,j).re=data->get_odata(k,2*i,j).re;
         real(newm(i,j),real(data->get_odata(k,2*i,j)));
     //newm(i,j).im=data->get_odata(k,2*i+1,j).re;
     imag(newm(i,j),real(data->get_odata(k,2*i+1,j)));
     }
     if (data->global_options.useHypercomplex) {
      for (size_t j=0; j<n; j++)
            for (size_t i=0; i<ni/2; i++){
            //hyperm(i,j).re=data->get_odata(k,2*i,j).im;
            real(hyperm(i,j),imag(data->get_odata(k,2*i,j)));
            //hyperm(i,j).im=data->get_odata(k,2*i+1,j).im;
            imag(hyperm(i,j),imag(data->get_odata(k,2*i+1,j)));
            }
     }
     data->resize(k, ni/2,n);
     data->set_odata(k,newm);
     if (data->global_options.useHypercomplex)
      data->set_hdata(k,hyperm);
}

void DirFtProc::EchoAntiechoResort(size_t k)
{
     size_t n=data->get_odata(k)->cols();
     size_t ni=data->get_odata(k)->rows();
     cmatrix newm(ni,n,complex(0.0,0.0));
     for (size_t j=0; j<n; j++)
     for (size_t i=0; i<ni-1; i=i+2){
	     //newm(i,j).re=-data->get_odata(k,i+1,j).im-data->get_odata(k,i,j).im;
	     real(newm(i,j),-imag(data->get_odata(k,i+1,j))-imag(data->get_odata(k,i,j)));
		 //newm(i,j).im=data->get_odata(k,i+1,j).re+data->get_odata(k,i,j).re;
	         imag(newm(i,j),real(data->get_odata(k,i+1,j))+real(data->get_odata(k,i,j)));
		 //newm(i+1,j).re=data->get_odata(k,i+1,j).re-data->get_odata(k,i,j).re;
		 real(newm(i+1,j),real(data->get_odata(k,i+1,j))-real(data->get_odata(k,i,j)));
         //newm(i+1,j).im=data->get_odata(k,i+1,j).im-data->get_odata(k,i,j).im;
	imag(newm(i+1,j),imag(data->get_odata(k,i+1,j))-imag(data->get_odata(k,i,j)));
     }
     data->set_odata(k,newm);
}

void DirFtProc::ZeroImag(size_t k)
{
     size_t np=data->get_odata(k)->cols();
     size_t ni=data->get_odata(k)->rows();
     //store imaginary part as hypercomplex data
     if (data->global_options.useHypercomplex) {
	cmatrix hyperm(ni,np, complex(0.0,0.0));
	for (size_t j=0; j<np; j++) 
     	for (size_t i=0; i<ni; i++)
		//hyperm(i,j).re=data->get_odata(k,i,j).im;
	        real(hyperm(i,j),imag(data->get_odata(k,i,j)));
	data->set_hdata(k,hyperm);
     }

     for (size_t j=0; j<np; j++) 
     for (size_t i=0; i<ni; i++)
	data->set_odata(k,i,j,complex(real(data->get_odata(k,i,j)),0.0));
}

void IndirFtProc::apply(int k)
{
	FT_MODE mode=FT_COMPLEX;
	if (option.size()!=1)
		optionError("Wrong number of options");
	if (data->get_odata(k)->rows()<2)
		optionError("FT in indirect dimension is possible for 2D data only");
	if (option.at(0)==QString("complex"))
		mode=FT_COMPLEX;
	else if (option.at(0)==QString("STATES"))
		mode=FT_STATES;
	else if (option.at(0)==QString("TPPI"))
		mode=FT_TPPI;
	else if (option.at(0)==QString("STATES_TPPI"))
		mode=FT_STATESTPPI;
	else if (option.at(0)==QString("echo-antiecho"))
		mode=FT_ECHOANTIECHO;
	else
		optionError("Option (FT type) is unknown");
	QApplication::setOverrideCursor(Qt::WaitCursor);	
	FourierTrans(1, mode, k);
	QApplication::restoreOverrideCursor();
}

void IndirFtProc::doInteractive()
{
	QStringList items;
	        items << tr("complex")<<tr("STATES")<<tr("TPPI")<<tr("STATES_TPPI")<<tr("echo-antiecho");

        bool ok;
        QString item = QInputDialog::getItem(0, tr("Indirect Fourier Transformation"),
                                             tr("Transformation type:"), items, 0, false, &ok);
        if (ok && !item.isEmpty())
		setOptions(item);
}

void magnSpectrumProc::apply(int k)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	size_t np=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if (!ni) 
		ni=1;
	for (size_t j=0; j<ni; j++)
	for (size_t i=0; i<np; i++) {
		complex old=data->get_odata(k,j,i);
		complex magn(sqrt(real(old)*real(old)+imag(old)*imag(old)),0.0);
		data->set_odata(k,j,i,magn);
	}
	QApplication::restoreOverrideCursor();
}

void magnSpectrumProc::plotsUpdate()
{
     if (data->get_odata(data->getActiveCurve())->rows()<2)
     	plot2D->cold_restart(data);
     else
     	plot3D->cold_restart(data);
}

void addNoiseProc::apply(int k)
{
	bool ok;
	double level=getDouble(0, ok);
	if (!ok)
		return;
	size_t np=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if (!ni) 
		ni=1;
	List<double> realpart;
	for (size_t i=0; i<ni; i++)
	for (size_t j=0; j<np; j++)
		realpart.push_back(real(data->get_odata(k,i,j)));
	double maxval=max(realpart);
	double absNoise=maxval*level/100.0;
	for (size_t i=0; i<ni; i++)
	for (size_t j=0; j<np; j++) {
		complex old= data->get_odata(k,i,j);
		//old.re+=random(absNoise);
		//old.im+=random(absNoise);
		real(old,real(old)+random(absNoise));
		imag(old,imag(old)+random(absNoise));
		data->set_odata(k,i,j,old);
	}
}

void addNoiseProc::plotsUpdate()
{
     if (data->get_odata(data->getActiveCurve())->rows()<2)
     	plot2D->cold_restart(data);
     else
     	plot3D->cold_restart(data);
}

void addNoiseProc::doInteractive()
{
	bool ok;
        double level = QInputDialog::getDouble(0, tr("Add noise"),
                                         tr("Noise level"), 1.0, 0, 99999999.0, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");
	optionString=QString("%1").arg(level);
	setOptions(optionString);
}


void DirPhaseProc::apply(int k)
{
	backup.clear();
	hyperbackup.clear();
	for (size_t i=0; i<data->size(); i++) {
		backup.push_back(*data->get_odata(i));
		hyperbackup.push_back(*data->get_hdata(i));
	}
	if (option.size()!=2) 
		optionError("Phasing: Wrong number of options");
	bool ok=true;
	double rp=getDouble(0,ok);
	if (!ok) 
		return;
	rp*=M_PI/180.0;
	double lp=getDouble(1,ok);
	if (!ok) 
		return;
	lp*=M_PI/180.0;
	applyPhase(rp, lp, k);
}


void DirPhaseProc::applyPhase(double rp,double lp,size_t k) //rp, lp in radians
{
	size_t ni=data->get_odata(k)->rows();
	if (ni<1)
		ni=1;
	size_t n=data->get_odata(k)->cols();
	if (k>=data->size())
		return;
	double phase;

	if (data->hasHyperData(k)) {
	for (size_t j=0; j<ni; j++)
	for (size_t i=0; i<n; i++) {
                //phase=rp+lp*(0.5-double(i)/double(n));
                phase=rp+lp*(double(i)/double(n));
		complex oldre(real(backup(k)(j,i)), real(hyperbackup(k)(j,i)));
		complex oldim(imag(backup(k)(j,i)), imag(hyperbackup(k)(j,i)));
		data->set_odata(k,j,i, complex(real(oldre)*cos(phase)+imag(oldre)*sin(phase), -real(oldre)*sin(phase)+imag(oldre)*cos(phase)));
		data->set_hdata(k,j,i, complex(real(oldim)*cos(phase)+imag(oldim)*sin(phase), -imag(oldre)*sin(phase)+imag(oldim)*cos(phase)));
	}
//	qDebug()<<"phase has been applied";
	}
	else {
	for (size_t j=0; j<ni; j++)
	for (size_t i=0; i<n; i++) {
                //phase=rp+lp*(0.5-double(i)/double(n));
                phase=rp+lp*(double(i)/double(n));
		complex old=backup(k)(j,i);
		data->set_odata(k,j,i, complex(real(old)*cos(phase)+imag(old)*sin(phase), -real(old)*sin(phase)+imag(old)*cos(phase)));
	}
	}
	gsimFD info=*(data->get_info(k));
	info.phd0=180*rp/M_PI;
	info.phd1=180*lp/M_PI;
	data->set_info(k,info);
}

void DirPhaseProc::plotsUpdate()
{
	if (data->get_odata(data->getActiveCurve())->rows()<2)
		plot2D->Update();
	else
		plot3D->cold_restart(data);	
}

void DirPhaseProc::doInteractive()
{
	if (data->get_odata(data->getActiveCurve())->rows()>1)
		prepare2D();
	else
		doDialog();
}

void DirPhaseProc::undoPrevPhase()
{
	size_t K=data->getActiveCurve();
	QList<size_t> indexes;
	foreach (int i, data->selectedCurves)
		if ((data->get_odata(i)->rows()>1) ==(data->get_odata(K)->rows()>1))
			indexes.push_back(i);
	for (size_t i=0; i<indexes.size(); i++) {
		size_t k=indexes.at(i);	
		double rprad=(isDir())? -M_PI*data->get_info(k)->phd0/180.0 : -M_PI*data->get_info(k)->phi0/180.0;
		double lprad=(isDir())? -M_PI*data->get_info(k)->phd1/180.0 : -M_PI*data->get_info(k)->phi1/180.0;
		applyPhase(rprad,lprad,k);
	}
}

void DirPhaseProc::doDialog(bool forAll)
{
	double phc0, phc1;
	if (isDir()) {
		phc0=data->get_info(data->getActiveCurve())->phd0;
		phc1=data->get_info(data->getActiveCurve())->phd1;
	}
	else {
		phc0=data->get_info(data->getActiveCurve())->phi0;
		phc1=data->get_info(data->getActiveCurve())->phi1;
	}

	pd= new PhaseDialog(mainwindow);
	
	if (forAll)
		for (size_t i=0; i<data->size(); i++)
			if (data->get_display(i)->isVisible)
				mainwindow->filesListWidget->item(i)->setSelected(true);
	backup.clear();
	hyperbackup.clear();
	for (size_t i=0; i<data->size(); i++) {
		backup.push_back(*(data->get_odata(i)));
		hyperbackup.push_back(*(data->get_hdata(i)));
	}
	undoPrevPhase();
	backup.clear();
	hyperbackup.clear();
	for (size_t i=0; i<data->size(); i++) {//create new backup with zero phases
		backup.push_back(*(data->get_odata(i)));
		hyperbackup.push_back(*(data->get_hdata(i)));
	}

	pd->rpSlider->setValue(int(phc0*10000));
	if (phc1<-180.0) {
		while (pd->lpSlider->minimum()>phc1*10000)
			pd->lpSlider->setMinimum(pd->lpSlider->minimum()-3600000);
		pd->lpSlider->setMaximum(pd->lpSlider->minimum()+3600000);
	}
	if (phc1>180.0) {
		while (pd->lpSlider->maximum()<phc1*10000)
			pd->lpSlider->setMaximum(pd->lpSlider->maximum()+3600000);
		pd->lpSlider->setMinimum(pd->lpSlider->maximum()-3600000);
	}
	pd->lpSlider->setValue(int(phc1*10000));
	pd->lpOldValue=pd->lpSlider->value();

	connect(pd->rpSlider, SIGNAL(valueChanged(int)), this, SLOT(displayPhase()));
	connect(pd->lpSlider, SIGNAL(valueChanged(int)), this, SLOT(displayPhase()));
	connect(pd->okButton, SIGNAL(clicked()), this, SLOT(writePhase()));
	connect(pd->cancelButton, SIGNAL(clicked()), this, SLOT(cancelClicked()));

	pd->show();
	displayPhase();
}

void DirPhaseProc::prepare2D()
{
	mode2D=true;
	count=0;
	KK=data->getActiveCurve();
	switchedOff.clear();
// make all current 1D datasets invisible 
	for (size_t i=0; i<data->size(); i++) 
		if (data->get_odata(i)->rows()<2) {
			Display_ display=*(data->get_display(i));
			if (display.isVisible) {
				display.isVisible=false;
				switchedOff.push_back(i);
			}
			data->set_display(i, display);
		}
	takedialog = new ModelessDialog(plot2D);
	takedialog->label->setText("Select slice by region");
	takedialog->button1->setText("Take");
	takedialog->button2->setText("Done");
	connect (takedialog->button1, SIGNAL(clicked()), this, SLOT(moreSlicesRequested()));
	connect (takedialog->button2, SIGNAL(clicked()), this, SLOT(doneSlicesRequested()));
	takedialog->show();
        takedialog->raise();
        takedialog->activateWindow();
}

void DirPhaseProc::finish2D()
{
	mode2D=false;
	for (size_t i=0; i<data->size(); i++) 
		if (data->getParent(i).isTempDeconv) {
			data->delete_curve(i);
			i--;
		}
	for (size_t i=0; i<switchedOff.size(); i++) {
		Display_ display=*(data->get_display(switchedOff.at(i)));
		display.isVisible=true;
		data->set_display(switchedOff.at(i),display);
	}
	data->setActiveCurve(KK);
	switchedOff.clear();
	emit changeFilesList();
}


void DirPhaseProc::moreSlicesRequested()
{
	size_t k=data->getActiveCurve();
	if (!plot3D->is_leftbar) {
		QMessageBox::warning(plot3D, "Warning", "Set main marker first");
		return;
	}
	count++;
	takedialog->label->setText(QString("Select slice by markers\n%1 slice(s) added").arg(count));
	size_t xi, yi;
	plot3D->fromreal_toindex(k,plot3D->leftbarpos,xi,yi);
	if (isDir())
		data->takeRow(k,yi);
	else
		data->takeCol(k,xi);
	parent_ parent=data->getParent(data->size()-1);
	parent.isTempDeconv=true;
	data->setParent(data->size()-1,parent);
	data->setActiveCurve(k); //return 2D dataset as active
	emit changeFilesList();
}

void DirPhaseProc::doneSlicesRequested()
{
	takedialog->close();
	delete takedialog;
	if (!count) {
		QMessageBox::warning(plot2D,"Aborted", "At least one slice should be selected!");
		return;
	}
	data->setActiveCurve(data->size()-1);
	emit changeFilesList();
	plot2D->cold_restart(data);
	doDialog(true);
}

void DirPhaseProc::displayPhase()
{
	if (mode2D) //exclude 2D dataset from phasing sequence
		for (size_t i=0; i<data->size(); i++)
			if (data->get_odata(i)->rows()>1)
				mainwindow->filesListWidget->item(i)->setSelected(false);
	foreach (int k, data->selectedCurves) {
		size_t xi=0, yi=0;//indexes for main marker position
		size_t ni=data->get_odata(k)->rows();
		if (!ni) ni=1;
		size_t n=data->get_odata(k)->cols();
		(ni>1)? plot3D->fromreal_toindex(k, plot3D->leftbarpos, xi, yi) : plot2D->fromreal_toindex(k, plot2D->leftbarpos, xi);
		double xr=double(xi)/double(n);//pivot point in fraction of the spectrum length

// to the new zero-phase
                //double newzp=double(pd->rpSlider->value())-(double(pd->lpSlider->value())-double(pd->lpOldValue))*(0.5-xr);
                double newzp=double(pd->rpSlider->value())-(double(pd->lpSlider->value())-double(pd->lpOldValue))*xr;
		pd->lpOldValue=pd->lpSlider->value();
		while (newzp>=1800000.0)
			newzp-=3600000.0;
		while (newzp<-1800000.0)
			newzp+=3600000.0;
		if (int(newzp)!=pd->rpSlider->value()) {
			disconnect(pd->rpSlider, SIGNAL(valueChanged(int)), this, SLOT(displayPhase()));
			pd->rpSlider->setValue(int(newzp));
			connect(pd->rpSlider, SIGNAL(valueChanged(int)), this, SLOT(displayPhase()));
		}

		double rprad=M_PI*pd->rpSlider->value()/1800000.0;
		double lprad=M_PI*pd->lpSlider->value()/1800000.0;

		applyPhase(rprad,lprad,k);
	}
	plotsUpdate();
}

void DirPhaseProc::writePhase()
{
// index of the marker position should be taken before slices are deleted
	size_t xi=0;//indexes for main marker position
	plot2D->fromreal_toindex(data->getActiveCurve(), plot2D->leftbarpos, xi);
	if (mode2D)
		finish2D();
	QString optString;
	foreach (int k, data->selectedCurves) {
		size_t ni=data->get_odata(k)->rows();
		if (!ni) ni=1;
		double rp=pd->rpSlider->value()/10000.0;
		double lp=pd->lpSlider->value()/10000.0;
		optString=QString("%1;%2").arg(rp).arg(lp);
		applyPhase(0.0,0.0,k);//restore initial value
	}
	delete pd;
	plotsUpdate();
	setOptions(optString);
}

void DirPhaseProc::cancelClicked()
{
	if (mode2D)
		finish2D();
	delete pd;
	applyPhase(0.0,0.0,data->getActiveCurve());//restore initial value
	plotsUpdate();
}


double phaseOptimiser::operator()(const vector<double>& pars) const
{
	if (pars.size()!=2)
		qDebug()<<"Wrong param list"<<pars.size();
	List<complex> d=data->row(0);
	const size_t np=data->cols();
	qApp->processEvents();
	for (size_t i=0; i<np; i++) {
                //double phase=pars[0]+pars[1]*(0.5-double(i)/double(np));
                double phase=pars[0]+pars[1]*(double(i)/double(np));
		complex old=d(i);
		d(i)=complex(real(old)*cos(phase)+imag(old)*sin(phase), -real(old)*sin(phase)+imag(old)*cos(phase));
	}
	return fabs(min(real(d)));
}


/* Doesn't work
double phaseOptimiser::operator()(const vector<double>& pars) const
{
	//eDISPA algorithm
	List<complex> Y=data->row(0);
	const size_t np=data->cols();
//find normalisation factor
	List<double> magn(np,0.0);
	for (size_t i=0; i<np; i++)
		magn(i)=sqrt(Y(i).real()*Y(i).real()+Y(i).imag()*Y(i).imag());
	double normf=max(magn);//normalisation factor
//	qDebug()<<"norm"<<normf;
//apply phasing
	for (size_t i=0; i<np; i++) {
		double phase=pars[0]+pars[1]*(double(i)/double(np));
		complex old=(*data)(0,i);
		Y(i)=(1/normf)*complex(old.re*cos(phase)+old.im*sin(phase), -old.re*sin(phase)+old.im*cos(phase));
	}
//set parameters
	double c=0.1;
	double a=2;
	double b=1;
	double w=2;
//find functional q
	double q=0.0;
	for (size_t i=0; i<np; i++) {
		if (magn(i)/normf>=c) {//omit points below treshold
			double magnY=sqrt(Y(i).real()*Y(i).real()+Y(i).imag()*Y(i).imag());
			q+=pow(magnY,a)*pow(Y(i).real(),b)*exp(-w*(2*i-np)/np);
//			count++;
		}
	}
	qDebug()<<"q"<<q;
	return 1.0/q;
}
*/

void autoPhaseProc::apply(int k)
{
	if (data->get_odata(k)->rows() > 1) //mainly for 1D
		if (data->get_info(k)->type1!=FD_TYPE_FID) //but can accept if 2nd dimension is FID too
			optionError("For 2D case this function is working when 2nd dimension is FID only.\n");
	vector<double> pars, errs;//initial parameters and errors
	List<FunctionMinimum> minima;
	QProgressDialog progress("Searching phase correction...", "Abort ", 0, 5, plot2D);
//	progress.setWindowModality(Qt::WindowModal);

	QApplication::setOverrideCursor(Qt::WaitCursor);
	for (int i=-2; i<=2; i++) {
		phaseOptimiser phOp=phaseOptimiser(data->get_odata(k));
		progress.setValue(i+2);
		qApp->processEvents();
		if (progress.wasCanceled()){
			QApplication::restoreOverrideCursor();
			return;
		}
		pars.clear();
		errs.clear();
		pars.push_back(0.0*M_PI/180.0);
		pars.push_back(i*300.0*M_PI/180.0);//5 degree for zero and first order phase corrections
		errs.push_back(0.05*M_PI/180.0);
		errs.push_back(10*M_PI/180.0);//5 degree for zero and first order phase corrections
		VariableMetricMinimizer theMinimizer;
#ifdef USE_OLD_MINUIT
		cout<<"Starting minimization"<<endl;
		FunctionMinimum min = theMinimizer.minimize(phOp, pars, errs);
		cout<<"Finishing minimization"<<endl;
#else
		FunctionMinimum min = theMinimizer.Minimize(phOp, pars, errs);
#endif
		minima.push_back(min);
#ifdef USE_OLD_MINUIT
		cout<<min.fval()<<endl;
		cout<<"pars:"<<180*min.parameters().vec()(0U)/M_PI<<"  "<<180*min.parameters().vec()(1U)/M_PI<<endl;
#else
                cout<<min.Fval()<<endl;
		cout<<"pars:"<<180*min.Parameters().Vec()(0U)/M_PI<<"  "<<180*min.Parameters().Vec()(1U)/M_PI<<endl;
#endif
	}
	progress.setValue(5);
	int q=-1;
	double minval=3e38;
	for (size_t i=0; i<minima.size(); i++) {
#ifdef USE_OLD_MINUIT
	if (minima(i).fval()<minval) {
		minval=minima(i).fval();
		q=i;
	}
#else
	if (minima(i).Fval()<minval) {
		minval=minima(i).Fval();
		q=i;
	}
#endif
	}
	if (q<0)
		optionError("Automatic phasing failed");
	FunctionMinimum gmin=minima(q);
#ifdef USE_OLD_MINUIT
	cout<<"Chosen "<<gmin.fval()<<endl;
	MnAlgebraicVector finpars=gmin.parameters().vec();
#else
	cout<<"Chosen "<<gmin.Fval()<<endl;
//	std::cout<<"minimum: "<<min<<std::endl;
	MnAlgebraicVector finpars=gmin.Parameters().Vec();
#endif
//apply phase
	const size_t np=data->get_odata(k)->cols();
	const size_t ni=data->get_odata(k)->rows();
	for (size_t j=0; j<ni; j++) {
		for (size_t i=0; i<np; i++) {
                        //double phase=finpars(0)+finpars(1)*(0.5-double(i)/double(np));
                        double phase=finpars(0)+finpars(1)*(double(i)/double(np));
			complex old=data->get_odata(k,j,i);
			data->set_odata(k,j,i,complex(real(old)*cos(phase)+imag(old)*sin(phase), -real(old)*sin(phase)+imag(old)*cos(phase)));
		}
	}
	gsimFD info=*(data->get_info(k));
	info.phd0=180*finpars(0)/M_PI;
	info.phd1=180*finpars(1)/M_PI;
	data->set_info(k,info);
	QApplication::restoreOverrideCursor();
}


void t1DepPhaseProc::apply(int k)
{
	if (option.size()!=6)
		optionError("t1-dependant phasing: Wrong number of options");
	bool ok=true;
	double rp=getDouble(0,ok);
	if (!ok) 
		return;
	rp*=M_PI/180.0;
    //cout<<"rp "<<rp<<endl;
	
	
	double rp_inc=getDouble(1,ok);
	if (!ok) 
		return;
	rp_inc*=M_PI/180.0;
        //cout<<"rp_inc "<<rp_inc<<endl;

	double lp=getDouble(2,ok);
	if (!ok) 
		return;
	lp*=M_PI/180.0;
        //cout<<"lp "<<lp<<endl;

	double lp_inc=getDouble(3,ok);
	if (!ok) 
		return;
	lp_inc*=M_PI/180.0;
    //cout<<"lp_inc "<<lp_inc<<endl;


    int step=getInt(4,ok);
	if (!ok) 
		return;
    cout<<"step"<<step<<endl;

	double pivot=getDouble(5,ok);
	if (!ok) 
		return;

//input finished

	size_t ni=data->get_odata(k)->rows();
	if (!ni) ni=1;
	const size_t np=data->get_odata(k)->cols();
	double phase;
	size_t count=0;

	for (size_t j=0; j<ni; j++) {
	for (size_t i=0; i<np; i++) {
		count=floor(j/step);
		phase=(rp+rp_inc*count)+(lp+lp_inc*count)*(pivot-double(i)/double(np));
        if (i==0) qDebug()<<"for row "<<j<<" max phase "<<lp_inc*count*(pivot-double(i)/double(np))<<"and min phase"<<lp_inc*count*(pivot-double(np-1)/double(np));
		complex old=data->get_odata(k,j,i);
		data->set_odata(k,j,i, complex(real(old)*cos(phase)+imag(old)*sin(phase), -real(old)*sin(phase)+imag(old)*cos(phase)));
		if (data->hasHyperData(k)) {
			complex oldh=data->get_hdata(k,j,i);
			data->set_hdata(k,j,i, complex(real(oldh)*cos(phase)+imag(oldh)*sin(phase), -real(oldh)*sin(phase)+imag(oldh)*cos(phase)));
		}
	}
	//qDebug()<<count;
	}
}


void t1DepPhaseProc::doInteractive()
{
	promptDialog* dialog=new promptDialog(plot2D);
	QDoubleSpinBox* phc0=new QDoubleSpinBox(dialog);
	phc0->setValue(0.0);
	phc0->setMinimum(-360.0);
	phc0->setMaximum(360.0);
	phc0->setDecimals(1);

	QDoubleSpinBox* phc0inc=new QDoubleSpinBox(dialog);	
	phc0inc->setValue(0.0);
	phc0inc->setMinimum(-9e36);
	phc0inc->setMaximum(9e36);
	phc0inc->setDecimals(1);
	
	QDoubleSpinBox* phc1=new QDoubleSpinBox(dialog);
	phc1->setValue(0.0);
	phc1->setMinimum(-360.0);
	phc1->setMaximum(360.0);
	phc1->setDecimals(1);

	QDoubleSpinBox* phc1inc=new QDoubleSpinBox(dialog);	
	phc1inc->setValue(0.0);
	phc1inc->setMinimum(-9e36);
	phc1inc->setMaximum(9e36);
	phc1inc->setDecimals(1);

	QSpinBox* stepBox=new QSpinBox(dialog);
	stepBox->setMinimum(1);
	stepBox->setMaximum(data->get_odata(data->getActiveCurve())->rows());
	stepBox->setValue(1);

	QDoubleSpinBox* pivotBox=new QDoubleSpinBox(dialog);	
	pivotBox->setValue(0.0);
	pivotBox->setMinimum(0.0);
	pivotBox->setMaximum(1.0);
	pivotBox->setDecimals(4);

	dialog->putLayout(hBlock(dialog,"Initial PHC0 (degree)",phc0));
	dialog->putLayout(hBlock(dialog,"PHC0 increment (degree)",phc0inc));
	dialog->putLayout(hBlock(dialog,"Initial PHC1 (degree)",phc1));
	dialog->putLayout(hBlock(dialog,"PHC1 increment (degree)",phc1inc));
	dialog->putLayout(hBlock(dialog,"Increment will be applied for each N row\nN=",stepBox));
	dialog->putLayout(hBlock(dialog,"Pivot point (fraction of the data length)",pivotBox));

	int res=dialog->exec();
	if (res==QDialog::Rejected)
		throw Failed("Interupted by user");

	double rp=phc0->value();
	double rp_inc =phc0inc->value();
	double lp=phc1->value();
	double lp_inc =phc1inc->value();
	int step=stepBox->value();
	double pivot=pivotBox->value();

	delete dialog;
	optionString=QString("%1;%2;%3;%4;%5;%6").arg(rp).arg(rp_inc).arg(lp).arg(lp_inc).arg(step).arg(pivot);
	setOptions(optionString);
}

void TshearingProc::apply(int k)
{
    if (option.size()!=2)
        optionError("Shearing: Wrong number of options");
    bool ok=true;


//input daslp:
    double daslp=getDouble(0,ok);

    QString type= option.at(1);
    if (type!=QString("STATES"))
       optionError(QString("Time-domain shearing: Processing type %1 is not supported").arg(type));

    size_t ni=data->get_odata(k)->rows();
    if (!ni) ni=1;
    const size_t np=data->get_odata(k)->cols();
    double phase;

    qDebug()<<"daslp "<<daslp;

    for (size_t j=0; j<ni; j+=2) { //for the STATES
    for (size_t i=0; i<np; i++) {
        phase=daslp*((i-np/2.0)/np)*j/2/ni;
        complex oldr0=data->get_odata(k,j,i);
        complex oldr1=data->get_odata(k,j+1,i);
        double R=real(oldr0)*cos(phase)+real(oldr1)*sin(phase);
        double I=-real(oldr0)*sin(phase)+real(oldr1)*cos(phase);
        double Rh=imag(oldr0)*cos(phase)+imag(oldr1)*sin(phase);
        double Ih=-imag(oldr0)*sin(phase)+imag(oldr1)*cos(phase);
        data->set_odata(k,j,i, complex(R,Rh));
        data->set_odata(k,j+1,i, complex(I,Ih));
    }
    }
}

void TshearingProc::doInteractive()
{
    int k=data->getActiveCurve();
    promptDialog* dialog=new promptDialog(plot2D);

    QStringList exps;
    exps << "MQMAS" << "STMAS";

    QComboBox* expBox= new QComboBox(dialog);
    expBox->addItems(exps);

    QStringList spins;
    spins << "3/2" << "5/2" << "7/2" << "9/2";

    QComboBox* ivalBox= new QComboBox(dialog);
    ivalBox->addItems(spins);

    QComboBox* typeBox=new QComboBox(dialog);
    typeBox->addItem("STATES");

    QSpinBox* mvalBox=new QSpinBox(dialog);
    mvalBox->setMinimum(3);
    mvalBox->setMaximum(7);
    mvalBox->setValue(3);

    dialog->putLayout(hBlock(dialog,"Experiment",expBox));
    dialog->putLayout(hBlock(dialog,"Spin number",ivalBox));
    dialog->putLayout(hBlock(dialog,"Coherence order",mvalBox));
    dialog->putLayout(hBlock(dialog,"2D type",typeBox));

    int res=dialog->exec();
    if (res==QDialog::Rejected)
        throw Failed("Interupted by user");

    double daslp=0.0;
    QString exp=expBox->currentText();
    int ival=ivalBox->currentIndex();
    double sw_sw1=data->get_info(k)->sw/data->get_info(k)->sw1;

    if (exp==QString("MQMAS"))
        switch (mvalBox->value())
     {
            case 3://3Q
                if (ival==0) //3/2
                    daslp=-(7.0/9.0)*360.0*(sw_sw1);

                else if (ival==1) //5/2
                    daslp=(19.0/12.0)*360.0*(sw_sw1);

                else if (ival==2) //7/2
                    daslp=(101.0/45.0)*360.0*(sw_sw1);

               else if (ival==3) //9/2
                    daslp=(91/36)*360.0*(sw_sw1);
            break;
            case 5: //5Q
                if (ival==2.5)
                    daslp=-(25.0/12.0)*360.0*(sw_sw1);
              else if (ival==3.5)
                    daslp=(11.0/9.0)*360.0*(sw_sw1);
              else if (ival==4.5)
                    daslp=(95.0/36.0)*360.0*(sw_sw1);
            break;
            case 7: //7Q
                if (ival==3.5)
                    daslp=-(161.0/45.0)*360.0*(sw_sw1);
                else if (ival==4.5)
                  daslp=(7.0/18.0)*360.0*(sw_sw1);
            break;
            case 9: //9Q
                  if (ival==4.5)
                  daslp=-(31.0/6.0)*360.0*(sw_sw1);
            break;
            default: //something wrong
                  daslp=0.0;
            break;
        }
    else if (exp==QString("STMAS"))
        switch (mvalBox->value())
        {
            case 3://3Q
                if (ival==0) //3/2
                    daslp=-(8.0/9.0)*360.0*(sw_sw1);

                else if (ival==1) //5/2
                    daslp=(7.0/24.0)*360.0*(sw_sw1);

                else if (ival==2) //7/2
                    daslp=(28.0/45.0)*360.0*(sw_sw1);

                else if (ival==3) //9/2
                    daslp=(55/72)*360.0*(sw_sw1);
                break;
            case 5: //5Q
                if (ival==2.5)
                    daslp=-(11.0/6.0)*360.0*(sw_sw1);
                else if (ival==3.5)
                    daslp=-(23.0/45.0)*360.0*(sw_sw1);
                else if (ival==4.5)
                    daslp=(1.0/18.0)*360.0*(sw_sw1);
                break;
            case 7: //7Q
                if (ival==3.5)
                    daslp=-(12.0/5.0)*360.0*(sw_sw1);
                else if (ival==4.5)
                    daslp=-(9.0/18.0)*360.0*(sw_sw1);
                break;
            case 9: //9Q
                if (ival==4.5)
                    daslp=-(25.0/9.0)*360.0*(sw_sw1);
                break;
            default: //something wrong
               daslp=0.0;
               break;
        }

    daslp = QInputDialog::getDouble(plot2D, tr("Shearing factor"), tr("Computed shearing factor (can be corrected):"),daslp);

    optionString=QString("%1;%2").arg(daslp).arg(typeBox->currentText());
    delete dialog;
    setOptions(optionString);
}

void t1DepGaussLB::apply(int k)
{
    if (option.size()!=4)
		optionError("t1-dependant Gaussian line broadening: Wrong number of options");
	bool ok=true;
	double coeff=getDouble(0,ok);
	if (!ok) 
		throw Failed("Non-numerical value");

	double coeff2=getDouble(1,ok);
	if (!ok) 
		throw Failed("Non-numerical value");

	double coeff2_inc=getDouble(2,ok);
	if (!ok) 
		throw Failed("Non-numerical value");

	int step=getInt(3,ok);
	if (!ok) 
		throw Failed("Non-numerical value");

//input finished

	size_t ni=data->get_odata(k)->rows();
	if (!ni) ni=1;
	const size_t np=data->get_odata(k)->cols();
	double scaler;
	size_t count=0;
	for (size_t j=0; j<ni; j++)
	for (size_t i=0; i<np; i++) {
		count=int(j/step);
		double act_c2=coeff2+coeff2_inc*count;
		scaler=exp(((data->get_x(k,i)-act_c2)/1e6*coeff)*((data->get_x(k,i)-act_c2)/1e6*coeff)*M_PI*(-1.0));
		complex old=data->get_odata(k,j,i);
		data->set_odata(k,j,i, old*scaler);
	}
}

void t1DepGaussLB::doInteractive()
{
	bool ok;
        double lb = QInputDialog::getDouble(plot2D, tr("Enter paramter"),tr("Line broadening (Hz)"), 10.0, 0.0, 999999999999.0, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	double centre = QInputDialog::getDouble(plot2D, tr("Enter paramter"),tr("Initial shift (us)"), 0.0, 0.0, 9e36, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	double centre_inc = QInputDialog::getDouble(plot2D, tr("Enter paramter"),tr("Shift increment (us)"), 0.0, 0.0, 9e36, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
    int step=QInputDialog::getInt(plot2D, "Enter paramter","Increment will be applied for each N row\nN=", 1, 1, data->get_odata(data->getActiveCurve())->rows(), 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	optionString=QString("%1;%2;%3;%4").arg(lb).arg(centre).arg(centre_inc).arg(step);
	setOptions(optionString);
}


void IndirPhaseProc::applyPhase(double rp,double lp)
{
	applyPhase(rp, lp, data->getActiveCurve());
}

void IndirPhaseProc::applyPhase(double rp,double lp,size_t k) //rp, lp in degree
{
	if (k>=data->size())
		return;
	size_t ni=data->get_odata(k)->rows();
	if (ni<2) {
		DirPhaseProc::applyPhase(rp,lp,k);
		return;
	}

	size_t n=data->get_odata(k)->cols();
	double phase;

	if (data->hasHyperData(k)) {
	for (size_t j=0; j<n; j++)
	for (size_t i=0; i<ni; i++) {
                //phase=rp+lp*(0.5-double(i)/double(ni));
                phase=rp+lp*(double(i)/double(ni));
		complex oldre=backup(k)(i,j);
		complex oldim=hyperbackup(k)(i,j);
		data->set_odata(k,i,j, complex(real(oldre)*cos(phase)+imag(oldre)*sin(phase), -real(oldre)*sin(phase)+imag(oldre)*cos(phase)));
		data->set_hdata(k,i,j, complex(real(oldim)*cos(phase)+imag(oldim)*sin(phase), -real(oldim)*sin(phase)+imag(oldim)*cos(phase)));
	}
	}
	else {
	for (size_t j=0; j<n; j++)
	for (size_t i=0; i<ni; i++) {
        //phase=rp+lp*(0.5-double(i)/double(ni));
        phase=rp+lp*(double(i)/double(ni));
	complex old=backup(k)(i,j);
	data->set_odata(k,i,j, complex(real(old)*cos(phase)+imag(old)*sin(phase), -real(old)*sin(phase)+imag(old)*cos(phase)));
	}
	}
	gsimFD info=*(data->get_info(k));
	info.phi0=180*rp/M_PI;
	info.phi1=180*lp/M_PI;
	data->set_info(k,info);
}

void RearrangeProc::apply(int k)
{
	if (!data->size()) return;
	if (option.size()!=1) 
		optionError("Wrong number of options");
	bool ok;
	size_t np=getInt(0,ok);
	if (!ok) 
		return;
	if (np==0)
		return;

	size_t ni=data->size(k)/np;
	if ((np*ni)!=data->size(k))
		optionError("Non-integer number of points in indirect dimension is requested");

	gsimFD info=*(data->get_info(k));
	size_t oldni=data->get_odata(k)->rows();
	if (!oldni) oldni=1;
	size_t oldnp=data->get_odata(k)->cols();
	if (!ni) ni=1;
		QApplication::setOverrideCursor(Qt::WaitCursor);
		if (info.sw1==0) info.sw1=1.0; //if data is wrong
		QList<complex> stack;
		cmatrix newd(ni,np);
		for (size_t i=0; i<oldni; i++) 
		for (size_t j=0; j<oldnp; j++)
			stack.append(data->get_odata(k,i,j));
		for (size_t i=0; i<ni; i++) 
		for (size_t j=0; j<np; j++)
			newd(i,j)=stack.takeFirst();
		data->set_odata(k,newd);
		data->set_info(k,info);	
		data->updateAxisVectors();
		QApplication::restoreOverrideCursor();
}


void RearrangeProc::doInteractive()
{
	size_t k=data->getActiveCurve();
	bool ok;
	int s1=data->get_odata(k)->cols();
        int size1 = QInputDialog::getInt(0, tr("Rearrange"),
                                         tr("New size in direct dimension"), s1, 0, 99999999, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	optionString=QString("%1").arg(size1);
	setOptions(optionString);
}

void DCOffsetProc::apply(int k)
{
	if (!data->size()) return;
	size_t ni=data->get_odata(k)->rows();
	if (!ni) ni=1;
	size_t np=data->get_odata(k)->cols();
	complex mean(0.0,0.0);
	bool ok;
	double limit=getDouble(0,ok);
	if (!ok) 
		return;
	QApplication::setOverrideCursor(Qt::WaitCursor);
	size_t noisepts=size_t(limit*np/100.0);
	size_t fromindex=np-1-noisepts;
	for (size_t i=0; i<ni; i++) {
		for (size_t j=fromindex; j<np; j++) {
			mean+=data->get_odata(k,i,j);
		}
		mean/=noisepts;
		for (size_t j=0; j<np; j++) {
			data->set_odata(k,i,j,data->get_odata(k,i,j)-mean);
		}
	mean=complex(0.0,0.0);
	}
	QApplication::restoreOverrideCursor();
}

void DCOffsetProc::plotsUpdate()
{
	plot2D->Update();
	plot3D->Update();
}

void DCOffsetProc::doInteractive()
{
	bool ok;
        QString optString;
        double limit = QInputDialog::getDouble(0, tr("DC Offset"),
                                         tr("How much of the FID end to use (in %)"), 10.0, 0.0, 100.0, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	optString+=QString("%1").arg(limit);
	setOptions(optString);
}

void ShearingProc::apply(int k)
{
	if (!data->size()) return;
	size_t np=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if (ni<1) ni=1;

	if ((option.size()!=3) && (option.size()!=4))
		optionError("Wrong number of options");
	bool ok=true;
	double shift=getDouble(0,ok);
	if (shift>np)
		optionError("Option (initial shift) should be less then np");
	double inc=getDouble(1,ok);
	if ((shift+inc*(ni-1))>np)
		optionError("Shift for the last row exceed np");
	int step=getInt(2,ok);
	if (shift>np)
		optionError("Option (step) should be less then np");
	bool cut=true;
	if (option.size()==4){
		if (option.at(3)=="shift")
			cut=false;
	}
	QApplication::setOverrideCursor(Qt::WaitCursor);
	shear(k,shift,inc,step, cut);
	QApplication::restoreOverrideCursor();
}

void ShearingProc::shear(const size_t k, double shift,const double inc,const int step, bool cut=true)
{
	size_t ni=data->get_odata(k)->rows();
	size_t np=data->get_odata(k)->cols();

	for (size_t i=0; i<ni; i++) { //for each row...
	if (cut) { //cut all point left to shearing point
		for (size_t j=int(shift); j<np; j++)
			data->set_odata(k,i,j-int(shift),data->get_odata(k,i,j));
		for (size_t j=np-int(shift); j<np; j++)
			data->set_odata(k,i,j, complex(0.0,0.0));
		
	}
	else { //cyclic shift
		List<complex> temp=data->get_odata(k)->row(i);
		for (size_t j=int(shift); j<np; j++)
			data->set_odata(k,i,j-int(shift),temp(j));
		for (size_t j=np-int(shift),l=0; j<np; j++,l++)
			data->set_odata(k,i,j, temp(l));
	}
	double ratio=double(i+1)/double(step);
	if (ratio==int(ratio))
		shift+=inc;
	}
}


void ShearingProc::doInteractive()
{
	const size_t k=data->getActiveCurve();
	int np=data->get_odata(k)->cols();

	promptDialog* dialog=new promptDialog(plot2D);
	
	size1=new QDoubleSpinBox(dialog);
	size1->setValue(0.0);
	size1->setMinimum(0.0);
	size1->setMaximum(double(np));
	size1->setDecimals(1);

	size2=new QDoubleSpinBox(dialog);	
	size2->setValue(1.0);
	size2->setMinimum(0.0);
	size2->setMaximum(double(np));
	size2->setDecimals(1);
	
	step=new QSpinBox(dialog);
	step->setValue(1);
	step->setMinimum(1);
	step->setMaximum(np);

	QComboBox* cut=new QComboBox(dialog);
	QStringList its;
	its<<"cut"<<"shift";
	cut->addItems(its);

	connect(size1,SIGNAL(valueChanged(double)),this,SLOT(drawShAxis()));
	connect(size2,SIGNAL(valueChanged(double)),this,SLOT(drawShAxis()));
	connect(step,SIGNAL(valueChanged(int)),this,SLOT(drawShAxis()));

	dialog->putLayout(hBlock(dialog,"Initial shift (pts)",size1));
	dialog->putLayout(hBlock(dialog,"Shift increment (pts)",size2));
	dialog->putLayout(hBlock(dialog,"Step (1 for each row, 2 for STATES or TPPI)",step));
	dialog->putLayout(hBlock(dialog,"Method",cut));

	drawShAxis();
	int res=dialog->exec();
	if (res==QDialog::Rejected) {
		delShAxis();
		throw Failed("Interupted by user");
	}

	optionString=QString("%1;%2;%3;%4").arg(size1->value()).arg(size2->value()).arg(step->value()).arg(cut->currentText());
	delete dialog;
	delShAxis();
	setOptions(optionString);
}

void ShearingProc::delShAxis()
{
	if (hasAxis) {
		const size_t k=data->getActiveCurve();
		QList<graph_object> g=data->get_graphics(k);
		if (g.size())
			g.removeLast();
		data->set_graphics(k,g);
	}
}

void ShearingProc::drawShAxis()
{
	const size_t k=data->getActiveCurve();
	if (data->get_odata(k)->rows()<2)
		return;
	hasAxis=true;
	delShAxis();
	QList<graph_object> g=data->get_graphics(k);
	double sz1=size1->value();
	double sz2=size2->value();
	int st=int(step->value());
	int lastind=int(sz1+sz2*data->get_odata(k)->rows()/st);
	if (lastind>=data->get_odata(k)->cols()) {
		hasAxis=false; //emergency exit
		delShAxis();
		return;
	}
	graph_object line;
	line.type=GRAPHLINE;
	line.colour=Qt::gray;
	QPointF start=QPointF(data->get_x(k,int(sz1)),data->get_y(k,0));
	QPointF end=QPointF(data->get_x(k,lastind),data->get_y(k,data->get_odata(k)->rows()-1));
	line.pos=QLineF(start,end);
	g.push_back(line);
	data->set_graphics(k,g);
	plot3D->update();
}

//void ThreeQMASProc::apply()
//{
//	if (!data->size()) return;
//	size_t k=data->getActiveCurve();
//	const gsimFD* info = data->get_info(k);
//	if (option.size()!=2) {
//		optionError("Wrong number of options");
//		return;
//		}
//	QString spin=option.at(0);
//	double shift=0.0;
//	double inc=0.0;
//	int step=0;
//	if (spin==QString("3/2"))
//		inc=(7.0/9.0)*(info->sw/info->sw1);
//	else if  (spin==QString("5/2"))
//		inc=(19.0/12.0)*(info->sw/info->sw1);
//	else if  (spin==QString("7/2"))
//		inc=(606.0/270.0)*(info->sw/info->sw1);
//	else if (spin==QString("9/2"))
//		inc=(1092.0/432.0)*(info->sw/info->sw1);
//	else {
//		optionError("Wrong spin");
//		return;
//	}
//	QString method=option.at(1);
//	if ((method==QString("STATES")) || (method==QString("STATES_TPPI")))
//		step=2;
//	else if ((method==QString("complex"))||(method==QString("TPPI")))
//		step=1;
//	else {
//		optionError(QString("Method %1 is not supported").arg(method));
//		return;
//	}
//	cout<<"3Q MAS Increment: "<<inc<<" step: "<<step<<endl;
//	shear(k,shift,inc,step);
//}

//void ThreeQMASProc::doInteractive()
//{
//	bool ok;
//	QStringList spins;
//	spins<<"3/2"<<"5/2"<<"7/2"<<"9/2";
//	QStringList methods;
//	methods<<"complex"<<"STATES"<<"TPPI"<<"STATES_TPPI";
//	QString spin=QInputDialog::getItem(plot2D, tr("3Q MAS"), "Nuclear spin number:", spins, 0, false, &ok);
//	if (!ok)
//		return;
//	QString method=QInputDialog::getItem(plot2D, tr("3Q MAS"), "Indirect dimension method:", methods, 0, false, &ok);
//	optionString=spin+QString(";")+method;
//	setOptions(optionString);
//}


bool lSortRowLessThen(const lSortRow& d1, const lSortRow& d2)
{
	return d1.x<d2.x;
}

void SortRowsProc::apply(int k)
{
	if (!data->size()) 
		return;
	size_t ni= data->get_odata(k)->rows();
	if (ni<1) ni=1;
	size_t np=data->get_odata(k)->cols();
	QString selArray=option.at(0);
	int m=-1;
	QList<Array_> a=data->get_arrays(data->getActiveCurve());
	for (size_t i=0; i<a.size(); i++)
		if (a.at(i).name==selArray) m=i;
	if (m==-1)
		optionError(QString("Array %1 doesn't exist").arg(selArray));
	if (a.at(m).data.size()!=int(ni))
		throw Failed("Sort impossible: the number of rows and the length of the array are different");
	QList<lSortRow> sorting_data;
	lSortRow newentry;
	for (size_t i=0; i<ni; i++) {
		newentry.x=a.at(m).data(i); //put value from vector to newentry;
		for (size_t j=0; j<np; j++)
			newentry.row.push_back(data->get_odata(k,i,j)); //put row from odata to newentry
		sorting_data.append(newentry);
		newentry.row.clear();
	}
	qSort(sorting_data.begin(), sorting_data.end(), lSortRowLessThen);
	for (size_t i=0; i<ni; i++){ 
	for (size_t j=0; j<np; j++)
		data->set_odata(k,i,j,sorting_data.at(i).row(j));
	a[m].data[i]=sorting_data.at(i).x;
	}
	data->set_arrays(k, a);
}

void SortRowsProc::plotsUpdate()
{
	plot3D->cold_restart(data);
}

void SortRowsProc::doInteractive()
{
	if (!data->size()) throw Failed("Data is empty");;
	QStringList list;
	QString optString("");
		QList<Array_> a=data->get_arrays(data->getActiveCurve());
	if (!a.size())
		optionError(QString("No any arrows found"));
	for (size_t i=0; i<a.size(); i++)
		list<<a.at(i).name;
        bool ok;
        QString item = QInputDialog::getItem(0, tr("Sort rows"),
                                             tr("Sort rows using array:"), list, 0, false, &ok);
        if (ok && !item.isEmpty())
        	optString=item;
	setOptions(item);
}

void DirInvertProc::apply(int k)
{
	if (!data->size()) return;
	invert(*data->get_odata(k),true);
	if (data->hasHyperData(k))
		invert(*data->get_hdata(k),true);
}

void DirInvertProc::plotsUpdate()
{
	if (data->get_odata(data->getActiveCurve())->rows()<2) plot2D->cold_restart(data);
	else plot3D->cold_restart(data);
}

void IndirInvertProc::apply(int k)
{
	if (!data->size()) return;
	if (data->get_odata(k)->rows()<2) 
		optionError("Invertion in indirect dimension is possible for 2D data only");
	invert(*data->get_odata(k),false);
	if (data->hasHyperData(k))
		invert(*data->get_hdata(k),false);
}

void IndirInvertProc::plotsUpdate()
{
	plot3D->cold_restart(data);
}

void appendSpectrum::doInteractive()
{
     promptDialog *dialog = new promptDialog(plot2D);
     QTableWidget * table = new QTableWidget(data->size(),1,dialog);
     QStringList headers;
     headers<<"Dataset";
     table->setHorizontalHeaderLabels(headers);
     QCheckBox * allCheckBox= new QCheckBox("Append all",dialog);
     QLabel * label = new QLabel(tr("Select datasets to be appended"));
     dialog->putWidget(label);
     dialog->putWidget(allCheckBox);
     dialog->putWidget(table);


     dialog->setWindowTitle("Append spectra");
     connect(allCheckBox, SIGNAL(toggled(bool)), table, SLOT(setDisabled(bool)));

     for (size_t i=0; i<data->size(); i++) {
	QTableWidgetItem *newItem = new QTableWidgetItem(data->get_short_filename(i));
	newItem->setCheckState(Qt::Checked);
     	table->setItem(i, 0, newItem);
     }
     table->resizeRowsToContents();
     table->resizeColumnsToContents();
     int accepted = dialog->exec();
     if (!accepted) {
	QMessageBox::information(plot2D,"Abort","Aborted by user");
	return;
     }
     QString optlist;
     if (allCheckBox->checkState()==Qt::Checked)
	optlist="/all";
     else {
	for (size_t i=0; i<table->rowCount(); i++) {
		if (table->item(i,0)->checkState()==Qt::Checked) 
			optlist+=data->get_short_filename(i)+";";
     	}
     }	
     if (!optlist.isEmpty())
     setOptions(optlist);
     delete dialog;
}

void appendSpectrum::apply(int)
{
	size_t n=data->size();
	if (option.at(0)=="/all"){
		option.clear();
		for (size_t i=0; i<n; i++)
			option<<data->get_short_filename(i);
	}
	size_t nspectra=option.size();

	int k=-1;
	for (size_t m=0; m<nspectra; m++) {
	k=-1;
	for (size_t i=0; i<n; i++)
		if (data->get_short_filename(i)==option.at(m))
			k=i;
	if (k<0)
		optionError("Data doesn't exist");
	}

	data_entry newdata=*(data->get_entry(k));
	if (!newdata.info.sw1)
		newdata.info.sw1=10.0;
	newdata.info.is_on_disk=false;
	newdata.info.filename=QString("combination");
	data->add_entry(newdata);
	k=data->size()-1; //create a copy at the end of the data set and apply all changes on it

	for (size_t m=1; m<nspectra; m++) {
	int k1=-1;
	for (size_t i=0; i<n; i++)
		if (data->get_short_filename(i)==option.at(m))
			k1=i;
	if (k1<0)
		optionError("Data doesn't exist");

	gsimFD info = *data->get_info(k);
	gsimFD info1= *data->get_info(k1);
	const size_t np1=data->get_odata(k)->cols();
	const size_t np2=data->get_odata(k1)->cols();
	const size_t ni1=data->get_odata(k)->rows();
	const size_t ni2=data->get_odata(k1)->rows();
	if (np1!=np2)
		return;
	QApplication::setOverrideCursor(Qt::WaitCursor);
	data->resize(k,ni1+ni2,np1);
	for (size_t i=0; i<ni2; i++) 
	for (size_t j=0; j<np1; j++) 
		data->set_odata(k, ni1+i, j, data->get_odata(k1,i,j));
	QApplication::restoreOverrideCursor();
	}
}

void appendSpectrum::plotsUpdate()
{
	plot3D->cold_restart(data);
	emit changeFilesList();
}


void addSpectra::doInteractive()
{
     promptDialog *addDialog = new promptDialog(plot2D);
     QTableWidget * table = new QTableWidget(data->size(),2,addDialog);
     QStringList headers;
     headers<<"Dataset"<<"Coefficient";
     table->setHorizontalHeaderLabels(headers);
     QCheckBox * allCheckBox= new QCheckBox("Add all",addDialog);
     QLabel * label = new QLabel(tr("Select datasets to be added"));
	addDialog->putWidget(label);
	addDialog->putWidget(allCheckBox);
	addDialog->putWidget(table);

     addDialog->setWindowTitle("Add spectra");
     connect(allCheckBox, SIGNAL(toggled(bool)), table, SLOT(setDisabled(bool)));

     for (size_t i=0; i<data->size(); i++) {
	QTableWidgetItem *newItem = new QTableWidgetItem(data->get_short_filename(i));
	newItem->setCheckState(Qt::Checked);
     	table->setItem(i, 0, newItem);
	QTableWidgetItem *newItem2 = new QTableWidgetItem("1.0");
	table->setItem(i, 1, newItem2);
     }
     table->resizeRowsToContents();
     table->resizeColumnsToContents();
     int accepted = addDialog->exec();
     if (!accepted) {
	QMessageBox::information(plot2D,"Abort","Aborted by user");
	return;
     }
     QString optlist;
     if (allCheckBox->checkState()==Qt::Checked)
	optlist="/all";
     else {
	bool ok;
	for (size_t i=0; i<table->rowCount(); i++) {
		if (table->item(i,0)->checkState()==Qt::Checked) {
			double coeff=table->item(i,1)->text().toDouble(&ok);
			if (!ok) {
				QMessageBox::critical(plot2D,"Error",QString("Invalid coefficient for the dataset %1").arg(data->get_short_filename(i)));
				return;
			}
			optlist+=data->get_short_filename(i)+";"+QString("%1").arg(coeff)+";";
		}
     	}
     }	
     if (!optlist.isEmpty())
     setOptions(optlist);
     delete addDialog;

}

void addSpectra::plotsUpdate()
{
	if (data->get_odata(data->getActiveCurve())->rows()>1)
		plot3D->cold_restart(data);
	else
		plot2D->cold_restart(data);
	emit changeFilesList();
}

/*
void addSpectra::apply(int)
{
	size_t n=data->size();
	if (option.at(0)=="/all"){
		option.clear();
		for (size_t i=0; i<n; i++) {
			option<<data->get_short_filename(i);
			option<<"1.0";
		}
	}
	size_t nnew=option.size()/2; //number of spectra to be added
	int k=-1;
	bool ok;
	for (size_t i=0; i<n; i++)
		if (data->get_short_filename(i)==option.at(0))
			k=i;
	if (k<0) {
		optionError("Data doesn't exist");
		return;
		}
	double mult=getDouble(1, ok);
	if (!ok) {
		qCritical("Can't read mult");
		return;
	}
	data_entry newdata=*(data->get_entry(k));
	newdata.info.is_on_disk=false;
	newdata.info.filename=QString("sum");

	for (size_t i=0; i<newdata.spectrum.rows(); i++) 
	for (size_t j=0; j<newdata.spectrum.cols(); j++) 
		newdata.spectrum(i,j)*=mult;


	//for each new spectrum
	for (size_t m=1; m<nnew; m++) {
	int k1=-1;
	for (size_t i=0; i<n; i++)
		if (data->get_short_filename(i)==option.at(2*m))
			k1=i;
	if (k1<0) {
		optionError("Data doesn't exist");
		return;
		}
	gsimFD info = *data->get_info(k);
	gsimFD info1= *data->get_info(k1);
	const size_t np=data->get_odata(k)->cols();
	const size_t np1=data->get_odata(k1)->cols();
	const size_t ni=data->get_odata(k)->rows();
	const size_t ni1=data->get_odata(k1)->rows();
	if ((np!=np1) || (ni!=ni1)) {
		optionError(QString("dataset '%1' has different size").arg(option.at(2*m)));
		return;
	}
//actual mathematics
	double coeff=getDouble(2*m+1, ok);
	if (!ok) {
		qCritical("Coef read failed");
		return;
	}
	if ((info.ref!=info1.ref) || ((ni>1)&&(info.ref1!=info1.ref1)) && (info.type==FD_TYPE_SPE)) {
		optionError(QString("dataset %1 has different referencing from dataset %2\nThe overlapping region will be used (works for 1D datasets only)").arg(data->get_short_filename(k)).arg(data->get_short_filename(k1)));
	size_t startx, endx, startx1, endx1;
	double leftx=0.0, rightx=0.0;
	cout<<data->get_x(k,0)<<":"<<data->get_x(k1,0)<<endl;
	leftx= (data->get_x(k,0)<data->get_x(k1,0))? data->get_x(k,0) : data->get_x(k1,0);
	rightx= (data->get_x(k,np-1)>data->get_x(k1,np1-1))? data->get_x(k,np-1) : data->get_x(k1,np1-1);
	plot2D->fromreal_toindex(k,QPointF(leftx,0.0),startx);
	plot2D->fromreal_toindex(k1,QPointF(leftx,0.0),startx1);
	plot2D->fromreal_toindex(k,QPointF(rightx,0.0),endx);
	plot2D->fromreal_toindex(k1,QPointF(rightx,0.0),endx1);

	if (startx>endx) swap(startx, endx);
	if (startx1>endx1) swap(startx1, endx1);
	for (size_t i=0; i<startx; i++)
		newdata.spectrum(0,i)=complex(0.0,0.0);
	for (size_t i=endx; i<np; i++)
		newdata.spectrum(0,i)=complex(0.0,0.0);
	for (size_t i=startx, p=startx1; i<endx; i++, p++)
			newdata.spectrum(0,i)+=coeff*data->get_odata(k1,0,p);
	}
	else {
	QApplication::setOverrideCursor(Qt::WaitCursor);
	for (size_t i=0; i<ni1; i++) 
	for (size_t j=0; j<np1; j++) 
		newdata.spectrum(i,j)+=coeff*data->get_odata(k1,i,j);
	}
	QApplication::restoreOverrideCursor();
	}
	data->add_entry(newdata);
}
*/
#include "Lineshapes.h"

cmatrix addSpectra::cubic_interpolate(size_t& newnp, double oldstart, double olddx, cmatrix oldsepc, double newstart, double newdx, bool& ascending)
{
	if (!ascending) {
//		invert(oldsepc,true);
		oldstart=-oldstart;
		olddx=-olddx;
		newstart=-newstart;
		newdx=-newdx;
	}
	const size_t ni=oldsepc.rows();
	cmatrix newspec(ni,newnp);
	for (size_t i=0; i<ni; i++) {
		List<complex> oldrow=oldsepc.row(i);
		List<complex> newrow=libcmatrix::cubic_interpolate(newnp, oldstart, olddx, oldrow, newstart, newdx);
		for (size_t j=0; j<newnp; j++) {
			newspec(i,j)=newrow(j);
		}
	}
//	if (!ascending)
//		invert(newspec,true);
	return newspec;
}

void addSpectra::apply(int)
{
	size_t n=data->size();
	if (option.at(0)=="/all"){
		option.clear();
		for (size_t i=0; i<n; i++) {
			option<<data->get_short_filename(i);
			option<<"1.0";
		}
	}
	size_t nnew=option.size()/2; //number of spectra to be added
	bool ok;
	QList<int> indexes;//indexes of spectra to be added
	QList<double> mults; //multiplifiers for spectras
	for (size_t m=0; m<nnew; m++) {
		int k1=-1;
		for (size_t i=0; i<n; i++)
			if (data->get_short_filename(i)==option.at(2*m)) {
				if (k1>=0) { //check if this name were found before
					QMessageBox::warning(plot2D,"Identical names",QString("Datasets no. %1 and no %2 have identical names '%3'.\nOnly the last dataset will be used in summation").arg(k1+1).arg(i+1).arg(data->get_short_filename(i)));
				}
				k1=i;
			}
		if (k1<0)
			optionError("Data doesn't exist");
		indexes.push_back(k1);
		mults.push_back(getDouble(2*m+1, ok));
		if (!ok) {
			qCritical("Coef read failed");
			return;
		};
	} //end of finding data to be added

// check  that data is 'homogeneous'
	bool is2D=data->get_odata(indexes.at(0))->rows()>1;
	for (size_t i=1; i<indexes.size(); i++) {
		if (is2D!=(data->get_odata(indexes.at(i))->rows()>1))
			optionError(QString("Dataset %1 has incompatible dimensionality").arg(data->get_short_filename(indexes.at(i))));
		if (data->get_info(indexes.at(i))->type!=data->get_info(indexes.at(0))->type)
			optionError(QString("Dataset %1 has incompatible type in direct dimension").arg(data->get_short_filename(indexes.at(i))));
		if (is2D)
			if (data->get_info(indexes.at(i))->type1!=data->get_info(indexes.at(0))->type1)
				optionError(QString("Dataset %1 has incompatible type in indirect dimension").arg(data->get_short_filename(indexes.at(i))));
	}

//check finished
//find spectral parameters for the 'sum'
	bool ascending=(data->get_x(indexes.at(0),1)-data->get_x(indexes.at(0),0)>0);
	List<double> xstarts; //zero x on spectra
	List<double> xends; //last x on spectra
	List<double> ress; //spectral resolutions
	for (size_t i=0; i<indexes.size(); i++) {
		xstarts.push_back(data->get_x(indexes.at(i),0));
		xends.push_back(data->get_x(indexes.at(i),data->get_odata(indexes.at(i))->cols()-1));
		ress.push_back(data->get_x(indexes.at(i),1)-data->get_x(indexes.at(i),0));
	}
	double sstart=ascending? max(xstarts):min(xstarts);
	double send=ascending? min(xends):max(xends);
	double sres=ascending? min(ress):max(ress);

//TODO sometimes sum has a different number of point even if summed datasets are identical 
	size_t newnp=size_t(fabs((send-sstart)/sres)+1);

	if ((sstart<send)!=ascending)
		optionError("Some datasets have non-overlapping x-regions");
	//do the same for the indirect dimension
	List<double> ystarts; //zero x on spectra
	List<double> yends; //last x on spectra
	List<double> yress; //spectral resolutions
	bool yascending=true;
	double systart, syend, syres;
	size_t newni=1;
	if (is2D) {
		for (size_t i=0; i<indexes.size(); i++) {
			ystarts.push_back(data->get_y(indexes.at(i),0));
			yends.push_back(data->get_y(indexes.at(i),data->get_odata(indexes.at(i))->rows()-1));
			yress.push_back(data->get_y(indexes.at(i),1)-data->get_y(indexes.at(i),0));
		}
		yascending=(data->get_y(indexes.at(0),1)-data->get_y(indexes.at(0),0)>0);
		systart=yascending? max(ystarts):min(ystarts);
		syend=yascending? min(yends):max(yends);
		syres=yascending? min(yress):max(yress);
		newni=size_t(fabs((syend-systart)/syres)+1);
		if ((systart<syend)!=yascending)
			optionError("Some datasets have non-overlapping y-regions");
	}
//do cubic interpolation

	List<cmatrix> intpl(indexes.size());//will contain interpolated data
	for (size_t i=0; i<indexes.size(); i++) {
		intpl(i)=cubic_interpolate(newnp,sstart,sres,*(data->get_odata(indexes.at(i))),xstarts(i),ress(i),ascending);
		if (is2D) {
			intpl(i).transpose();
			cmatrix temp=cubic_interpolate(newni,systart,syres,intpl(i),ystarts(i),yress(i),yascending);
			temp.transpose();
			intpl(i)=temp;
		}
	}
//	qDebug()<<"Sizes"<<intpl(0).cols()<<intpl(1).cols();
//	qDebug()<<"Asked"<<newni<<"X"<<newnp;
//Do summation
	cmatrix sumsp(newni,newnp,complex(0,0));
	for (size_t i=0; i<indexes.size(); i++) {
		sumsp+=intpl(i)*mults.at(i);
	}
        //cout<<"sumsp"<<endl;
	data_entry newdata=*(data->get_entry(indexes.at(0)));
	newdata.info.is_on_disk=false;
	newdata.info.filename=QString("sum");
//calculation of sw,sw1,ref,ref1
	if (data->get_info(indexes.at(0))->type==FD_TYPE_SPE) {
		newdata.info.sw=fabs(sres)*(newnp);
		newdata.info.ref=sstart-newdata.info.sw/2.0;
		if (data->global_options.xfrequnits==UNITS_KHZ) {
			newdata.info.sw*=1e3;
			newdata.info.ref*=1e3; }
		else if (data->global_options.xfrequnits==UNITS_PPM) {
			newdata.info.sw*=newdata.info.sfrq;
			newdata.info.ref*=newdata.info.sfrq;}
	}
	else {
		newdata.info.sw=1/sres;
		if (data->global_options.xtimeunits==UNITS_MS)
			newdata.info.sw*=1e3;
		if (data->global_options.xtimeunits==UNITS_US)
			newdata.info.sw*=1e6;
	}
	if (is2D){
		if (data->get_info(indexes.at(0))->type1==FD_TYPE_SPE) {
			newdata.info.sw1=fabs(syres)*(newni);
			newdata.info.ref1=systart-newdata.info.sw1/2.0;
			if (data->global_options.yfrequnits==UNITS_KHZ) {
				newdata.info.sw1*=1e3;
				newdata.info.ref1*=1e3; }
			else if (data->global_options.yfrequnits==UNITS_PPM) {
				newdata.info.sw1*=newdata.info.sfrq1;
				newdata.info.ref1*=newdata.info.sfrq1;}
		}
		else {
			newdata.info.sw1=1/syres;
			if (data->global_options.ytimeunits==UNITS_MS)
				newdata.info.sw1*=1e3;
			if (data->global_options.ytimeunits==UNITS_US)
				newdata.info.sw1*=1e6;
		}
	}
	newdata.spectrum=sumsp;
	data->add_entry(newdata);
	data->updateAxisVectors();
}


/*void baselineFunction::set_xvector(List<double> x)
{
	xvector=x;
}

void baselineFunction::set_function(int t) //type of function
{
	type=t;
}

void baselineFunction::operator()(BaseList<double> dest, const BaseList<double>& paras) const
{
//	dest.clear();
	size_t order=paras.size();
	for (size_t i=0; i<xvector.size(); i++) {  //for each point
		double result=0.0;
		double member=0.0;
	for (int k=0; k<order; k++) {
		member=paras(k);
		switch (type) {
			case BL_POLY:
				member*=std::pow(xvector(i),k);
			break;
			case BL_SIN:
				member*=sin(xvector(i));
			break;
		}
		result+=member;
		dest(i)=result;
		}
	}
}
*/
/*
int factorial (int num)
{
 if (num<=0)
  return 1;
 if (num==1)
  return 1;
 return factorial(num-1)*num; // recursive call
}
*/

void baselineOptimiser::calculate(const vector<double>& paras, List<double>& dest) const
{
//temporary destination
	dest.clear();
	dest.create(theXvec.size(),0.0);
	size_t order=paras.size();
	for (size_t i=0; i<theXvec.size(); i++) {  //for each point
	double result=0.0;
	double member=0.0;
	for (int k=0; k<order; k++) {
		switch (type) {
			case BL_POLY:
				member=paras[k]*std::pow(double(theXvec(i)),k);//create a*x^k
			break;
			case BL_FR:
				member=(k)? paras[k]*cos(int((k+1)/2)*double(theXvec(i))*2*M_PI/double(theXvec(theXvec.size()-1))+k*M_PI/2) : paras[k];
			break;
			case BL_BERN:
				int n=order-1;
				double xv=double(theXvec(i))/double(theXvec(theXvec.size()-1));
//				member=paras[k]*(factorial(n)/(factorial(k)*factorial(n-k)))*std::pow(xv,k)*std::pow(1.0-xv,n-k); 
				member=paras[k]*std::pow(xv,k)*std::pow(1.0-xv,n-k); 
			break;
		}
		result+=member;
	}
	dest(i)=result;
	}
}

double baselineOptimiser::operator()(const vector<double>& paras) const
{
	double chi2=0.0;
	List<double> dest;
	calculate(paras, dest);
	for (size_t i=0; i<theXvec.size(); i++) { 
		chi2+=(theMeasurements(i)-dest(i))*(theMeasurements(i)-dest(i))/theError;
	}
	return chi2;
}

void print(vector<double> x)
{
	for (size_t i=0; i<x.size(); i++)
		cout<<x[i]<<":";
	cout<<endl;
}


void baselineCorrection::excludeRegion(size_t fromx, size_t tox)
{
	List<size_t> xtemp;
	List<double> ytemp;
	if (fromx>tox)
		std::swap(fromx, tox);
        //cout<<"From "<<fromx<<" to "<<tox<<endl;
		for (size_t i=0; i<xtrunc.size(); i++)
			if ((xtrunc(i)<fromx) || (xtrunc(i)>=tox)) {
				xtemp.push_back(xtrunc(i));
				ytemp.push_back(ytrunc(i));
			}
        //cout<<xtrunc.size()-xtemp.size()<<" pts excluded\n";
	xtrunc=xtemp;
	ytrunc=ytemp;
}

double baselineCorrection::calcNoise(int k, const int col)
{
	const size_t N=int(data->get_odata(k)->cols()/32); //number of points in section
	List<double> values; //noise values for different sections
	for (size_t i=0; i<32; i++) { //for each section
		List<double> points;
		for (size_t j=0; j<N; j++) {
			if (i*N+j>=data->get_odata(k)->cols())
				break;
			points.push_back(data->get_value(k,col,i*N+j));
		}
		values.push_back(max(points)-min(points));
	}
	return min(values);
}


List<int> baselineCorrection::findPeaks(int k, const int col) //TODO The function marks first 30 points as a peak area. Why?
{
//	qDebug()<<"start peak search"<<col;
	List<int> results;
	cmatrix backup=*(data->get_odata(k));
	double noise=calcNoise(k,col);
//	qDebug()<<"Noise:"<<noise;
	double n=2.0; //exceed this value
	int N=30; //+- points
	
	size_t ni=data->get_odata(k)->rows();
	if (ni<1)
		ni=1;
	size_t np=data->get_odata(k)->cols();

	cmatrix temp(ni,np,complex(0,0));
	for (size_t j=0; j<np; j++){
		List<double> rectangle;
		for (int q=j-N; q<j+N+1; q++) {
			if ( (q<0) || (q>=np))
				continue;
			rectangle.push_back(data->get_value(k,col,q));
		}
		if (rectangle.size())
		if (max(rectangle)-min(rectangle)<n*noise)
			temp(col,j)=data->get_odata(k,col,j);
	}

// finding zeros:
	int j=0;
	while (j<np) {
		int x0=0, x1=0;
		if (temp(col,j)==complex(0,0)) {
			if (j!=0)
				x0=j-1; //initial value at zero range
			x1=j;
			while ((temp(col,x1)==complex(0,0)) && (x1<np-1))
				x1++;
//			qDebug()<<"Zero Region from "<<x0<<"to"<<x1;
//			graph_object line;
//			line.type=GRAPHLINE;
//			line.pos=QLineF(QPointF(data->get_x(k,x0),0),QPointF(data->get_x(k,x1),0));
//			line.colour=Qt::red;
//			QList<graph_object> graphics=data->get_graphics(k);
//			graphics.push_back(line);
//			data->set_graphics(k,graphics);
			results.push_back(x0);
			results.push_back(x1);
			if (j<np-1)
				j=x1; //not the last point where program can stuck
			else
				j++;
		}
		else
			j++;
	}
//	qDebug()<<"endpeak search";
	return results;
}


void baselineCorrection::apply(int k)
{
	bool ok;
	xdata.clear();
	const int np=data->get_odata(k)->cols();
	if (option.size()<3)
		optionError("Baseline correction: three or more arguments should be specified");
	for (size_t i=0; i<np; i++)
		xdata.push_back(i);
	QString sbltype=option.at(0);
	int bltype;
	if (sbltype=="poly")
		bltype=BL_POLY;
	else if (sbltype=="fr" || sbltype=="fourier")
		bltype=BL_FR;
	else if (sbltype=="bern")
		bltype=BL_BERN;
	else 
		optionError(QString("Baseline correction: type %1 of function is not recognised").arg(sbltype));
	size_t ncoef=getInt(1,ok);//number of coefficients
	if (!ok) 
		optionError(QString("Baseline correction: second parameter (order) should be an integer number").arg(sbltype));
	vector<double> paras;
	size_t ni=data->get_odata(k)->rows();
	if (ni<1)
		ni=1;


QApplication::setOverrideCursor(Qt::WaitCursor);
QProgressDialog progress("Correcting baselines...", "Abort", 0, ni, plot2D);
for (size_t q=0; q<ni; q++) {
	progress.setValue(q);
	qApp->processEvents();
        if (progress.wasCanceled())
                break;
	paras.clear();
	ydata.clear();
	for (size_t i=0; i<ncoef; i++)
		paras.push_back(0.0); 
	for (size_t i=0; i<data->get_odata(k)->cols(); i++)
		ydata.push_back(real(data->get_odata(k,q,i))); //fill ydata vector

//remove peak regions
	xtrunc=xdata;
	ytrunc=ydata;
	if ((option.last()!="/auto") && (option.last()!="auto")) { //for compatibilty '/auto' is also can be accepted
		if ((option.size()-2)%2)
			optionError("Each peak region should be presented by two numbers");
		int n_regions=(option.size()-2)/2;
		for (size_t i=0; i<n_regions; i++) {
			double fromxr=getDouble(2+2*i,ok);
			if (!ok)
				optionError(QString("Start of the peak region %1 should be a number").arg(i));
			double toxr=getDouble(3+2*i,ok);
			if (!ok)
				optionError(QString("End of the peak region %1 should be a number").arg(i));
			size_t fromx, tox; //in pts
			plot2D->fromreal_toindex(k,QPointF(fromxr,0.0),fromx);
			plot2D->fromreal_toindex(k,QPointF(toxr,0.0),tox);
			excludeRegion(fromx, tox);
//			qDebug()<<"truncated"<<xtrunc.size();
	}
	}
	else {
		List<int> pts=findPeaks(k, q);
		for (size_t i=0; i<pts.size(); i+=2) {
			excludeRegion(pts(i), pts(i+1));
		}
	}

	double sum=0.0;
	for (size_t i=ytrunc.size(); i--;)
		sum+=ytrunc(i);
	paras[0]=sum/ytrunc.size();
//	cout<<"Av val:="<<paras[0]<<endl;

	double maxv=max(ytrunc);
//	cout<<"Max y:"<<maxv<<endl;


	vector<double> errors;
	switch (bltype) {
	case BL_POLY:
		for (size_t i=0; i<paras.size(); i++) {
			errors.push_back(maxv*0.01/std::pow(data->get_odata(k)->cols()-1,double(i)));
		}
		break;
	case BL_BERN:
	case BL_FR:
		for (size_t i=0; i<paras.size(); i++) {
			errors.push_back(maxv*0.01);
		}
		break;
	}

//	cout<<"Errors ";
	for (size_t i=0; i<errors.size(); i++)
		cout<<errors[i]<<" ";
	cout<<endl;

	double noise=0.01*maxv;

	List<int> xstrunc;
	List<double> ystrunc;

	size_t npoints=ncoef*10;
	if (npoints>=xtrunc.size()) {
		xstrunc=xtrunc;
		ystrunc=ytrunc;
	}
	else for (size_t i=0; i<npoints; i++) {
		xstrunc.push_back(xtrunc(i*xtrunc.size()/npoints));
		ystrunc.push_back(ytrunc(i*ytrunc.size()/npoints));
	}

/*	QList<graph_object> graphics=data->get_graphics(k);
	for (size_t i=0; i<xstrunc.size(); i++) {
		graph_object line;
		line.type=GRAPHLINE;
		line.pos=QLineF(QPointF(data->get_x(k,xstrunc(i)),plot2D->ywinmin),QPointF(data->get_x(k,xstrunc(i)),plot2D->ywinmax));
		line.colour=QColor(Qt::red);
		graphics.push_back(line);
	}
	qDebug()<<"Graphics size"<<graphics.size();
	data->set_graphics(k,graphics);
*/


	baselineOptimiser gen(ystrunc,xstrunc,noise);
	gen.type=bltype;

	VariableMetricMinimizer optimiser;

#ifdef USE_OLD_MINUIT
	FunctionMinimum min=optimiser.minimize(gen, paras, errors,0,0,0.1);
	vector<double> finpars=min.userParameters().params();
#else
	FunctionMinimum min=optimiser.Minimize(gen, paras, errors,0,0,0.1);
	vector<double> finpars=min.UserParameters().Params();
#endif
//	cout<<"After:"<<endl;
	print(finpars);

	List<double> fit;
	baselineOptimiser fingen(ydata,xdata,noise);
	fingen.type=bltype;
	fingen.calculate(finpars, fit);

// actual correction
	complex newv;
	for (size_t i=0; i<data->get_odata(k)->cols(); i++) {
		newv=data->get_odata(k,q,i)-complex(fit(i),0.0);//set corrected point
		data->set_odata(k,q,i,newv);
	}

/*	qDebug()<<"Fit"<<fit.size();
	QList<graph_object> graphics=data->get_graphics(k);
	for (size_t i=0; i<fit.size()-1; i++) {
		graph_object line;
		line.type=GRAPHLINE;
		line.pos=QLineF(QPointF(data->get_x(k,i),fit(i)),QPointF(data->get_x(k,i+1),fit(i+1)));
		line.colour=QColor(Qt::red);
		graphics.push_back(line);
	}
	qDebug()<<"Graphics size"<<graphics.size();
	data->set_graphics(k,graphics);
	plot2D->update();
*/

}//repeat for all rows
        progress.setValue(ni); 
	QApplication::restoreOverrideCursor();
}

void baselineCorrection::doInteractive()
{
//	bool ok;
	count=0;
	firstGraphicObject=data->get_graphics(data->getActiveCurve()).size();
	QStringList items;
	items<<QString("poly")<<QString("fourier")<<QString("bern");

	promptDialog dial(plot2D);
	dial.setWindowTitle(tr("Baseline correction"));
	QComboBox* typeBox=new QComboBox(&dial);
	typeBox->addItems(items);
	dial.putLayout(hBlock(&dial,"Type of correction:",typeBox));
	QSpinBox* orderBox=new QSpinBox(&dial);
	orderBox->setMinimum(1);
	orderBox->setMaximum(100);
	orderBox->setValue(8);
	dial.putLayout(hBlock(&dial,"Order:",orderBox));
	items.clear();
	items<<"Auto"<<"Manual";
	QComboBox* modeBox=new QComboBox(&dial);
	modeBox->addItems(items);
	dial.putLayout(hBlock(&dial,"Peak determination mode:",modeBox));
	int res=dial.exec();
	if (!res)
		return; //canceled dialog
	QString type=typeBox->currentText();
	optionString=type+";";
	int order=orderBox->value();
	optionString+=QString("%1;").arg(order);
	QString item = modeBox->currentText();
	if (item=="Auto") {
		optionString+=QString("auto");
		setOptions(optionString);
	}
	else {
		peakdialog = new ModelessDialog(plot2D);
		peakdialog->label->setText("Select peak regions by markers");
		peakdialog->button1->setText("Take");
		peakdialog->button2->setText("Done");
		connect (peakdialog->button1, SIGNAL(clicked()), this, SLOT(morePeaksRequested()));
		connect (peakdialog->button2, SIGNAL(clicked()), this, SLOT(donePeaksRequested()));
		peakdialog->show();
        	peakdialog->raise();
        	peakdialog->activateWindow();
	}
}

void baselineCorrection::morePeaksRequested()
{
	Plot2DWidget* plot=(data->get_odata(data->getActiveCurve())->rows()<2)? plot2D : plot3D;
	if ((!plot->is_leftbar) || (!plot->is_rightbar)) {
		QMessageBox::warning(plot2D, "Warning", "Set markers first");
		return;
	}
	count++;
	peakdialog->label->setText(QString("Select peak regions by markers\n%1 region(s) added").arg(count));
	QString region=QString("%1;%2;").arg(plot->leftbarpos.x()).arg(plot->rightbarpos.x());
	optionString+=region;

	QList<graph_object> graphics=data->get_graphics(data->getActiveCurve());
	graph_object line;
	line.colour=Qt::gray;
	line.type=GRAPHLINE;
	line.pos=QLineF(plot->leftbarpos.x(),plot->ywinmin/1.1,plot->leftbarpos.x(),plot->ywinmax/1.1);
	graphics.push_back(line);
	line.pos=QLineF(plot->rightbarpos.x(),plot->ywinmin/1.1,plot->rightbarpos.x(),plot->ywinmax/1.1);
	graphics.push_back(line);
	line.pos=QLineF(plot->leftbarpos.x(),plot->ywinmin/1.1,plot->rightbarpos.x(),plot->ywinmax/1.1);
	graphics.push_back(line);
	line.pos=QLineF(plot->rightbarpos.x(),plot->ywinmin/1.1,plot->leftbarpos.x(),plot->ywinmax/1.1);
	graphics.push_back(line);
	line.pos=QLineF(plot->rightbarpos.x(),plot->ywinmin/1.1,plot->leftbarpos.x(),plot->ywinmin/1.1);
	graphics.push_back(line);
	line.pos=QLineF(plot->rightbarpos.x(),plot->ywinmax/1.1,plot->leftbarpos.x(),plot->ywinmax/1.1);
	graphics.push_back(line);

	data->set_graphics(data->getActiveCurve(),graphics);
	plot->update();
}

void baselineCorrection::donePeaksRequested()
{
	peakdialog->close();
	delete peakdialog;
	setOptions(optionString);
	QList<graph_object> graphics=data->get_graphics(data->getActiveCurve());
	size_t n=graphics.size();
	for (int i=firstGraphicObject; i<n; i++)
		graphics.removeLast();
	data->set_graphics(data->getActiveCurve(),graphics);
	plot2D->update();
	plot3D->update();
}

void baselineCorrection::plotsUpdate()
{
	if (data->get_odata(data->getActiveCurve())->rows()>1)
		plot3D->cold_restart(data);
	else
		plot2D->cold_restart(data);
}


void smoothProc::apply(int k)
{
	bool ok;
	if (option.size()<1 || option.size()>2)
		optionError("Smoothing require 1 or 2 options");
	int dirPt=getInt(0,ok);
	if (!ok)
		optionError("first option is not specified");
	int indirPt=0;
	if (option.size()>1) {
		indirPt=getInt(1,ok);
	}
	size_t ni=data->get_odata(k)->rows();
	if (ni<1)
		ni=1;
	size_t np=data->get_odata(k)->cols();
	cmatrix temp(ni,np);
	for (size_t i=0; i<ni; i++)
	for (size_t j=0; j<np; j++) 
		temp(i,j)=data->average_pt(k,j,dirPt,i,indirPt);
	data->set_odata(k,temp);
}

void smoothProc::plotsUpdate()
{
	if (data->get_odata(data->getActiveCurve())->rows()>1)
		plot3D->cold_restart(data);
	else
		plot2D->cold_restart(data);
}

void smoothProc::doInteractive()
{
	bool ok=true;
        int dirPt = QInputDialog::getInt(0, tr("Smoothing"),
                                         tr("+/- points for direct dimension"), 1, 0, 999999, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
    int indirPt = QInputDialog::getInt(0, tr("Smoothing"),
                                         tr("+/- points for indirect dimension"), 1, 0, 99999, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	optionString=QString("%1;%2").arg(dirPt).arg(indirPt);
	setOptions(optionString);
}

void transposeProc::apply(int k)
{
	data_entry entry=*(data->get_entry(k));
	if (entry.spectrum.rows()<2)
		optionError("The active spectrum contains <2 rows: transposing impossible");

	entry.spectrum.transpose();

	std::swap(entry.display.vscale, entry.display.xscale);
	std::swap(entry.display.vshift, entry.display.xshift);
	
	entry.axis.xvector.swap(entry.axis.yvector);

	std::swap(entry.info.type, entry.info.type1);
	std::swap(entry.info.sfrq, entry.info.sfrq1);
	std::swap(entry.info.sw, entry.info.sw1);
	data->set_entry(data->getActiveCurve(),entry); 
}

void transposeProc::plotsUpdate()
{
	emit changeInfo();
	plot3D->cold_restart(data);
}

void cropProc::apply(int k) {
	bool ok, ok1;
	size_t startx, endx, starty, endy;
	gsimFD info=*(data->get_info(k));
	const size_t ni=data->get_odata(k)->rows();
	const size_t np=data->get_odata(k)->cols();
//	if ((ni && option.size()<2) || (ni>=2 && option.size()<4))
	if ((option.size()!=2) && (ni>=2 && option.size()!=4))
		optionError("Wrong number of options");
	if (ni<2) {
                plot2D->fromreal_toindex(k,QPointF(getDouble(0,ok,data->currentXUnits(k),k,true,false),0.0),startx, false);
		if (!ok)
			optionError("Option 1 is not a number");
                plot2D->fromreal_toindex(k,QPointF(getDouble(1,ok,data->currentXUnits(k),k,true,false),0.0),endx, false);
		if (!ok)
			optionError("Option 2 is not a number");
		if (startx>endx)
			std::swap(startx,endx);
		if (startx==endx)
			optionError("The first and last points are identical: wrong units?");
		size_t newnp=endx+1-startx;
		info.sw=info.sw*newnp/np;
		if (info.type==FD_TYPE_SPE) {
			double centre=fabs(data->get_x(k,startx)-data->get_x(k,endx))/2.0;
			info.ref=(data->get_x(k,startx)<data->get_x(k,endx))? data->get_x(k,startx)+centre : data->get_x(k,endx)+centre;
			if (data->global_options.xfrequnits==UNITS_PPM)
				info.ref*=info.sfrq;
			else if (data->global_options.xfrequnits==UNITS_KHZ)
				info.ref*=1000;
		}
		cmatrix temp(1,newnp);
		size_t i,p;
		for (i=startx, p=0; i<=endx; i++,p++) {
			temp(0,p)=data->get_odata(k,0,i);
		}
		data->set_odata(k,temp);
	}
	else {
		if (option.size()==4) {
                        plot3D->fromreal_toindex(k,QPointF(getDouble(0,ok,data->currentXUnits(k),k,true,false),getDouble(1,ok1,data->currentYUnits(k),k,false,false)),startx,starty, false);
			if (!ok && !ok1)
				optionError("Either option 1 or 2 is not a number");
                        double xx=getDouble(2,ok,data->currentXUnits(k),k,true,false);
                        double yy=getDouble(3,ok1,data->currentYUnits(k),k,false,false);
                        plot3D->fromreal_toindex(k,QPointF(xx,yy),endx,endy, false);
			if (!ok || !ok1)
				optionError("Either option 3 or 4 is not a number");
		}
		else { //two point specified
                        plot3D->fromreal_toindex(k,QPointF(getDouble(0,ok,data->currentXUnits(k),k,true,false),data->get_y(k,0)),startx,starty, false);
			if (!ok)
				optionError("Option 1 is not a number");
                        plot3D->fromreal_toindex(k,QPointF(getDouble(1,ok,data->currentXUnits(k),k,true,false),data->get_y(k,ni-1)),endx,endy, false);
			if (!ok)
				optionError("Option 2 is not a number");
		}
		if (startx==endx)
			optionError("The first and last points in direct dimension are identical: wrong units?");
		if (starty==endy)
			optionError("The first and last points in indirect dimension are identical: wrong units?");
		if (startx>endx)
			std::swap(startx,endx);
		if (starty>endy)
			std::swap(starty,endy);
		if (info.type==FD_TYPE_SPE) {
			double centre=fabs(data->get_x(k,startx)-data->get_x(k,endx))/2.0;
			info.ref=(data->get_x(k,startx)<data->get_x(k,endx))? data->get_x(k,startx)+centre : data->get_x(k,endx)+centre;
			if (data->global_options.xfrequnits==UNITS_PPM)
				info.ref*=info.sfrq;
			else if (data->global_options.xfrequnits==UNITS_KHZ)
				info.ref*=1000;
		}
		if (info.type1==FD_TYPE_SPE) {
			double centre=fabs(data->get_y(k,starty)-data->get_y(k,endy))/2.0;
			info.ref1=(data->get_y(k,starty)<data->get_y(k,endy))? data->get_y(k,starty)+centre : data->get_y(k,endy)+centre;
			if (data->global_options.yfrequnits==UNITS_PPM)
				info.ref1*=info.sfrq1;
			else if (data->global_options.yfrequnits==UNITS_KHZ)
				info.ref1*=1000;
		}
		size_t newnp=endx+1-startx;
		info.sw=info.sw*newnp/np;
		size_t newni=endy+1-starty;
		info.sw1=info.sw1*newni/ni;
		cmatrix temp(newni, newnp);
		size_t i,j,p,q;
		for (i=startx, p=0; i<=endx; i++,p++)
		for (j=starty, q=0; j<=endy; j++, q++)
			temp(q,p)=data->get_odata(k,j,i);
		data->set_odata(k,temp);
	}
	data->set_info(k,info);
	data->updateAxisVectors();
}

void cropProc::doInteractive()
{
	size_t k=data->getActiveCurve();
	bool is2D=data->get_odata(k)->rows()>1;
	Plot2DWidget* plot=(is2D)? plot3D : plot2D;
	if ((!plot->is_leftbar) || (!plot->is_rightbar)) {
		throw Failed("Please, set markers first");
	}
        QString xunits=plot->xaxis_label;
        QString yunits=plot->yaxis_label;
	if (!is2D)
                optionString=QString("%1%3;%2%3").arg(plot->leftbarpos.x()).arg(plot->rightbarpos.x()).arg(xunits);
	else
                optionString=QString("%1%5;%2%6;%3%5;%4%6").arg(plot->leftbarpos.x()).arg(plot->leftbarpos.y()).arg(plot->rightbarpos.x()).arg(plot->rightbarpos.y()).arg(xunits).arg(yunits);
	setOptions(optionString);
}

void cropProc::plotsUpdate()
{
	emit changeInfo();
	if (data->get_odata(data->getActiveCurve())->rows()>2)
		plot3D->cold_restart(data);
	else
		plot2D->cold_restart(data);
}

#ifdef QT_SCRIPT_LIB
scriptProc::scriptProc(DataStore* d): BaseProcessingFunc(d, tr("Custom script"), tr("<N/A>"))
{
	engine= new QScriptEngine(this);
	QScriptValue dataValue = engine->scriptValueFromQObject(data);
	engine->globalObject().setProperty("data", dataValue); 
}

scriptProc::~scriptProc()
{
	delete engine;
}

void scriptProc::apply(int)
{
}

void scriptProc::doInteractive()
{
	QString s=QInputDialog::getText(plot2D,"Script","Input command:" );
	engine->evaluate(s);
}

#endif //QT_SCRIPT_LIB
/*
double removePeaks::calcNoise(int k)
{
	const size_t N=int(data->get_odata(k)->cols()/32); //number of points in section
	List<double> values; //noise values for different sections
	for (size_t i=0; i<32; i++) { //for each section
		List<double> points;
		for (size_t j=0; j<N; j++) {
			if (i*N+j>=data->get_odata(k)->cols())
				break;
			points.push_back(data->get_value(k,0,i*N+j));
		}
		values.push_back(max(points)-min(points));
	}
	return min(values);
}

void removePeaks::apply(int k)
{
	cmatrix backup=*(data->get_odata(k));
	double noise=calcNoise(k);
	qDebug()<<"Noise:"<<noise;
	double n=1.0; //exceed this value
	int N=30; //+- points
	
	size_t ni=data->get_odata(k)->rows();
	if (ni<1)
		ni=1;
	size_t np=data->get_odata(k)->cols();

	cmatrix temp(ni,np,complex(0,0));
	for (size_t i=0; i<ni; i++) {
	for (size_t j=0; j<np; j++){
		List<double> rectangle;
		for (int q=j-N; q<j+N+1; q++) {
			if ( (q<0) || (q>=np))
				continue;
			rectangle.push_back(data->get_value(k,i,q));
		}
		if (rectangle.size())
		if (max(rectangle)-min(rectangle)<n*noise)
			temp(i,j)=data->get_odata(k,i,j);
	}
	}
//	data->set_odata(k,temp);
//smoothing
	int dirPt=20;
	int indirPt=1;
	qDebug()<<"dirPT"<<dirPt;
	cmatrix temp2(ni,np,complex(0,0));
	for (size_t i=0; i<ni; i++)
	for (size_t j=0; j<np; j++) 
		if (temp(i,j)!=complex(0,0))
			temp2(i,j)=data->average_pt(k,j,dirPt,i,indirPt);
// substituting zeros:
	for (size_t i=0; i<ni; i++){
		int j=0;
		while (j<np) {
			int x0=0, x1=0;
			if (temp2(i,j)==complex(0,0)) {
				if (j!=0)
					x0=j-1; //initial value at zero range
				x1=j;
				while ((temp2(i,x1)==complex(0,0)) && (x1<np))
					x1++;
				qDebug()<<"Zero Region from "<<x0<<"to"<<x1;
				for (size_t hh=x0+1; hh<=x1-1; hh++)
					temp2(i,hh)=((hh-x0)*(data->get_value(k,i,x1)-data->get_value(k,i,x0))/(x1-x0)+data->get_value(k,i,x0));
				j=x1;
			}
			else
				j++;
		}
	}

//	data->set_odata(k,temp2);
//	backup-=temp2;
//	data->set_odata(k,backup);
//
//get first derivatives
//	cmatrix der(ni,np, complex(0,0));
//	for (size_t i=0; i<ni; i++)
//	for (size_t j=1; j<np; j++)
//		der(i,j)=temp(i,j)-temp(i,j-1);

//finish processing
//	data->set_odata(k,der);

}
*/

void loadFileProc::apply(int k)
{
	if (option.isEmpty())
		optionError("Filename should be specified");
	const size_t Kold=data->getActiveCurve();
	data->setActiveCurve(k);
	QString filename=option.at(0);
	gsimFD infoold=*(mainwindow->data->get_info(k));
	data->mutex.unlock(); //fileOpen has its own lock
	mainwindow->fileOpen(filename,"Any known fileformats",0);
	data->mutex.lock();
	gsimFD infonew=*(mainwindow->data->get_info(k));
	infonew.filename=infoold.filename;
	infonew.file_filter=infoold.file_filter;
	data->set_info(k,infonew);
	data->setActiveCurve(Kold);
}

void loadFileProc::plotsUpdate()
{
	BaseProcessingFunc::plotsUpdate();
	emit changeFilesList();
}

void loadFileProc::doInteractive()
{
    QString all_ext;
    for (size_t i=0; i<mainwindow->fileFilterList.size(); i++) {
		if (mainwindow->fileFilterList.at(i)->canRead())
			all_ext+=mainwindow->fileFilterList.at(i)->extensions()+" ";
    }
    all_ext="Any known fileformats ("+all_ext+")";

    QString filename = QFileDialog::getOpenFileName(mainwindow, tr("Open File"), mainwindow->working_directory,all_ext);
    if (filename.isEmpty()) return;
    setOptions(filename);
}

void saveFileProc::apply(int k)
{
	if (option.isEmpty())
		optionError("At least filename should be specified");
	const size_t Kold=data->getActiveCurve();
	data->setActiveCurve(k);
	QString filename=option.at(0);
	QString format;
	if (option.size()>1)
		format=option.at(1);
	else
		format="matlab";
	if (format=="matlab")
		format="MATLAB files";
	else if (format=="spinsight")
		format="SPINSIGHT spectra or FID";
	else if (format=="simplot" || format=="simpson")
		format="SIMPSON spectra or FID";
	else
		optionError("Format unknown.\nKnown formats are: matlab, spinsight, simplot");

	gsimFD infoold=*(mainwindow->data->get_info(k));
	data->mutex.unlock(); //fileSave has its own lock
	mainwindow->fileSave(filename,format,0);
	data->mutex.lock();
	gsimFD infonew=*(mainwindow->data->get_info(k));
	infonew.file_filter=infoold.file_filter;
	data->set_info(k,infonew);
	data->setActiveCurve(Kold);
}

void saveFileProc::doInteractive()
{
    QFileDialog file_dialog(mainwindow,"Choose a file",mainwindow->working_directory);
    file_dialog.setFileMode(QFileDialog::AnyFile);
    QStringList filters;
	
	for (size_t i=0; i<mainwindow->fileFilterList.size(); i++)
		if (mainwindow->fileFilterList.at(i)->canWrite()) 
				filters.push_back(mainwindow->fileFilterList.at(i)->name()+" (*)");
    file_dialog.setNameFilters(filters);
    file_dialog.setLabelText(QFileDialog::Accept,"Save");

    QStringList s;
    if (file_dialog.exec())
    	s = file_dialog.selectedFiles();

   if (s.isEmpty()) 
	return;
   QString filter=file_dialog.selectedNameFilter();
   if (filter=="MATLAB files (*)")
	filter="matlab";
   else if (filter=="SPINSIGHT spectra or FID (*)")
	filter="spinsight";
   else if (filter=="SIMPSON spectra or FID (*)")
	filter="simplot";
   else
	optionError("Unsupported file format\nSupported formats: matlab, simplot, spinsight");
  setOptions(s.at(0)+QString(";")+filter);
}

void ExternalCommandProc::apply(int)
{
        QString command=optionString;
//        command.replace(";"," ");
        for (size_t i=0; i<option.size();i++)
                command+=option.at(i)+" ";
//	qDebug()<<"Command"<<command;
	QProcess proc(mainwindow);
	proc.execute(command);
}

void ExternalCommandProc::doInteractive()
{
	bool ok;
        QString defaultString=previousOptionString();
	QString command=QInputDialog::getText(mainwindow, tr("Enter command"),
                                          tr("Enter your command here:"), QLineEdit::Normal,
                                          defaultString, &ok);
	if (!ok)
		return;
	command.replace(QString(" "),QString(";"));
	setOptions(command);
}

void drawDiag::doInteractive()
{
	bool ok=true;
        int mq = QInputDialog::getInt(0, tr("Draw Diagonal"),
                                         tr("Order of coherence (1 for single-quantum)"), 1, 0, 999999, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	optionString=QString("%1").arg(mq);
	setOptions(optionString);
}

void drawDiag::apply(int k)
{
	if (data->get_odata(k)->rows()<2)
		optionError("This function is applicable for 2D datasets only");
	if (option.size()!=1)
		optionError("Option should contain 1 integer parameter");
	if (data->get_info(k)->type!=FD_TYPE_SPE || data->get_info(k)->type1!=FD_TYPE_SPE)
		optionError("Both dimensions should be frequency dimensions");
	bool ok;
	int mq=getInt(0,ok);
	if (!ok)
		optionError("Aborted");

	double scaler=1.0;
	if (data->global_options.yfrequnits==UNITS_PPM)
		scaler=1.0/data->get_info(k)->sfrq1;
	else if (data->global_options.yfrequnits==UNITS_KHZ)
		scaler=1.e-3;

	double pe=scaler*(-data->get_info(k)->ref1-data->get_info(k)->sw/2.0*mq);
	double ps=scaler*(-data->get_info(k)->ref1+data->get_info(k)->sw/2.0*mq);

	QPointF start(data->get_x(k,0),ps);
	QPointF end(data->get_x(k,data->get_odata(k)->cols()-1),pe);


//	qDebug()<<"Diagonal"<<start<<end;
	QList<graph_object> g=data->get_graphics(k);
	graph_object l;
	l.type=GRAPHLINE;
	l.pos=QLineF(start,end);
	g.push_back(l);
	data->set_graphics(k,g);
}

void balanceIndir::apply(int k)
{
	if (data->get_odata(k)->rows()<2)
		optionError("This function is applicable for 2D datasets only");
	if (option.size()!=1)
		optionError("Option should contain 1 integer parameter");
	if (data->get_odata(k)->rows()%2)
		optionError("Odd number of rows in the dataset");
	bool ok;
	double coeff=getDouble(0,ok);
//	if (!ok)
//		optionError("Aborted");
	for (size_t i=1; i<data->get_odata(k)->rows(); i+=2)
		for (size_t j=0; j<data->get_odata(k)->cols(); j++) {
			complex num=data->get_odata(k,i,j)*coeff;
			data->set_odata(k,i,j,complex(-imag(num),real(num)));
			}
}

void balanceIndir::doInteractive()
{
	bool ok=true;
        double c = QInputDialog::getDouble(0, tr("XY balance"),
                                         tr("coefficient"), 1.0, 0, 999999, 1, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	optionString=QString("%1").arg(c);
	setOptions(optionString);
}

void binningDir::doInteractive()
{
	bool ok=true;
    double c = QInputDialog::getDouble(0, tr("Binning/bucketing"),
                                         tr("Width of each integral region"), 0.014, 0, 999999, 3, &ok);
	if (!ok)
		throw Failed("Non-numerical value");;
	optionString=QString("%1").arg(c);
	setOptions(optionString);
}

void binningDir::apply(int k)
{
	if (option.size()!=1)
		optionError("This function require 1 option exactly");
	bool ok;
	double step=getDouble(0, ok);
	double start=0.0,end=0.0;
	bool inverted=false;
	start=data->get_x(k,0);
	end=data->get_x(k,data->get_odata(k)->cols()-1);
	if (start>end) {
		std::swap(start,end);
		inverted=true;
	}
	double range=end-start;
	int hpts=int(range/step)+1;//?
	List<complex> binned(hpts,complex(0,0));
	InterpHistogram<complex> hist(binned, range);
	cmatrix newspc(data->get_odata(k)->rows(),hpts, complex(0,0));
	for (size_t i=0; i<data->get_odata(k)->rows(); i++) {
		for (size_t j=0; j<data->get_odata(k)->cols(); j++)
			hist.add(data->get_odata(k,i,j),data->get_x(k,j)-start);
		for (size_t j=0; j<hpts; j++)
			newspc(i,j)=binned(j);
	}
	if (inverted)
		invert(newspc,true);
	gsimFD info=*(data->get_info(k));
	data->resize(k,data->get_odata(k)->rows(),hpts);
	data->set_odata(k,newspc);
	data->set_info(k,info);
	data->updateAxisVectors();
}

void redorProc::apply(int k)
{
	if (data->get_odata(k)->rows()<=1){
		optionError("This operation is applicable for arrayed data only");
	}
	if (data->get_odata(k)->rows()%2){
		optionError("For REDOR processing the even number of rows is required");
	}
	data_entry new_entry=*(data->get_entry(k));
	new_entry.info.is_on_disk=false;
	new_entry.info.filename=QString("redor");
	data->add_entry(new_entry);
	const size_t l=data->size()-1;
	data->resize(l,data->get_odata(k)->rows()/2,data->get_odata(k)->cols());
//	qDebug()<<"applying on"<<l;
//	qDebug()<<data->get_odata(l)->rows()<<data->get_odata(k)->cols();
	for (size_t i=0; i<data->get_odata(l)->rows(); i++)
	for (size_t j=0; j<data->get_odata(k)->cols(); j++) {
		double red=(real(data->get_odata(k,2*i,j))-real(data->get_odata(k,2*i+1,j)))/real(data->get_odata(k,2*i,j));
		complex point=complex(red,0.0);
		data->set_odata(l,i,j,point);
	}
	data->updateAxisVectors();
}

void redorProc::plotsUpdate()
{
	plot3D->cold_restart(data);
	emit changeFilesList();
}

void scale::doInteractive()
{
        bool ok=true;
    double c = QInputDialog::getDouble(0, tr("Spectral scaling"),
                                         tr("scaling factor"), 1.0, 0, 999999, 3, &ok);
        if (!ok)
                throw Failed("Non-numerical value");;
        optionString=QString("%1").arg(c);
        setOptions(optionString);
}

void scale::apply(int k)
{
        if (option.size()!=1)
                optionError("This function require 1 option exactly");
        bool ok;
        double scaling=getDouble(0, ok);
        for (size_t i=0; i<data->get_odata(k)->rows(); i++)
            for (size_t j=0; j<data->get_odata(k)->cols(); j++){
                complex newpt =data->get_odata(k,i,j)*scaling;
                data->set_odata(k,i,j,newpt);
                if (data->hasHyperData(k)) {
                    complex newpth=data->get_hdata(k,i,j)*scaling;
                    data->set_hdata(k,i,j,newpth);
                }
            }
}

void sum_sb::doInteractive()
{
    bool ok=true;
    double srate = QInputDialog::getDouble(0, tr("MAS rate"),
                                     tr("Input MAS or sidebands repetition rate in Hz"), 1.0, 0, 999999, 3, &ok);
    if (!ok)
            throw Failed("Non-numerical value");
    int n = QInputDialog::getInt(0, tr("SB order"),
                                     tr("Input highest sideband order"), 1.0, 0, 999999, 1, &ok);
    if (!ok)
            throw Failed("Non-numerical value");
    optionString=QString("%1;%2").arg(srate).arg(n);
    setOptions(optionString);
}

void sum_sb::apply(int k)
{
        if (option.size()!=2)
                optionError("This function requires the sideband repetition rate and the highest sideband order");
        bool ok;
        double srate = getDouble(0, ok);
        int n = getInt(1,ok);
        if ((2*n-1)*srate > data->get_info(k)->sw)
            throw Failed("Highest sideband order is beyond the sw");
//calculate number of sidebands to be added
        cmatrix sum(data->get_odata(k)->rows(),data->get_odata(k)->cols(),0.0);
        double dw = 1.0/data->get_info(k)->sw;
//do the computations
        for (int q=-n; q<=n; q++) {//for all sidebands
            double phase_correction = 2*M_PI*srate*q;
            for (size_t i=0; i<data->get_odata(k)->rows(); i++){ //for every row
                for (size_t j=0; j<data->get_odata(k)->cols(); j++){ //for every point
                    complex old = data->get_odata(k,i,j);
                    sum(i,j) += complex(real(old)*cos(phase_correction*dw*j)+imag(old)*sin(phase_correction*dw*j),
                                        -real(old)*sin(phase_correction*dw*j)+imag(old)*cos(phase_correction*dw*j));
                }
            }
        }
        data->set_odata(k,sum);
}
