//Calculation of the second moment of the line

#include <math.h>
//#include <cstdlib>
//#include "ttyio.h"

using namespace std;
using namespace libcmatrix;

void MainForm::Moments(List<double> &spec, double& norm, double &first_moment, double &second_moment, double &fourth_moment) //returns moments expressed in pts
{
	size_t N=spec.size();
	norm=0.0;
	for (size_t i=0; i<N; i++)
		norm+=spec[i];
	
	if (norm==0.0) 
		cerr<<"Integral intensity is zero!\n";

	first_moment=0;
	for (size_t i=0; i<N; i++)
	first_moment+=i*spec[i];
	first_moment/=norm;
	second_moment=0;
	fourth_moment=0;
	for (size_t i=0; i<N; i++) {
		second_moment+=(i-first_moment)*(i-first_moment)*spec[i];
		fourth_moment+=(i-first_moment)*(i-first_moment)*(i-first_moment)*(i-first_moment)*spec[i];
	}
	second_moment/=norm;
	fourth_moment/=norm;
}


double MainForm::WidthHalfHeght(List<double> &spec)
{
	size_t N=spec.size();
	double maximum=max(spec);
	double left=0, right=0, phm=0; //left and right points oon the half-height, width at half height in pts
	for(size_t i=0; i<N-1; i++) {
	if ((spec[i]<=maximum/2) && (spec[i+1]>maximum/2))
		left=i+(maximum/2-spec[i])/(spec[i+1]-spec[i]);
	}
	for (size_t i=N-1; i>0; i--) {
	if ((spec[i]<=maximum/2) && (spec[i-1]>maximum/2))
    		right=i-(maximum/2-spec[i])/(spec[i-1]-spec[i]);
	}
	phm=right-left;
	return (phm);
}


QVector<QString> MainForm::smoment(size_t starti, size_t endi, size_t k)
{
	List<double> sep_line;
	QVector<QString> message;
	for (size_t i=starti; i<endi; i++){
		sep_line.push_back(data->get_value(k,0,i));
      	}

  //Just a simple line representation
//  const int SLENGTH=64;
//  List<double> repr(SLENGTH,0.0);
//  int step=int(sep_line.length()/SLENGTH);
//  if (step>0) {
//  for (int i=0; i<sep_line.length(); i++){
//  int k=int(i/step);
//  repr(k)+=sep_line(i);
// }
//  double max_peak=max(repr);
// for (int i=0; i<SLENGTH; i++) repr(i)=int(10*repr(i)/max_peak);
//  for (int i=9;i>=0; i--) {
//  for (int j=0; j<SLENGTH; j++)
//  {
//    	(repr(j)>i)? cout<<"#" : cout<<" ";
//}
//cout<<endl;
//}
//}
  //end
	double s0,s1,s2,s4;
	const gsimFD* info=data->get_info(k);
	const size_t np=data->get_odata(k)->cols();
//	const size_t ni=data->get_odata(k)->rows();

	Moments(sep_line, s0, s1, s2, s4);
	message.push_back(QString("<b>Total intensity</b>: %1").arg(s0));
	double s1hz=s1*(info->sw/np);
	message.push_back(QString("<b>First moment:</b> %1 Hz = %2 kHz").arg(s1hz).arg(s1hz/1000));
	double s2hz=s2*(info->sw/np)*(info->sw/np);
	message.push_back(QString("<b>Second moment:</b> %1 Hz<sup>2</sup> = %2 kHz<sup>2</sup>").arg(s2hz).arg(s2hz/1e6));
	double s4hz=s4*(info->sw/np)*(info->sw/np)*(info->sw/np)*(info->sw/np);
	message.push_back(QString("<b>Fourth moment:</b> %1 Hz<sup>4</sup> = %2 kHz<sup>4</sup>").arg(s4hz).arg(s4hz/1e12));
	double kappa;
	if (s2hz>0) {
		kappa=s4hz/(s2hz*s2hz);
		message.push_back(QString("<b>kappa:</b> %1 <i>(3 for gaussian, >>1 for Lorenzian)</i>").arg(kappa));
		double theor_lw=sqrt(3.1416/2)*sqrt(s2hz/(kappa-1.87));
		message.push_back(QString("<b>FWHH</b> <i>(general lineshape theory)</i><b>:</b> %1 Hz = %2 kHz").arg(theor_lw).arg(theor_lw/1000));
	}
	else
		message.push_back("<u><i>Can't calculate FWHH because the second moment is zero</i></u>");
	double whh=WidthHalfHeght(sep_line);
	double fhhw=whh*(info->sw/np);
	message.push_back(QString("<b>FWHH</b> <i>(measuared directly)</i><b>:</b> %1 Hz = %2 kHz").arg(fhhw).arg(fhhw/1000));
	return message;
}

