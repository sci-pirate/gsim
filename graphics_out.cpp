#include "graphics_out.h"
#include <iostream>
#include <math.h>
#include <QDebug>

SVGDevice::SVGDevice(QSize s, QString dir) : QPaintDevice()
{
	xppt=5.0;
	yppt=5.0;
	size=s;
	engine = new SVGEngine(dir);
}

SVGDevice::~SVGDevice()
{
delete engine;
}

QPaintEngine * SVGDevice::paintEngine () const
{
	return engine;
}

int SVGDevice::metric ( PaintDeviceMetric metric ) const
{
switch (metric){
	case QPaintDevice::PdmWidth:
		return size.width();
	case QPaintDevice::PdmHeight:
		return size.height();
	case QPaintDevice::PdmWidthMM:
		return  int(size.width()/xppt);
	case QPaintDevice::PdmHeightMM:
		return  int(size.height()/yppt);
	case QPaintDevice::PdmNumColors:
		return 65536;//should it be millions?
	case QPaintDevice::PdmDepth:
		return 32;
	case QPaintDevice::PdmDpiX:
	case QPaintDevice::PdmPhysicalDpiX:
		return int(xppt*25.4);
	case QPaintDevice::PdmDpiY:
	case QPaintDevice::PdmPhysicalDpiY:
		return int(yppt*25.4);
	default:
		qWarning ("SVGDevice::Strange metric asked");
		return 0;
}
}

SVGEngine::SVGEngine(QString dir) : QPaintEngine(QPaintEngine::AllFeatures) 
{
	outdir=dir;
	clipApplied=false;
	matrixApplied=false;
}

SVGEngine::~SVGEngine()
{
	
}

bool SVGEngine::begin(QPaintDevice* p)
{
	setPaintDevice(p);
	output=QString("<?xml version=\"1.0\" standalone=\"no\"?>\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"  \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n<svg width=\"%1cm\" height=\"%2cm\" viewBox=\"0 0 %3 %4\"  xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">  <desc>Created by GSim </desc>").arg(paintDevice()->width()/paintDevice()->logicalDpiX()*2.54).arg(paintDevice()->height()/paintDevice()->logicalDpiX()*2.54).arg(paintDevice()->width()).arg(paintDevice()->height());
	return true;
}

bool SVGEngine::end()
{
	if (clipApplied)
		output+="\n</g>";
	output+="\n</svg>\n";
	return true;
}

void SVGEngine::drawLines ( const QLineF * lines, int lineCount )
{
QPen pen=painter()->pen();
QString colour=QString("rgb(%1,%2,%3)").arg(pen.color().red()).arg(pen.color().green()).arg(pen.color().blue());
for (size_t i=0; i<lineCount; i++) {
QString clipping=checkClipPath();
	if (painter()->hasClipping())
	if (!painter()->clipRegion().contains(QRect(lines[i].p1().toPoint(),lines[i].p2().toPoint()).normalized()))
		continue;
	output+=QString("\n<line x1=\"%1\" y1=\"%2\" x2=\"%3\" y2=\"%4\" stroke=\"%5\" stroke-width=\"1\"").arg(lines[i].x1()).arg(lines[i].y1()).arg(lines[i].x2()).arg(lines[i].y2()).arg(colour);
	if (pen.style()!=Qt::SolidLine)
		output+=" stroke-dasharray=\"3,3\" stroke-dashoffset=\"0\"";
	output+=QString("%1 />").arg(clipping);
}
}

void SVGEngine::drawPolygon ( const QPointF * points, int pointCount, PolygonDrawMode mode )
{
	QString clipping=checkClipPath();
	QPen pen=painter()->pen();
	QBrush brush=painter()->brush();
	bool first=true;
	switch (mode) {
		case QPaintEngine::PolylineMode:
		output+=QString("\n<path fill=\"none\" stroke=\"rgb(%1,%2,%3)\" stroke-width=\"1\" %4").arg(pen.color().red()).arg(pen.color().green()).arg(pen.color().blue()).arg(clipping);
		if (pen.style()!=Qt::SolidLine)
			output+=" stroke-dasharray=\"3,3\" stroke-dashoffset=\"0\"";
		output+="\nd=\"";
		for (size_t i=0; i<pointCount; i++) {
				if (first) {
					output+=QString("M ");
					first=false;
				}
				else
					output+= QString("L ");
				output+=QString("%1,%2 ").arg(points[i].x()).arg(points[i].y());
		}
		output+="\"/>";
		break;
		case QPaintEngine::OddEvenMode:
		output+=QString("\n<polygon fill=\"rgb(%1,%2,%3)\" stroke=\"none\" %4").arg(brush.color().red()).arg(brush.color().green()).arg(brush.color().blue()).arg(clipping);
		output+="\npoints=\"";
		for (size_t i=0; i<pointCount; i++) {
				output+=QString("%1,%2 ").arg(points[i].x()).arg(points[i].y());
		}
		output+="\"/>";
		break;
		default:
		std::cout<<"drawPolygon with unsupproted mode\n";
		break;
	}
}


void SVGEngine::drawTextItem ( const QPointF & p, const QTextItem & textItem ) 
{
	QRect trect=painter()->fontMetrics().boundingRect(textItem.text());
	trect.moveTo(p.toPoint());
	if ((!QRect(0,0,paintDevice()->width(),paintDevice()->height()).intersects(trect))&&(!painter()->matrixEnabled()))
		return; //do nothing if the text is outside the drawinfg area (FIXME: check valid for non-rotated text only)
	QString clipping=checkClipPath();
	QString colour=QString("\"rgb(%1,%2,%3)\" ").arg(painter()->pen().color().red()).arg(painter()->pen().color().green()).arg(painter()->pen().color().blue());
	output+=QString("\n<text x=\"%1\" y=\"%2\"\nfont-family=\"%3\" font-size=\"%4\"").arg(p.x()).arg(p.y()).arg(painter()->font().family()).arg(painter()->font().pointSize());
	if (painter()->font().italic())
		output+=QString(" font-style=\"italic\"");
	if (painter()->font().bold())
		output+=QString(" font-weight=\"bold\"");
	output+=QString(" fill=%1 %2 >%3</text>").arg(clipping).arg(colour).arg(textItem.text());
}

void SVGEngine::drawRects ( const QRectF * rects, int rectCount )
{
QString clipping=checkClipPath();
QPen pen=painter()->pen();
QString colour=QString("rgb(%1,%2,%3)").arg(pen.color().red()).arg(pen.color().green()).arg(pen.color().blue());
for (size_t i=0; i<rectCount; i++) {
	if (painter()->hasClipping())
	if (!painter()->clipRegion().contains(rects[i].toRect()))
		continue;
	double x=(rects[i].left()<rects[i].right())? rects[i].left():rects[i].right();
	double y=(rects[i].top()<rects[i].bottom())? rects[i].top():rects[i].bottom();
	output+=QString("\n<rect x=\"%1\" y=\"%2\" width=\"%3\" height=\"%4\"\nfill=\"%5\" stroke=\"%5\" stroke-width=\"1\" %6/>").arg(x).arg(y).arg(fabs(rects[i].width())).arg(fabs(rects[i].height())).arg(colour).arg(clipping);
	}
}

/*
void SVGEngine::drawPixmap ( const QRectF & r, const QPixmap & pm, const QRectF &)
{
	QString clipping=checkClipPath();
	static int file_counter=1;
	QString fname=outdir+QString("_image%1.png").arg(file_counter);
	qDebug()<<"Pixmap will be saved in "<<fname.toUtf8().data();
	file_counter++;
	pm.save(fname,"png");
	output+=QString("\n<image x=\"%1\" y=\"%2\" width=\"%3\" height=\"%4\"\nxlink:href=\"%5").arg(r.x()).arg(r.y()).arg(pm.width()).arg(pm.height()).arg(fname);
	output+=QString("%1\">\n<title>%2</title>\n</image>").arg(clipping).arg(fname);
}
*/
#include <QBuffer>

void SVGEngine::drawPixmap ( const QRectF & r, const QPixmap & pm, const QRectF &)
{
	QString clipping=checkClipPath();
	QByteArray bytes;
	QBuffer buffer(&bytes);
	buffer.open(QIODevice::WriteOnly);
	pm.save(&buffer, "PNG");
	buffer.close();
	output+=QString("\n<image x=\"%1\" y=\"%2\" width=\"%3\" height=\"%4\"\nxlink:href=\"data:image/png;base64,%5").arg(r.x()).arg(r.y()).arg(pm.width()).arg(pm.height()).arg(bytes.toBase64().data());
	output+=QString("%1\">\n</image>").arg(clipping);
}


QString SVGEngine::checkClipPath()
{
	static size_t id=1;
	static QRegion region;
	static QString oS;
	if (region!=painter()->clipRegion()){
		if (clipApplied)
			output+="\n</g>"; //eng previuos g-container 
		QRect clipRect=painter()->clipRegion().boundingRect();	
		output+=QString("\n<clipPath\nclipPathUnits=\"userSpaceOnUse\"\nid=\"clipPath%1\">\n<rect\nx=\"%2\"\ny=\"%3\"\nwidth=\"%4\"\nheight=\"%5\"\n/>\n</clipPath>").arg(id).arg(clipRect.x()).arg(clipRect.y()).arg(clipRect.width()).arg(clipRect.height());
		QString matrix;
		if (painter()->matrixEnabled()) {
			QMatrix m=painter()->matrix();
			matrix=QString("  transform=\"matrix(%1,%2,%3,%4,%5,%6)\"").arg(m.m11()).arg(m.m12()).arg(m.m21()).arg(m.m22()).arg(m.dx()).arg(m.dy());
	}

		output+=QString("\n<g clip-path = \"url(#clipPath%1)\" %2 >").arg(id).arg(matrix);
		id++;
		clipApplied=true;
		region=painter()->clipRegion();
	}
	return oS;
}

PSDevice::PSDevice(QSize s) : QPaintDevice()
{
	xppt=5.0;
	yppt=5.0;
	size=s;
	engine = new PSEngine;
}

PSDevice::~PSDevice()
{
delete engine;
}

QPaintEngine * PSDevice::paintEngine () const
{
	return engine;
}

int PSDevice::metric ( PaintDeviceMetric metric ) const
{
switch (metric){
	case QPaintDevice::PdmWidth:
		return size.width();
	case QPaintDevice::PdmHeight:
		return size.height();
	case QPaintDevice::PdmWidthMM:
		return  int(size.width()/xppt);
	case QPaintDevice::PdmHeightMM:
		return  int(size.height()/yppt);
	case QPaintDevice::PdmNumColors:
		return 65536;//should it be millions?
	case QPaintDevice::PdmDepth:
		return 32;
	case QPaintDevice::PdmDpiX:
	case QPaintDevice::PdmPhysicalDpiX:
		return int(xppt*25.4);
	case QPaintDevice::PdmDpiY:
	case QPaintDevice::PdmPhysicalDpiY:
		return int(yppt*25.4);
	default:
		qWarning ("PSDevice::Strange metric asked");
		return 0;
}
}

PSEngine::PSEngine() : QPaintEngine(QPaintEngine::AllFeatures) 
{
}

PSEngine::~PSEngine()
{
	
}

bool PSEngine::begin(QPaintDevice* p)
{
	setPaintDevice(p);

#ifdef Q_WS_X11
	output=QString("%%!PS-Adobe-3.0 EPSF-3.0\n%%BoundingBox: 0 0 %1 %2\n/S {stroke} def\n/M {moveto} def\n/L {lineto} def\n\n/dl {M L stroke} def\n/ds {/str exch def M str show} def\n/cr { /h exch def /w exch def\nnewpath\n0 0 M\nw 0 L\nw h L\n0 h L\n/fg {setrgbcolor} def\nclosepath\nclip\n%%stroke\nnewpath\n} def\n/%3 findfont %4 scalefont setfont\n1 setlinejoin\n1 setlinecap\n1 setlinewidth\n").arg(paintDevice()->width()).arg(paintDevice()->height()).arg("Helvetica").arg(painter()->font().pointSize());
#else
	output=QString("%%!PS-Adobe-3.0 EPSF-3.0\n%%BoundingBox: 0 0 %1 %2\n/S {stroke} def\n/M {moveto} def\n/L {lineto} def\n\n/dl {M L stroke} def\n/ds {/str exch def M str show} def\n/cr { /h exch def /w exch def\nnewpath\n0 0 M\nw 0 L\nw h L\n0 h L\n/fg {setrgbcolor} def\nclosepath\nclip\n%%stroke\nnewpath\n} def\n/%3 findfont %4 scalefont setfont\n1 setlinejoin\n1 setlinecap\n1 setlinewidth\n").arg(paintDevice()->width()).arg(paintDevice()->height()).arg("Arial").arg(painter()->font().pointSize());
#endif
	return true;
}

bool PSEngine::end()
{
	output+="showpage\n%%EOF\n";
	return true;
}

void PSEngine::drawLines ( const QLineF * lines, int lineCount )
{
QPen pen=painter()->pen();
if (pen.style()!=Qt::SolidLine)
	output+="[3] 0 setdash\n";
output+=QString("%1 %2 %3 setrgbcolor\n").arg(pen.color().red()).arg(pen.color().green()).arg(pen.color().blue());
for (size_t i=0; i<lineCount; i++) {
	if ((!isIn(lines[i].p1()))&&(!isIn(lines[i].p2())))
		continue;
	output+=QString("%1 %2 %3 %4 dl\n").arg(lines[i].x1()).arg(paintDevice()->height()-lines[i].y1()).arg(lines[i].x2()).arg(paintDevice()->height()-lines[i].y2());
	}
if (pen.style()!=Qt::SolidLine)
	output+="[] 0 setdash\n";
}

#define PSX paintDevice()->width()
#define PSY paintDevice()->height()
#define PSOUT(p) ((p.x() < 0 || p.x() > PSX) && (p.y() < 0 || p.y() > PSY))

void PSEngine::drawPolygon ( const QPointF * points, int pointCount, PolygonDrawMode mode )
{

	QPen pen=painter()->pen();
	QBrush brush=painter()->brush();
	if ((pen.style()!=Qt::SolidLine) && (pen.style()!=Qt::NoPen))
		output+="[3] 0 setdash\n";
	output+=QString("%1 %2 %3 setrgbcolor\n").arg(pen.color().red()).arg(pen.color().green()).arg(pen.color().blue());
	switch (mode) {
	case QPaintEngine::PolylineMode:
		output+=QString("%1 %2 M\n").arg(points[0].x()).arg(paintDevice()->height()-points[0].y());
		for (size_t i = 1; i < pointCount; i++) {
			output+=QString("%1 %2 L\n").arg(points[i].x()).arg(paintDevice()->height()-points[i].y());
		}
		output+="S\n";
	break;
	case QPaintEngine::OddEvenMode:
		output+=QString("%1 %2 M\n").arg(points[0].x()).arg(paintDevice()->height()-points[0].y());
		for (size_t i = 1; i < pointCount; i++) {
			output+=QString("%1 %2 L\n").arg(points[i].x()).arg(paintDevice()->height()-points[i].y());
		}
		output+="closepath\n";
		output+=QString("%1 %2 %3 setrgbcolor\n").arg(brush.color().red()).arg(brush.color().green()).arg(brush.color().blue());
		output+="fill\n";
	break;
	default:
		std::cout<<"drawPolygon with unsupproted mode\n";
	break;
	}
if (pen.style()!=Qt::SolidLine)
	output+="[] 0 setdash\n";
}

void PSEngine::drawTextItem ( const QPointF & p, const QTextItem & textItem ) 
{
	QRect trect=painter()->fontMetrics().boundingRect(textItem.text());
	trect.moveTo(p.toPoint());
	if ((!QRect(0,0,paintDevice()->width(),paintDevice()->height()).intersects(trect))&&(!painter()->matrixEnabled()) )
		return; //do nothing if the text is outside the drawinfg area
	QPointF pp=p;
	double angle=0.0;
	if (painter()->matrixEnabled()) {
		QMatrix m=painter()->matrix();
		QPointF op=pp;
		pp.setX(m.m11()*op.x()+m.m21()*op.y()+m.dx()); //is it just what 'map()' is doing?
		pp.setY(m.m22()*op.y()+m.m12()*op.x()+m.dy());
		angle=180*asin(m.m21())/M_PI; //rotation angle (works OK for 90 and -90, not checked for other cases)
	}
	QPen pen=painter()->pen();
	output+=QString("%1 %2 %3 setrgbcolor\n").arg(pen.color().red()).arg(pen.color().green()).arg(pen.color().blue());
	output+=QString("%1 %2 moveto\n").arg(pp.x()).arg(paintDevice()->height()-pp.y()); //move where text should be
	if (painter()->matrixEnabled())
		output+=QString("%1 rotate\n").arg(angle);//rotate if necessary
	output+=QString("(%1) show\n").arg(textItem.text());
	if (painter()->matrixEnabled())
		output+=QString("%1 rotate\n").arg(-angle);//rotate back if necesarry
}

void PSEngine::drawRects ( const QRectF * rects, int rectCount )
{
QPen pen=painter()->pen();
for (size_t i=0; i<rectCount; i++) {
	if (!isIn(rects[i].bottomLeft()))
		continue;
	output+=QString("%1 %2 %3 setrgbcolor\n").arg(pen.color().red()).arg(pen.color().green()).arg(pen.color().blue());
	double x=(rects[i].left()<rects[i].right())? rects[i].left():rects[i].right();
	double y=(rects[i].top()<rects[i].bottom())? rects[i].top():rects[i].bottom();
	output+=QString("%1 %2 %3 %4 rectfill\n").arg(x).arg(paintDevice()->height()-y).arg(fabs(rects[i].width())).arg(-fabs(rects[i].height()));
	}
}

bool PSEngine::isIn(const QPointF& point)
{
	if (!painter()->hasClipping())
		return true;
	return painter()->clipRegion().contains(point.toPoint());
}


#ifdef USE_EMF_OUTPUT
EMFDevice::EMFDevice(QSize s, QString f) : QPaintDevice()
{
	xppt=5.0;
	yppt=5.0;
	size=s;
	engine = new EMFEngine(f);
}

EMFDevice::~EMFDevice()
{
delete engine;
}

QPaintEngine * EMFDevice::paintEngine () const
{
	return engine;
}

int EMFDevice::metric ( PaintDeviceMetric metric ) const
{
switch (metric){
	case QPaintDevice::PdmWidth:
		return size.width();
	case QPaintDevice::PdmHeight:
		return size.height();
	case QPaintDevice::PdmWidthMM:
		return  int(size.width()/xppt);
	case QPaintDevice::PdmHeightMM:
		return  int(size.height()/yppt);
	case QPaintDevice::PdmNumColors:
		return 65536;//should it be millions?
	case QPaintDevice::PdmDepth:
		return 32;
	case QPaintDevice::PdmDpiX:
	case QPaintDevice::PdmPhysicalDpiX:
		return int(xppt*25.4);
	case QPaintDevice::PdmDpiY:
	case QPaintDevice::PdmPhysicalDpiY:
		return int(yppt*25.4);
	default:
		qWarning ("EMFDevice::Strange metric asked");
		return 0;
}
}

EMFEngine::EMFEngine(QString f) : QPaintEngine(QPaintEngine::AllFeatures) 
{	
	filename=f;
}

EMFEngine::~EMFEngine()
{
	
}

bool EMFEngine::begin(QPaintDevice* p)
{
	setPaintDevice(p);
	HWND desktop = GetDesktopWindow();
	HDC dc = GetDC( desktop );

	size.left=0;
	size.top=0;
//	size.right=int(paintDevice()->width()*35);
//	size.bottom=int(paintDevice()->height()*35);
	size.right=int(paintDevice()->width()/paintDevice()->logicalDpiX()*2.54*1000);
	size.bottom=int(paintDevice()->height()/paintDevice()->logicalDpiY()*2.54*1000);

	PCSTR fname = filename.toUtf8().data();
	PCSTR description = "Metafile created\0with GSim\0";

	metaDC=CreateEnhMetaFileA( dc, fname, NULL, description );

	SetWindowExtEx( metaDC, paintDevice()->width(), paintDevice()->height(), 0 );
	SetViewportExtEx( metaDC, paintDevice()->width(), paintDevice()->height(), 0 );

//	SetTextAlign(metaDC,TA_LEFT | TA_BASELINE);
	return true;
}

bool EMFEngine::end()
{
	HENHMETAFILE  metafile = CloseEnhMetaFile( metaDC );
        DeleteEnhMetaFile( metafile );
        DeleteDC( metaDC );
 	return true;
}

void EMFEngine::drawLines ( const QLineF * lines, int lineCount )
{
	setClipping();
	QPen pen=painter()->pen();
	INT style=(pen.style()==Qt::SolidLine)? PS_SOLID : PS_DOT;
	HPEN wpen = CreatePen(style,1,RGB(pen.color().red(),pen.color().green(),pen.color().blue()));
	SelectObject( metaDC, wpen );	
	for (size_t i=0; i<lineCount; i++) {
		POINT pts[2];
		pts[0].x=int(lines[i].p1().x());
		pts[0].y=int(lines[i].p1().y());
		pts[1].x=int(lines[i].p2().x());
		pts[1].y=int(lines[i].p2().y());
		Polyline(metaDC, pts, 2);
	}
	resetClipping();
}

void EMFEngine::drawPolygon ( const QPointF * points, int pointCount, PolygonDrawMode mode )
{
	setClipping();
	QPen pen=painter()->pen();
	QBrush brush=painter()->brush();
	if (mode==QPaintEngine::PolylineMode) {
		POINT pts[pointCount];
		for (size_t i=0; i<pointCount; i++) {
			pts[i].x=int(points[i].x());
			pts[i].y=int(points[i].y());
		}
		HPEN wpen = CreatePen(PS_SOLID,1,RGB(pen.color().red(),pen.color().green(),pen.color().blue()));
		SelectObject( metaDC, wpen );
		Polyline(metaDC, pts, pointCount);
	}
	else if (mode==QPaintEngine::OddEvenMode) {
		POINT pts[pointCount];
		for (size_t i=0; i<pointCount; i++) {
			pts[i].x=int(points[i].x());
			pts[i].y=int(points[i].y());
		}
		HBRUSH wbrush = CreateSolidBrush(RGB(brush.color().red(),brush.color().green(),brush.color().blue()));
		HPEN wpen = CreatePen(PS_NULL,1,RGB(pen.color().red(),pen.color().green(),pen.color().blue()));
		SelectObject( metaDC, wpen );
		SelectObject( metaDC, wbrush );
		Polygon(metaDC, pts, pointCount);
	}
	else {
		std::cout<<"EMF: drawPolygon with unsupported mode\n";
	}
	resetClipping();
}

void EMFEngine::drawTextItem ( const QPointF & p, const QTextItem & textItem ) 
{
	setClipping();
	QFontMetrics metrics=painter()->fontMetrics();
	QColor colour=painter()->pen().color();
	QPointF pp=p;
	int angle=0;
	XFORM xf;
	if (painter()->matrixEnabled()) {
		QMatrix m=painter()->matrix();
		xf.eM11=m.m11();
		xf.eM12=m.m12();
		xf.eM21=m.m21();
		xf.eM22=m.m22();
		xf.eDx=m.dx();
		xf.eDy=m.dy();
		ModifyWorldTransform(metaDC,&xf,MWT_RIGHTMULTIPLY);
	}
	SetBkMode( metaDC, TRANSPARENT );

	HFONT wfont = CreateFontA(metrics.height(),metrics.averageCharWidth(),0,angle*10,FW_NORMAL,0,0,0,DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,painter()->font().family().toUtf8().data());
	SelectObject( metaDC, wfont );
	SetTextColor( metaDC, RGB(colour.red(),colour.green(),colour.blue()) );
	pp.setY(pp.y()-metrics.height()/2);
	TextOutA(metaDC, int(pp.x()), int(pp.y()), textItem.text().toUtf8().data(), textItem.text().size());
	xf.eM11=1.0;
	xf.eM22=1.0;
	xf.eM12=0.0;
	xf.eM21=0.0;
	xf.eDx=0.0;
	xf.eDy=0.0;
	SetWorldTransform(metaDC,&xf);
	resetClipping();
}

void EMFEngine::drawRects ( const QRectF * rects, int rectCount )
{
	setClipping();
	QPen pen=painter()->pen();
	QBrush brush=painter()->brush();
	HBRUSH wbrush = CreateSolidBrush(RGB(brush.color().red(),brush.color().green(),brush.color().blue()));
	HPEN wpen = CreatePen(PS_SOLID,1,RGB(pen.color().red(),pen.color().green(),pen.color().blue()));
	SelectObject( metaDC, wpen );
	SelectObject( metaDC, wbrush );
	for (size_t i=0; i<rectCount; i++)
		Rectangle(metaDC,int(rects[i].left()),int(rects[i].top()),int(rects[i].right()),int(rects[i].bottom()));
	resetClipping();
}

void EMFEngine::drawPixmap ( const QRectF &, const QPixmap &, const QRectF &)
{
	qDebug()<<"Doesn't work";
}

void EMFEngine::setClipping()
{
#ifdef Q_WS_WIN
	if (!painter()->hasClipping()) {
		QRect rect=painter()->clipRegion().boundingRect();
		HRGN hrgn = CreateRectRgn(rect.left(),rect.top(),rect.right(),rect.bottom());
		SelectClipRgn(metaDC, hrgn);
	}
#endif
}

void EMFEngine::resetClipping()
{
#ifdef Q_WS_WIN
	if (!painter()->hasClipping())
		SelectClipRgn(metaDC, NULL);
#endif
}

#endif
