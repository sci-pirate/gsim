/****************************************************************************
** Form interface generated from reading ui file 'aboutform.ui'
**
** Created: Wed May 25 11:16:53 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
****************************************************************************/

#ifndef spectralParsDialog_H
#define spectralParsDialog_H

#include <QVariant>
#include <QDialog>
#include "ui_spectralparsdialog.h"
#include "mainform.h"
//#include <iostream.h>


class spectralParsDialog : public QDialog, public Ui::spectralParsDialog
{
    Q_OBJECT

public:
    spectralParsDialog( MainForm* parent = 0, Qt::WindowFlags fl = 0 );
    ~spectralParsDialog();

protected:
   MainForm * p;
   void setGrey(QWidget*);
   void searchParameter(QString);

public slots:
  void on_okButton_clicked();
  void on_cancelButton_clicked();
//  void on_searchButton_clicked();
//  void on_allButton_clicked();
  void resetColours();
  void on_searchLineEdit_textChanged(const QString&);
};

#endif // ABOUTFORM_H
