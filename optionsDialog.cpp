#include <QMessageBox>
#include "optionsDialog.h"

#include <QVariant>
#include <QStyleFactory>
#include <QDebug>

optionsDialog::optionsDialog(MainForm* parent, Qt::WindowFlags fl, DataStore* d)
	:QDialog(parent, fl) {
    setupUi(this);
    setWindowTitle( "Edit options" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );

    p=parent;
    data=d;
    connect(editProcButton, SIGNAL(clicked()), p, SLOT(on_editProcButton_clicked()));
    // signals and slots connections
    readSettings();
}


optionsDialog::~optionsDialog()
{
    writeSettings();
    // no need to delete child widgets, Qt does it all for us
}

void optionsDialog::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("Options");
        resize(settings.value("size", QSize(400, 400)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
	ticksSpinBox->setValue(data->global_options.ticks);
	if (data->global_options.hasXaxis)
		xaxisCheckBox->setCheckState(Qt::Checked);
	else
		xaxisCheckBox->setCheckState(Qt::Unchecked);
	if (data->global_options.hasYaxis)
		yaxisCheckBox->setCheckState(Qt::Checked);
	else
		yaxisCheckBox->setCheckState(Qt::Unchecked);
	if (data->global_options.yLeftLabels)
		yLeftLabelsBox->setCheckState(Qt::Checked);
	else
		yLeftLabelsBox->setCheckState(Qt::Unchecked);
	if (data->global_options.hasExtraTicks)
		extraTicksCheckBox->setCheckState(Qt::Checked);
	else
		extraTicksCheckBox->setCheckState(Qt::Unchecked);
	switch (data->global_options.xtimeunits){
		case UNITS_US:
			xTimeComboBox->setCurrentIndex(0);
			break;
		case UNITS_MS:
			xTimeComboBox->setCurrentIndex(1);
			break;
		case UNITS_S:
			xTimeComboBox->setCurrentIndex(2);
			break;
                case UNITS_TPTS:
                        xTimeComboBox->setCurrentIndex(3);
                        break;
	}
	switch (data->global_options.ytimeunits){
		case UNITS_US:
			yTimeComboBox->setCurrentIndex(0);
			break;
		case UNITS_MS:
			yTimeComboBox->setCurrentIndex(1);
			break;
		case UNITS_S:
			yTimeComboBox->setCurrentIndex(2);
			break;
               case UNITS_TPTS:
                        yTimeComboBox->setCurrentIndex(3);
                        break;
	}
	switch (data->global_options.xfrequnits){
		case UNITS_HZ:
			xFreqComboBox->setCurrentIndex(0);
			break;
		case UNITS_KHZ:
			xFreqComboBox->setCurrentIndex(1);
			break;
		case UNITS_PPM:
			xFreqComboBox->setCurrentIndex(2);
			break;
	}
	switch (data->global_options.yfrequnits){
		case UNITS_HZ:
			yFreqComboBox->setCurrentIndex(0);
			break;
		case UNITS_KHZ:
			yFreqComboBox->setCurrentIndex(1);
			break;
		case UNITS_PPM:
			yFreqComboBox->setCurrentIndex(2);
			break;
	}
	extraTicksSpinBox->setValue(data->global_options.extraTicks);
	if (data->global_options.axisFont.isEmpty())
		fontButton->setText(QString("%1, %2pt").arg(font().family()).arg(font().pointSize()));
	else {
		QFont font;
		font.fromString(data->global_options.axisFont);
		QString fontname=QString("%1, %2pt").arg(font.family()).arg(font.pointSize());
		fontButton->setFont(font);
		fontButton->setText(fontname);
	}
	QStringList keys=QStyleFactory::keys();
	styleComboBox->addItems(keys);
	int ind=keys.indexOf(data->global_options.style);
	if (ind>0)
		styleComboBox->setCurrentIndex(ind);
	for (size_t i=0; i<p->colourSchemeList.size(); i++)
		schemeComboBox->addItem(p->colourSchemeList.at(i)->name);
	if (data->global_options.antialiasing)
		antialiasingCheckBox->setCheckState(Qt::Checked);
	else
		antialiasingCheckBox->setCheckState(Qt::Unchecked);
	if (data->global_options.useBold1D)
		boldCheckBox->setCheckState(Qt::Checked);
	else
		boldCheckBox->setCheckState(Qt::Unchecked);
	if (data->global_options.useSkyline)
		skylineButton->setChecked(true);
	else
		skylineButton->setChecked(false);
	if (data->global_options.smoothTransformations)
		smoothCheckBox->setCheckState(Qt::Checked);
	else
		smoothCheckBox->setCheckState(Qt::Unchecked);
	if (data->global_options.animatedMarkers)
		animatedMarkersCheckBox->setCheckState(Qt::Checked);
	else
		animatedMarkersCheckBox->setCheckState(Qt::Unchecked);
	if (data->global_options.mixFidSpec)
		fidSpectrumBox->setCheckState(Qt::Checked);
	else
		fidSpectrumBox->setCheckState(Qt::Unchecked);
	customPrintLogoLineEdit->setText(data->global_options.customPrintLogo);
	if (data->global_options.useSystemPrintPars)
		useOrigParamCheckBox->setCheckState(Qt::Checked);
	else
		useOrigParamCheckBox->setCheckState(Qt::Unchecked);
	if (data->global_options.printPars)
		printParsCheckBox->setCheckState(Qt::Checked);
	else
		printParsCheckBox->setCheckState(Qt::Unchecked);
        print1DmodeComboBox->setCurrentIndex(data->global_options.print1Dmode);
        printLineWidthSpinBox->setValue(data->global_options.printLineWidth);
	if (data->global_options.useHypercomplex)
		hypercomplexCheckBox->setCheckState(Qt::Checked);
	else
		hypercomplexCheckBox->setCheckState(Qt::Unchecked);
        settings.endGroup();
}

void optionsDialog::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("Options");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
        settings.endGroup();
}

void optionsDialog::on_okButton_clicked()
{
	const size_t k=data->getActiveCurve();
	int oldx,oldy; //old default axis units
	if (data->size()) { //makes sense only if data is present
		oldx=(data->get_info(k)->type==FD_TYPE_FID)? data->global_options.xtimeunits : data->global_options.xfrequnits;
		oldy=(data->get_info(k)->type1==FD_TYPE_FID)? data->global_options.ytimeunits : data->global_options.yfrequnits;
	}
	data->global_options.ticks=ticksSpinBox->value();
	data->global_options.hasXaxis=xaxisCheckBox->checkState()==Qt::Checked;
	data->global_options.hasYaxis=yaxisCheckBox->checkState()==Qt::Checked;
	data->global_options.yLeftLabels=yLeftLabelsBox->checkState()==Qt::Checked;
	data->global_options.hasExtraTicks=extraTicksCheckBox->checkState()==Qt::Checked;
	data->global_options.extraTicks=extraTicksSpinBox->value();
	switch (xTimeComboBox->currentIndex()){
		case 0:
			data->global_options.xtimeunits=UNITS_US;
			break;
		case 1:
			data->global_options.xtimeunits=UNITS_MS;
			break;
		case 2:
			data->global_options.xtimeunits=UNITS_S;
			break;
                case 3:
                        data->global_options.xtimeunits=UNITS_TPTS;
                        break;
	}
	switch (yTimeComboBox->currentIndex()){
		case 0:
			data->global_options.ytimeunits=UNITS_US;
			break;
		case 1:
			data->global_options.ytimeunits=UNITS_MS;
			break;
		case 2:
			data->global_options.ytimeunits=UNITS_S;
			break;
                case 3:
                        data->global_options.ytimeunits=UNITS_TPTS;
                        break;
	}
	switch (xFreqComboBox->currentIndex()){
		case 0:
			data->global_options.xfrequnits=UNITS_HZ;
			break;
		case 1:
			data->global_options.xfrequnits=UNITS_KHZ;
			break;
		case 2:
			data->global_options.xfrequnits=UNITS_PPM;
			break;
	}
	switch (yFreqComboBox->currentIndex()){
		case 0:
			data->global_options.yfrequnits=UNITS_HZ;
			break;
		case 1:
			data->global_options.yfrequnits=UNITS_KHZ;
			break;
		case 2:
			data->global_options.yfrequnits=UNITS_PPM;
			break;
	}
//changing to new units (see above)
	if (data->size()){
		int newx=(data->get_info(k)->type==FD_TYPE_FID)? data->global_options.xtimeunits : data->global_options.xfrequnits;
		int newy=(data->get_info(k)->type1==FD_TYPE_FID)? data->global_options.ytimeunits : data->global_options.yfrequnits;
		p->changeScales(oldx,newx,oldy,newy);
	}

	data->global_options.antialiasing = (antialiasingCheckBox->checkState()==Qt::Checked);
	data->global_options.useBold1D = (boldCheckBox->checkState()==Qt::Checked);
	data->global_options.useSkyline = (skylineButton->isChecked());
	data->global_options.smoothTransformations=(smoothCheckBox->checkState()==Qt::Checked);
	data->global_options.animatedMarkers=(animatedMarkersCheckBox->checkState()==Qt::Checked);
	data->global_options.useHypercomplex=(hypercomplexCheckBox->checkState()==Qt::Checked);
	data->global_options.mixFidSpec=(fidSpectrumBox->checkState()==Qt::Checked);
	data->global_options.style=styleComboBox->currentText();
	data->global_options.customPrintLogo=customPrintLogoLineEdit->text();
	data->global_options.printPars=(printParsCheckBox->checkState()==Qt::Checked);
        data->global_options.print1Dmode=print1DmodeComboBox->currentIndex();
	data->global_options.printLineWidth=printLineWidthSpinBox->value();
	data->global_options.useSystemPrintPars=(useOrigParamCheckBox->checkState()==Qt::Checked);
	close();
}

void optionsDialog::on_cancelButton_clicked()
{
	close();
}

void optionsDialog::on_fontButton_clicked()
{
	bool ok;
	QFont oldf;
	oldf.fromString(data->global_options.axisFont);
	QFont font = QFontDialog::getFont(&ok, QFont(oldf), this);
	if (ok) {
		data->global_options.axisFont=font.toString();
		fontButton->setFont(font);
		fontButton->setText(QString("%1, %2pt").arg(font.family()).arg(font.pointSize()));
		p->plot2D->setFont(font);
		p->plot3D->setFont(font);
		p->activePlot()->update();
	}
}

void optionsDialog::on_styleComboBox_currentIndexChanged(const QString & style)
{
	QApplication::setStyle(style);
}

void optionsDialog::on_schemeComboBox_currentIndexChanged(const QString & schemeName)
{
	ColourScheme* scPt=NULL;
	if (!p->colourSchemeList.size())
		qDebug()<<"colourSchemeList is empty! (option dialog warning)";
	for (int i=0; i<p->colourSchemeList.size(); i++) {
		if (schemeName==p->colourSchemeList.at(i)->name)
			scPt=p->colourSchemeList.at(i);
	}
	if (!scPt)
		scPt=p->colourSchemeList.at(0); //set white scheme by default
	p->plot2D->setScheme(*scPt);
	p->plot3D->setScheme(*scPt);
}
