/****************************************************************************
** Form interface generated from reading ui file 'aboutform.ui'
**
** Created: Wed May 25 11:16:53 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
****************************************************************************/

#ifndef DECONVFORM_H
#define DECONVFORM_H

#include <QClipboard>
#include <QVariant>
#include <QDialog>
#include <QShortcut>
#include <QKeySequence>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include "ui_deconvform.h"
//#include "optim.h"
#include "mainform.h"
#include <vector>
//#include <iostream.h>

#ifdef USE_OLD_MINUIT
	#include "Minuit/FCNBase.h"
	#include "Minuit/FunctionMinimum.h"
	#include "Minuit/MnUserParameterState.h"
	//#include "Minuit/MinimumPrint.h"
	#include "Minuit/MnMigrad.h"
	#include "Minuit/MnSimplex.h"
	#include "Minuit/MnMinimize.h"
#else
	#include "Minuit2/FCNBase.h"
	#include "Minuit2/FunctionMinimum.h"
	#include "Minuit2/MnUserParameterState.h"
	//#include "Minuit/MinimumPrint.h"
	#include "Minuit2/MnMigrad.h"
	#include "Minuit2/MnSimplex.h"
	#include "Minuit2/MnMinimize.h"

	using namespace ROOT::Minuit2;

#endif


enum {LG_SUM, LG_CONVOLUTION};

class LineGenerator //: public BaseFitFunction
{
public:
	LineGenerator();
	~LineGenerator() {};
//        void operator()(BaseList<double> dest, const BaseList<double>& pars) const;
	void calculate(vector<double>&, vector<double> );
	vector<double> xvector;
	void setXvector(vector<double>& xv) {xvector=xv;};
//	int method;
//	BaseList<double> generate(BaseList<double>&);
};

class DeconvForm;

class deconvFcn : public FCNBase {
public:
	deconvFcn(const vector<double>& meas,
		  const vector<double>& xvec,
		  const double mvar) : theMeasurements(meas),
			theXvec(xvec)
			{theError=mvar;	gen = new LineGenerator(); negint=false;};
	~deconvFcn() {delete gen;};
#ifdef USE_OLD_MINUIT
	virtual double up() const {return 1.0;}
#else
	virtual double Up() const {return 1.0;}
#endif
	virtual double operator()(const vector<double>&) const;
	LineGenerator* gen;
	DeconvForm* deconvDialog;
	bool negint;
private:
	vector<double> theMeasurements;
//	std::vector<double> thePositions;
	vector<double> theXvec;
	double theError;
};

vector<double> create_lineshape (double, double, double, double, vector<double>);
vector<double> create_lineshape_sum (double, double, double, double, vector<double>);
vector<double> create_lineshape_conv (double, double, double, double, vector<double>);
vector<double> create_baseline(double, double, vector<double>); //creates y-values of baseline for given offset, slope and x-vector 
//template <class T> vector<T> fromList_toVector(List<T>);

class DeconvForm : public QDialog, public Ui::DeconvForm
{
    Q_OBJECT

public:
    DeconvForm( MainForm* parent = 0, Qt::WindowFlags fl = Qt::Window );
    ~DeconvForm();

//    QMenuBar *menubar;
//    QToolBar *toolBar;
//    QMenu *fileMenu;
//    QMenu *editMenu;
//    QAction* fileExitAction;

protected:
   MainForm * p; //pointer to parent window
   DataStore * data; //pointer to datastore
//   LineGenerator * model; //model functional object
   void readSettings();
   void writeSettings();

   void wheelEvent ( QWheelEvent * );

    QShortcut * editCopyShortcut;
    QShortcut * editPasteShortcut;


   size_t xstart; //x position (in real coordnates) of the staring point in fitted data
   size_t xend; //x position (in real coordnates) of the last point in fitted data
   void read_parameters(QStringList&, vector<double>&, vector<size_t>&); //read paremeters from interactive table 

   void create_parameters(QStringList&, MnUserParameters&, const vector<double>);

   vector<double> get_xvector(size_t); //return vectors of x-coordinates
   vector<double> get_yvector(const size_t, const size_t); //return experimental y-data
   void delete_temp_lines(); //deletes visual line, created interactivly
   void accumResults(size_t, size_t, QList<data_entry>&, vector<double>, vector<double>, double, QStringList);
   void showResults(size_t, QList<data_entry>&);
   void rewrite_linestable(vector<double> );
   vector<double> convert_pars(vector<double>);
   List<double> fromvector_tolist(vector<double>);
   int searchHalfWay(int , int ); ///Search halfwidth
   void closeEvent(QCloseEvent *);

public slots:
  void on_addButton_clicked();
  void on_delButton_clicked();
  void on_closeButton_clicked();
  void on_startButton_clicked();
  void update_temp_lines(const vector<double>* defpar=NULL); //redraw lines changed interctively
  void editCopy();
  void editPaste();
  void changeMixing(int);
  void guessRightbarPos();
};

#endif // DECONVFORM_H
