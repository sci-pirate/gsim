#ifndef PLOTAREA_H
#define PLOTAREA_H

#include "base.h"
#include "ui_plotarea.h"


class PlotArea : public QWidget, public Ui::mdiWindowWidget
{
    Q_OBJECT

public:
    PlotArea( QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~PlotArea();
    DataStore* data;
};

#endif // ABOUTFORM_H
