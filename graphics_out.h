#ifndef GRAPHICSOUT_H_
#define GRAPHICSOUT_H_

#include <QPaintDevice>
#include <QPaintEngine>
#include <QApplication>

#ifdef USE_EMF_OUTPUT

	#ifdef Q_WS_X11
		#include <libEMF/emf.h>
	#endif

	#ifdef Q_WS_MAC
		#include <libEMF/emf.h>
	#endif

	#ifdef Q_WS_WIN
		#include <windows.h>
	#endif

#undef min
#undef max
#endif

class SVGEngine : public QPaintEngine
{
public:
	SVGEngine(QString dir="");
	~SVGEngine();
	virtual bool begin(QPaintDevice*);
	virtual bool end();
	virtual void updateState( const QPaintEngineState & ) {};
	virtual void drawImage(const QRectF& r, const QImage& im, const QRectF& sr) {drawPixmap(r,QPixmap::fromImage(im),sr);};
	virtual QPaintEngine::Type type() const {return QPaintEngine::User;};
	virtual void drawLines ( const QLineF * , int );
	virtual void drawPolygon ( const QPointF * , int , PolygonDrawMode );
//	virtual void drawPath( const QPainterPath &);
	virtual void drawTextItem ( const QPointF & , const QTextItem & );
	virtual void drawRects ( const QRectF * , int );
	virtual void drawPixmap ( const QRectF &, const QPixmap &, const QRectF &);
	virtual QString checkClipPath();
	QString output;
	QString outdir;
	QFont appFont;
private:
	bool clipApplied;
	bool matrixApplied;
//	bool isIn(const QPointF&);
};

class SVGDevice : public QPaintDevice
{

public:
	SVGDevice(QSize, QString dir="");
	~SVGDevice();
	virtual QPaintEngine * paintEngine () const;
	QString result() {return engine->output;};
protected:
	virtual int metric ( PaintDeviceMetric ) const;
private:
	QSize size;
	double xppt;
	double yppt; ///resolution in dots per mm
	SVGEngine* engine;
};


class PSEngine : public QPaintEngine
{
public:
	PSEngine();
	~PSEngine();
	virtual bool begin(QPaintDevice*);
	virtual bool end();
	virtual void updateState( const QPaintEngineState & ) {};
	virtual void drawPixmap(const QRectF&, const QPixmap&, const QRectF&) {};
	virtual QPaintEngine::Type type() const {return QPaintEngine::User;};
	virtual void drawLines ( const QLineF * , int );
	virtual void drawPolygon ( const QPointF * , int , PolygonDrawMode );
	virtual void drawTextItem ( const QPointF & , const QTextItem & );
	virtual void drawRects ( const QRectF * , int );
	QString output;
	QFont appFont;
private:
	bool isIn(const QPointF&);
};

class PSDevice : public QPaintDevice
{

public:
	PSDevice(QSize);
	~PSDevice();
	virtual QPaintEngine * paintEngine () const;
	QString result() {return engine->output;};
protected:
	virtual int metric ( PaintDeviceMetric ) const;
private:
	QSize size;
	double xppt;
	double yppt; ///resolution in dots per mm
	PSEngine* engine;
};

#ifdef USE_EMF_OUTPUT
class EMFEngine : public QPaintEngine
{
public:
	EMFEngine(QString);
	~EMFEngine();
	virtual bool begin(QPaintDevice*);
	virtual bool end();
	virtual void updateState( const QPaintEngineState & ) {};
	virtual void drawImage(const QRectF& r, const QImage& im, const QRectF& sr) {drawPixmap(r,QPixmap::fromImage(im),sr);};
	virtual QPaintEngine::Type type() const {return QPaintEngine::User;};
	virtual void drawLines ( const QLineF * , int );
	virtual void drawPolygon ( const QPointF * , int , PolygonDrawMode );
	virtual void drawTextItem ( const QPointF & , const QTextItem & );
	virtual void drawRects ( const QRectF * , int );
	virtual void drawPixmap ( const QRectF &, const QPixmap &, const QRectF &);
private:
	QString filename;
	HDC metaDC;
	RECT size;
	void setClipping();
	void resetClipping();
};

class EMFDevice : public QPaintDevice
{
public:
	EMFDevice(QSize, QString);
	~EMFDevice();
	virtual QPaintEngine * paintEngine () const;
protected:
	virtual int metric ( PaintDeviceMetric ) const;
private:
	QSize size;
	double xppt;
	double yppt; ///resolution in dots per mm
	EMFEngine* engine;
};

#endif //EMF output
#endif
