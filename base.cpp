#include <math.h>
#include <iostream>
#include "base.h"
#include <QRegExp>
//#include "spinsightio.cc"
#include <QMessageBox>
#include <QSettings>
#include <QStyle>
#include <QFileDialog>
#include <QDebug>
#include "filefilters.h"


void global_options_::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("Options");
	ticks=settings.value("ticks",5).toInt();
	hasXaxis=settings.value("xaxis",true).toBool();
	hasYaxis=settings.value("yaxis",true).toBool();
	hasExtraTicks=settings.value("hasExtraTicks",true).toBool();
	extraTicks=settings.value("extraTicks",5).toInt();
	xtimeunits=settings.value("xTimeUnits",UNITS_US).toInt();
	xfrequnits=settings.value("xFrequencyUnits",UNITS_PPM).toInt();
	ytimeunits=settings.value("yTimeUnits",UNITS_US).toInt();
	yfrequnits=settings.value("yFrequencyUnits",UNITS_PPM).toInt();
	axisFont=settings.value("axisFont",QString("")).toString();
	antialiasing=settings.value("Antialiasing",false).toBool();
	customPrintLogo=settings.value("CustomPrintLogo","").toString();
	printPars=settings.value("PrintPars",true).toBool();
	printLineWidth=settings.value("PrintLineWidth",1).toInt();
	smoothTransformations=settings.value("SmoothTransformations",true).toBool();
	animatedMarkers=settings.value("AnimatedMarkers",false).toBool();
	style=settings.value("Style","").toString();
	useBold1D=settings.value("UseBold1D",true).toBool();
	useSkyline=settings.value("UseSkyline",false).toBool();
	mixFidSpec=settings.value("MixFIDandSpectra",false).toBool();
	useSystemPrintPars=settings.value("UseSystemPrintPars",true).toBool();
        print1Dmode=settings.value("Print1DMode",0).toInt();
	useHypercomplex=settings.value("UseHypercomplex",false).toBool();
	yLeftLabels=settings.value("YLeftLabels",false).toBool();
	QStringList defaultToolBar;
	defaultToolBar<<"DC Offset"<<"Resize (dir)"<<"LB (dir)"<<"FT/FFT (dir)"<<"Phase (dir)";
	procToolBarList=settings.value("ProcessingToolBar",defaultToolBar).toStringList();
        settings.endGroup();
}

void global_options_::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("Options");
        settings.setValue("ticks", int(ticks));
	settings.setValue("xaxis", hasXaxis);
	settings.setValue("yaxis", hasYaxis);
	settings.setValue("hasExtraTicks", hasExtraTicks);
	settings.setValue("extraTicks", int(extraTicks));
	settings.setValue("xTimeUnits",xtimeunits);
	settings.setValue("xFrequencyUnits",xfrequnits);
	settings.setValue("yTimeUnits",ytimeunits);
	settings.setValue("yFrequencyUnits",yfrequnits);
	settings.setValue("axisFont",axisFont);
	settings.setValue("Antialiasing",antialiasing);
	settings.setValue("CustomPrintLogo",customPrintLogo);
	settings.setValue("PrintPars",printPars);
	settings.setValue("PrintLineWidth",printLineWidth);
	settings.setValue("SmoothTransformations",smoothTransformations);
	settings.setValue("AnimatedMarkers",animatedMarkers);
	settings.setValue("Style",style);
	settings.setValue("UseBold1D",useBold1D);
	settings.setValue("UseSkyline",useSkyline);
	settings.setValue("MixFIDandSpectra",mixFidSpec);
	settings.setValue("UseSystemPrintPars",useSystemPrintPars);
        settings.setValue("Print1DMode",print1Dmode);
	settings.setValue("UseHypercomplex",useHypercomplex);
	settings.setValue("ProcessingToolBar",procToolBarList);
	settings.setValue("YLeftLabels",yLeftLabels);
        settings.endGroup();
}

Array_::Array_(QString s)
{
	name=s;
}

int zToInt(QString s, bool* ok)
{
	int mult=1;
	if (s.endsWith("k") || s.endsWith("K")) {
		s.chop(1);
	 	mult=1024;
	}
	return s.toInt(ok)*mult;
}


double convertUnits(double oldval, int oldunits, int newunits, const gsimFD* info, int np, bool directd, bool deltaonly)
{
	double newval=0.0;
	double sw, ref, sfrq;
	if (directd) {
		sw=info->sw;
		ref=info->ref;
		sfrq=info->sfrq;
	}
	else {
		if (np<2) //for 1D data the vertical units are not specified
			return oldval;
		sw=info->sw1;
		ref=info->ref1;
		sfrq=info->sfrq1;
	}
	
	if (unitsType(oldunits)!=unitsType(newunits)) {
		qDebug()<<"Cannot convert";
		return oldval;//cannot convert
		}
	if (unitsType(oldunits)==UNITSTYPE_TIME) { //convert to seconds
		switch (oldunits) {
			case UNITS_S:
				oldval=oldval;
				break;
			case UNITS_MS:
				oldval=oldval/1000.0;
				break;
			case UNITS_US:
				oldval=oldval/1.0e6;
				break;
			case UNITS_TPTS:
				if (deltaonly)
					oldval=oldval/sw;
				else
					oldval=(oldval-1.0)/sw;
				break;
			case UNITS_TVIRT:
				oldval=oldval*np/sw;
				break;
			default:
				oldval=oldval;
				break;
		}
		switch (newunits) {
			case UNITS_S:
				newval=oldval;
				break;
			case UNITS_MS:
				newval=oldval*1000.0;
				break;
			case UNITS_US:
				newval=oldval*1.0e6;
				break;
			case UNITS_TPTS:
				if (deltaonly)
					newval=oldval*sw;
				else
					newval=oldval*sw+1.0;
				break;
			case UNITS_TVIRT:
				newval=oldval*sw/np;
				break;
			default:
				newval=oldval;
				break;
			}
	}
	else { //frequency units, convert to HZ
		switch (oldunits) {
			case UNITS_HZ:
				oldval=oldval;
				break;
			case UNITS_KHZ:
				oldval=oldval*1000;
				break;
			case UNITS_PPM:
				oldval=oldval*(sfrq+ref/1e6);
				break;
			case UNITS_FPTS:
				if (deltaonly)
					oldval=oldval*sw/np;
				else
					if (directd)
                                                oldval=(oldval-1.0)*sw/np-sw/2+ref;
					else
                                                oldval=(np-oldval-1.0)*sw/np-sw/2+ref;
				break;
			default:
				oldval=oldval;
				break;
			}
		switch (newunits) {
			case UNITS_HZ:
				newval=oldval;
				break;
			case UNITS_KHZ:
				newval=oldval/1000;
				break;
			case UNITS_PPM:
				newval=oldval/(sfrq+ref/1e6);
				break;
			case UNITS_FPTS:
				if (deltaonly)
					newval=oldval*np/sw;
				else
					if (directd)
						newval=np-((oldval+sw/2-ref)*np/sw+1.0);
					else
						newval=((oldval+sw/2-ref)*np/sw+1.0);
				break;
			default:
				newval=oldval;
				break;
		}
	}
return newval;
}
	
List<double> convertUnits(List<double>& oldval, int oldunits, int newunits, const gsimFD* info, int np, bool directd, bool deltaonly)
{
	size_t len=oldval.size();
	List<double> newval;
	newval.create(len);
	for (size_t i=0; i<len; i++) {
		newval(i)=convertUnits(oldval(i), oldunits, newunits, info, np, directd, deltaonly);
	}
	return newval;
}

int unitsType(int units)
{
	if ( (units==UNITS_S) || (units==UNITS_MS) || (units==UNITS_US) || (units==UNITS_TPTS))
		return UNITSTYPE_TIME;
	else if ( (units==UNITS_PPM ) ||  (units==UNITS_HZ ) || (units==UNITS_KHZ) || (units==UNITS_FPTS))
		return UNITSTYPE_FREQ;
	else
		return UNITSTYPE_IND;
}


void invert(cmatrix& a, bool isRow=true)
{
	complex temp;
	size_t np=a.cols();
	size_t ni=a.rows();
	if (isRow) {
		size_t half=np/2;
		for (size_t k=ni; k--;) 
		for (size_t i=half; i--;){
			::std::swap(a(k,i),a(k,np-1-i));
		}
	}
	else {
		size_t half=ni/2;
		for (size_t k=np; k--;) 
		for (size_t i=half; i--;){
			::std::swap(a(i,k),a(ni-1-i,k));
		}
	}
}

void invert(List<double>& a)
{
        size_t np=a.size();
        size_t half=np/2;
        for (size_t i=half; i--;)
                ::std::swap(a(i),a(np-1-i));
}

void halfshift(List<complex>& spectrum)
{
  const size_t L=spectrum.length();
  if (L & 1)
    throw Failed("halfshift: data size is not a multiple of 2");
  const size_t halfL=L/2;
  for (size_t i=halfL;i--;)
    ::std::swap(spectrum(i),spectrum(i+halfL));
}

void halfshift(cmatrix& spectrum)
{
  const size_t L=spectrum.cols();
  if (L & 1)
    throw Failed("halfshift: data size is not a multiple of 2");
  const size_t halfL=L/2;
  for (size_t j=spectrum.rows(); j--;)
  for (size_t i=halfL;i--;)
    ::std::swap(spectrum(j,i),spectrum(j,i+halfL));
}


DataStore::DataStore(QObject* parent) : QObject(parent)
{
	active_curve=0;
}

DataStore::~DataStore()
{
	data.clear();
}


QString DataStore::get_short_filename(size_t i)
{
	if (data.at(i).info.file_filter)
		return data.at(i).info.file_filter->short_filename(data.at(i).info.filename);
	else //artificial line doesn't have an associated file filter
		return data.at(i).info.filename;
}

void DataStore::anyFileOpen(QString& s, BaseFileFilter* filter, int flags)
{
    mutex.lock();
    QList<data_entry> newdata;
    try {
    	filter->read(newdata, s);
    }
    catch (MatrixException exc) {
	mutex.unlock();
	throw Failed(exc.what());
    }	
    foreach (data_entry dat, newdata) {
	dat.info.filename=s;
	dat.info.file_filter=filter;
	dat.info.is_on_disk=true;
	dat.info.processing.clear();
	if (dat.spectrum.cols()<=1) {
	if (dat.spectrum.rows()>1) {
		cmatrix tempm=transpose(dat.spectrum);
		swap(tempm,dat.spectrum);
	}
	else{
 		QMessageBox::critical(0, "Error", "File contains just 1 point and cannot be displayed");
		return;}
	}
	if (flags&OpenAppend)
		add_entry(dat);
	else
		set_entry(active_curve,dat); //what about reload?
	size_t k=data.size()-1;
        updateAxisVectors();
	show_real(k);
	}
if (size())
	emit dataEmptified(false);
mutex.unlock();
return;
}

int DataStore::anyFileSave(size_t k, QString& s, BaseFileFilter* filter, int flags)
{
	mutex.lock();
	QList<data_entry> list;
	list.push_back(*get_entry(k));
	try {
		filter->write(list,s);
	}
	catch (MatrixException exc) {
		mutex.unlock();
		throw Failed(exc.what());
	}
	if (flags & SaveUpdateFileName) {
		data[k].info.filename=s;
		data[k].info.is_on_disk=true;
		data[k].info.file_filter=filter;
	}
	mutex.unlock();
	return 0;
}

void DataStore::add_integral(size_t i, List<double>& xint, List<double>& yint)
{
	mutex.lock();
	data[i].integrals.xintegrals.append(xint);
	data[i].integrals.yintegrals.append(yint);
	mutex.unlock();
}

void DataStore::takeRow(size_t k, size_t r)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t len=data.at(d2d).spectrum.cols();
    data_entry newdata;
    size_t d1d=data.size();//index of 1d data
    data.append(newdata);
    List<double> xvec;
    data[d1d].info.filename=QString("Row%1@").arg(r+1)+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw;
    data[d1d].info.ref=data.at(d2d).info.ref;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq;
    data[d1d].info.type=data.at(d2d).info.type;
    data[d1d].info.phd0=data.at(d2d).info.phd0;
    data[d1d].info.phd1=data.at(d2d).info.phd1;
    data[d1d].spectrum.create(1,len,0.0);
    for (size_t i=0; i<len; i++) {
        data[d1d].spectrum(0,i)=get_odata(d2d,r,i);
	xvec.push_back(get_x(d2d,i));
	if (hasHyperData(d2d)) {
//		cout<<"Taken row should appreciate hyperdata\n";
		//data[d1d].spectrum(0,i).im=real(get_hdata(d2d,r,i));
		imag(data[d1d].spectrum(0,i),real(get_hdata(d2d,r,i)));
		}
    }
    set_x(d1d, xvec);
    show_real(d1d);
    data[d1d].info.is_on_disk=false;
    active_curve=data.size()-1;
    data[d1d].parent.hasParent=true;
    data[d1d].parent.isRow=true;
    data[d1d].parent.parentIndex=d2d;
    data[d1d].parent.sliceIndex=r;
    mutex.unlock();
}

void DataStore::takeCol(size_t k, size_t c)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t len=data.at(d2d).spectrum.rows(); //length of the slice
    data_entry newdata;
    size_t d1d=data.size();//index of 1d data
    data.append(newdata);
    List<double> xvec;
    data[d1d].info.filename=QString("Col%1@").arg(c+1)+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw1;
    data[d1d].info.ref=data.at(d2d).info.ref1;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq1;
    data[d1d].info.type=data.at(d2d).info.type1;
    data[d1d].info.phd0=data.at(d2d).info.phi0;
    data[d1d].info.phd1=data.at(d2d).info.phi1;
    data[d1d].spectrum.create(1,len,0.0);
    for (size_t i=0; i<len; i++) {
        data[d1d].spectrum(0,i)=get_odata(d2d,i,c);
        xvec.push_back(get_y(d2d,i));
    }
    show_real(d1d);
    set_x(d1d,xvec);
    data[d1d].info.is_on_disk=false;
    active_curve=data.size()-1;
    data[d1d].parent.hasParent=true;
    data[d1d].parent.isRow=false;
    data[d1d].parent.parentIndex=d2d;
    data[d1d].parent.sliceIndex=c;
    mutex.unlock();
}

void DataStore::takeHorProjection(size_t k)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t len=data.at(d2d).spectrum.cols();
    size_t len2=data.at(d2d).spectrum.rows();
    data_entry newdata;
    size_t d1d=data.size();//index of 1d data
    data.push_back(newdata);
    List<double> xvec;
    data[d1d].info.filename=QString("HorProj@")+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw;
    data[d1d].info.ref=data.at(d2d).info.ref;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq;
    data[d1d].info.type=data.at(d2d).info.type;
    data[d1d].info.phd0=data.at(d2d).info.phd0;
    data[d1d].info.phd1=data.at(d2d).info.phd1;
    data[d1d].spectrum.create(1,len,0.0);
    for (size_t i=0; i<len; i++) {
	for (size_t j=0; j<len2; j++) {
	        data[d1d].spectrum(0,i)+=get_odata(d2d,j,i);
	}
     show_real(d1d);
        xvec.push_back(get_x(d2d,i));
    }
    set_x(d1d, xvec);
    data[d1d].info.is_on_disk=false;
    active_curve=data.size()-1;
    mutex.unlock();
}

void DataStore::takeHorPosSky(size_t k)
{
    mutex.lock();
  //cout<<"takeHorPosSky\n";
    size_t d2d=k; //index of 2d data
    size_t len=data.at(d2d).spectrum.cols();
    size_t len2=data.at(d2d).spectrum.rows();
    data_entry newdata;
    size_t d1d=data.size();//index of 1d data
    data.push_back(newdata);
    List<double> xvec;
    data[d1d].info.filename=QString("HorPosSky@")+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw;
    data[d1d].info.ref=data.at(d2d).info.ref;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq;
    data[d1d].info.type=data.at(d2d).info.type;
    data[d1d].info.phd0=data.at(d2d).info.phd0;
    data[d1d].info.phd1=data.at(d2d).info.phd1;
    data[d1d].spectrum.create(1,len,0.0);
    for (size_t i=0; i<len; i++) {
	data[d1d].spectrum(0,i)=-1e36;
	for (size_t j=0; j<len2; j++) {
		if (real(get_odata(d2d,j,i))>real(data[d1d].spectrum(0,i)))
	        	data[d1d].spectrum(0,i)=get_odata(d2d,j,i);
	}
     show_real(d1d);
        xvec.push_back(get_x(d2d,i));
    }
    set_x(d1d, xvec);
    data[d1d].info.is_on_disk=false;
    active_curve=data.size()-1;
    mutex.unlock();
}

void DataStore::takeHorNegSky(size_t k)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t len=data.at(d2d).spectrum.cols();
    size_t len2=data.at(d2d).spectrum.rows();
    data_entry newdata;
    size_t d1d=data.size();//index of 1d data
    data.push_back(newdata);
    List<double> xvec;
    data[d1d].info.filename=QString("HorNegSky@")+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw;
    data[d1d].info.ref=data.at(d2d).info.ref;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq;
    data[d1d].info.type=data.at(d2d).info.type;
    data[d1d].info.phd0=data.at(d2d).info.phd0;
    data[d1d].info.phd1=data.at(d2d).info.phd1;
    data[d1d].spectrum.create(1,len,0.0);
    for (size_t i=0; i<len; i++) {
	data[d1d].spectrum(0,i)=1e36;
	for (size_t j=0; j<len2; j++) {
		if (real(get_odata(d2d,j,i))<real(data[d1d].spectrum(0,i)))
	        	data[d1d].spectrum(0,i)=get_odata(d2d,j,i);
	}
     show_real(d1d);
        xvec.push_back(get_x(d2d,i));
    }
    set_x(d1d, xvec);
    data[d1d].info.is_on_disk=false;
    active_curve=data.size()-1;
    mutex.unlock();
}

void DataStore::takeVerProjection(size_t k)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t ni=data.at(d2d).spectrum.rows();
    size_t np=data.at(d2d).spectrum.cols();
    data_entry newdata;
    List<double> xvec;
    size_t d1d=data.size();//index of 1d data
    data.append(newdata);
    data[d1d].info.filename=QString("VerProj@")+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw1;
    data[d1d].info.ref=data.at(d2d).info.ref1;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq1;
    data[d1d].info.type=data.at(d2d).info.type1;
    data[d1d].info.phd0=data.at(d2d).info.phi0;
    data[d1d].info.phd1=data.at(d2d).info.phi1;
    data[d1d].spectrum.create(1,ni,0.0);
    for (size_t i=0; i<ni; i++) {
	xvec.push_back(get_y(d2d,i));
	for (size_t j=0; j<np; j++) 
	        data[d1d].spectrum(0,i)+=get_odata(d2d,i,j);
    }
    show_real(d1d);
    data[d1d].info.is_on_disk=false;
    set_x(d1d, xvec);
    active_curve=data.size()-1;
    mutex.unlock();
}

void DataStore::takeVerPosSky(size_t k)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t ni=data.at(d2d).spectrum.rows();
    size_t np=data.at(d2d).spectrum.cols();
    data_entry newdata;
    List<double> xvec;
    size_t d1d=data.size();//index of 1d data
    data.append(newdata);
    data[d1d].info.filename=QString("VerPosSky@")+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw1;
    data[d1d].info.ref=data.at(d2d).info.ref1;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq1;
    data[d1d].info.type=data.at(d2d).info.type1;
    data[d1d].info.phd0=data.at(d2d).info.phi0;
    data[d1d].info.phd1=data.at(d2d).info.phi1;
    data[d1d].spectrum.create(1,ni,0.0);
    for (size_t i=0; i<ni; i++) {
	xvec.push_back(get_y(d2d,i));
	data[d1d].spectrum(0,i)=-1e38;
	for (size_t j=0; j<np; j++)
		if (real(get_odata(d2d,i,j))>real(data[d1d].spectrum(0,i)))
	        	data[d1d].spectrum(0,i)=get_odata(d2d,i,j);
    }
    show_real(d1d);
    data[d1d].info.is_on_disk=false;
    set_x(d1d, xvec);
    active_curve=data.size()-1;
    mutex.unlock();
}

void DataStore::takeVerNegSky(size_t k)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t ni=data.at(d2d).spectrum.rows();
    size_t np=data.at(d2d).spectrum.cols();
    data_entry newdata;
    List<double> xvec;
    size_t d1d=data.size();//index of 1d data
    data.append(newdata);
    data[d1d].info.filename=QString("VerNegSky@")+get_short_filename(d2d);
    data[d1d].info.sw=data.at(d2d).info.sw1;
    data[d1d].info.ref=data.at(d2d).info.ref1;
    data[d1d].info.sfrq=data.at(d2d).info.sfrq1;
    data[d1d].info.type=data.at(d2d).info.type1;
    data[d1d].info.phd0=data.at(d2d).info.phi0;
    data[d1d].info.phd1=data.at(d2d).info.phi1;
    data[d1d].spectrum.create(1,ni,0.0);
    for (size_t i=0; i<ni; i++) {
	xvec.push_back(get_y(d2d,i));
	data[d1d].spectrum(0,i)=1e38;
	for (size_t j=0; j<np; j++)
		if (real(get_odata(d2d,i,j))<real(data[d1d].spectrum(0,i)))
	        	data[d1d].spectrum(0,i)=get_odata(d2d,i,j);
    }
    show_real(d1d);
    data[d1d].info.is_on_disk=false;
    set_x(d1d, xvec);
    active_curve=data.size()-1;
    mutex.unlock();
}

void DataStore::takeHorTrain(size_t k)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t len=data.at(d2d).spectrum.rows()*data.at(d2d).spectrum.cols();
    data_entry newdata;
    List<double> xvec;
    newdata.info.filename=QString("HorTrain@")+get_short_filename(d2d);


//   horizontal train have simplified x-axis and always appears as FID
    newdata.info.sw=data.at(d2d).spectrum.cols();
    newdata.info.type=FD_TYPE_FID;
    newdata.spectrum.create(1,len,0.0);
    for (size_t i=get_odata(d2d)->rows(); i--;)
    for (size_t j=get_odata(d2d)->cols(); j--;) {
    	newdata.spectrum(0,j+i*get_odata(d2d)->cols())=get_odata(d2d,i,j);
    }
    newdata.info.is_on_disk=false;

//attach text labels and separating lines
   QList<Array_> arrays=get_arrays(d2d);
   QList<Array_> apts;
   foreach(Array_ arr, arrays)
   	if (arr.data.size()==data.at(d2d).spectrum.rows())
		apts.push_back(arr);
   QList<graph_object> graphics=get_graphics(d2d);
   for (size_t i=0; i<data.at(d2d).spectrum.rows(); i++) {
	graph_object g;
	g.type=GRAPHTEXT;
	g.angle=270; //text rotated by 270 degree
	QString text;
	foreach (Array_ a, apts)
		text+=QString("%1=%2 ").arg(a.name).arg(a.data(i));
	g.maindata=text;
	double x=double(i);
	if (global_options.xtimeunits==UNITS_MS)
		x*=1e3;
	else if (global_options.xtimeunits==UNITS_US)
		x*=1e6;
	double y=max(real(newdata.spectrum))*1.1;
	g.pos=QPointF(x,y);
	graphics.push_back(g);
	graph_object l;
	l.type=GRAPHLINE;
	l.pos=QLineF(QPointF(x,max(real(newdata.spectrum))),QPointF(x,min(real(newdata.spectrum))));
	graphics.push_back(l);
   }
    newdata.graphics=graphics;

    data.append(newdata);
//    set_x(data.size()-1, xvec);
    active_curve=data.size()-1;
    updateAxisVectors();
    mutex.unlock();
}

void DataStore::takeStack(size_t k)
{
    mutex.lock();
    size_t d2d=k; //index of 2d data
    size_t len=data.at(d2d).spectrum.cols();
    size_t nrow=data.at(d2d).spectrum.rows();
    if (!nrow) nrow=1;
    double ystep=max(real(data[d2d].spectrum))*0.49/nrow;
    double xstep=(max(get_x(d2d))-min(get_x(d2d)))*0.49/nrow;
    if (data.at(d2d).info.type==FD_TYPE_SPE)
	xstep*=-1.0;

    for(size_t k=nrow; k--;)
{
    data_entry* newdata = new data_entry;
    List<double> xvec;
    if (!newdata) cout<<"Can't allocate more meemory!\n";
    newdata->info.filename=QString("Row%1@").arg(k+1)+get_short_filename(d2d);
    newdata->info.sw=data.at(d2d).info.sw;
    newdata->info.ref=data.at(d2d).info.ref;
    newdata->info.sfrq=data.at(d2d).info.sfrq;
    newdata->info.type=data.at(d2d).info.type;
    newdata->info.phd0=data.at(d2d).info.phd0;
    newdata->info.phd1=data.at(d2d).info.phd1;
    newdata->spectrum.create(1,len);
    for (size_t i=0; i<len; i++) {
    	newdata->spectrum(0,i)=get_odata(d2d,k,i);
	xvec.push_back(get_x(d2d,i));
    }
    newdata->display.vshift=k*ystep;
    newdata->display.xshift=k*xstep;
    newdata->info.is_on_disk=false;
    data.append(*newdata);
    set_x(data.size()-1, xvec);
    delete newdata;
}
    active_curve=data.size()-1;
    mutex.unlock();
}


void DataStore::delete_curve(int i)
{
	if (i>=data.size()) {
		cerr<<"Cannot delete evolution\n";
		return;
	}
 mutex.lock();
	data.removeAt(i);
	if (data.size()) 
           active_curve=data.size()-1;
	else active_curve=0;
mutex.unlock();
if (!data.size())
	emit dataEmptified(true);
}

void DataStore::resize(size_t k, size_t ver, size_t hor)
{
//     mutex.lock(); //mutex here can interfere with mutex in processing functions
 	if (ver==0) ver=1;
	if (k>size()) cerr<<"resize::wrong data set\n";
	gsimFD info=*(get_info(k));

	int sizeMb=ver*hor*8/1024/1024;
	if (hasHyperData(k))
		sizeMb*=2;
	if (sizeMb>10) {
		 int ret = QMessageBox::warning(0, tr("Too big size"),
                   QString("Resized data requres about %1Mb to store, which can slow down GSim significantly.\n"
                      "Do you want to proceed?").arg(int(sizeMb)),
                   QMessageBox::Ok | QMessageBox::Abort);
		if (ret==QMessageBox::Abort)
			throw Failed("Aborted by user");
	}

	const size_t oldhor=get_odata(k)->cols();
	size_t oldver=get_odata(k)->rows();
	if (oldver==0) oldver=1;
	if (oldhor!=hor || oldver!=ver) {
	cmatrix newdata(ver,hor,complex(0.0,0.0));
	size_t uptover= (ver>oldver)? oldver : ver;
	size_t uptohor= (hor>oldhor)? oldhor : hor;

	if (hasHyperData(k)){
		cmatrix newhdata(ver,hor,complex(0.0,0.0));
		for (size_t i=0; i<uptohor; i++)
		for (size_t j=0;j<uptover;j++)
			newhdata(j,i)=data[k].hyperspectrum(j,i);
		data[k].hyperspectrum.swap(newhdata);
	}

	for (size_t i=0; i<uptohor; i++)
	for (size_t j=0;j<uptover;j++)
			newdata(j,i)=data[k].spectrum(j,i);
	data[k].spectrum.swap(newdata);	
	if (info.type==FD_TYPE_SPE)
		info.sw=info.sw*hor/oldhor;
	if (info.type1==FD_TYPE_SPE)
		info.sw1=info.sw*ver/oldver;
	set_info(k, info);
	updateAxisVectors();
	}
}


void DataStore::create_xvector(size_t index, double start, double step, size_t np)
{
List<double> xvec;
for(size_t i=0; i<np; i++)
	xvec.push_back(start+i*step);
set_x(index, xvec);
}

void DataStore::create_yvector(size_t index, double start, double step, size_t np)
{
List<double> yvec;
for(size_t i=0; i<np; i++)
	yvec.push_back(start+i*step);
set_y(index, yvec);
}

void DataStore::updateAxisVectors()
{
	for (size_t k=0; k<size(); k++) {
		List<double> xvec;
		const gsimFD* info=get_info(k);
		const size_t np=get_odata(k)->cols();
		const size_t ni=get_odata(k)->rows();
		if (info->type==FD_TYPE_FID)
			for (size_t i=0; i<np; i++)
				xvec.push_back(convertUnits(i+1,UNITS_TPTS,global_options.xtimeunits,get_info(k),np));
		else {
				if ((info->sfrq<=0.0) && (global_options.xfrequnits==UNITS_PPM)) {
					QMessageBox::critical(0,"Cannot use ppm", QString("The spectrometer frequency for dataset %1\nis not set. Scales will be converted to Hz").arg(get_short_filename(k)));
					global_options.xfrequnits=UNITS_HZ;
					}
                        for (size_t i=0; i<np; i++)
				xvec.push_back(convertUnits(i+1,UNITS_FPTS,global_options.xfrequnits,get_info(k),np));
                        invert(xvec);
		}
		set_x(k,xvec);
		if (ni>1){
			List<double> yvec;
                        if (info->type1==FD_TYPE_FID) {
				for (size_t i=0; i<ni; i++)
                                    yvec.push_back(convertUnits(i+1,UNITS_TPTS,global_options.ytimeunits,get_info(k),ni,false));
                            }
                        else {
					if (global_options.yfrequnits == UNITS_PPM) {
						if (info->sfrq1<=0.0) {
							QMessageBox::critical(0,"Cannot use ppm", "The spectrometer frequency is not seta\nScales will be converted to Hz");
							global_options.yfrequnits=UNITS_HZ;
						}
					}
                                for (size_t i=0; i<ni; i++)
                                    yvec.push_back(convertUnits(i+1,UNITS_FPTS,global_options.yfrequnits,get_info(k),ni,false));
                            }
		set_y(k,yvec);
		}
	}
}


void DataStore::makeBackup()
{
	backup=data;
	emit undoChanged(true); //activate undo button
}

void DataStore::undo()
{
if (backup.size()) { 
	data=backup;
	if (active_curve>data.size()-1)
		active_curve=data.size()-1;
	backup.clear();
	emit undoChanged(false); //disactivate undo function
	emit dataEmptified(false);
}
}

complex DataStore::average_pt(size_t k, int j, int dj, int i, int di)
{
	int count=0;
	complex sum(0.0,0.0);
	for (int p=i-di; p<i+di+1; p++)
	for (int q=j-dj; q<j+dj+1; q++) {
		if ( (p<0) || (p>=data[k].spectrum.rows()))
			continue;
		if ( (q<0) || (q>=data[k].spectrum.cols()))
			continue;
		sum+=data[k].spectrum(p,q);
		count++;
	}
	return sum/count;
}

int DataStore::currentXUnits(size_t k)
{
    if (get_info(k)->type==FD_TYPE_FID)
        return global_options.xtimeunits;
    else
        return global_options.xfrequnits;
}

int DataStore::currentYUnits(size_t k)
{
    if (get_odata(k)->rows()<2)
        return UNITS_NONE;
    else
        if (get_info(k)->type1==FD_TYPE_FID)
            return global_options.ytimeunits;
        else
            return global_options.yfrequnits;
 }


whiteScheme::whiteScheme()
{
	name="white";
	backgroundColour=Qt::white;
	textColour=Qt::black;
	markerColour=Qt::red;
	axisColour=Qt::black;
	watchColour=Qt::yellow;
	rectBorderColour=QColor(0,0,255);
	rectFillColour=QColor(0,0,255,30);
	plot2DColour<<QColor(0,0,102,255)<<QColor(0,102,0,255)<<QColor(102,0,102,255)<<QColor(0,128,128,255)<< QColor(153,0,0,255)<<QColor(214,0,147,255)<<QColor(0,204,0,255)<<QColor(255,102,0,255)<<QColor(255,0,102,255)<< QColor(0,204,0,255)<<QColor(0,128,128,255)<<QColor(51,153,255,255)<<QColor(51,51,0,255)<<QColor(102,102,153,255)<< QColor(255,51,153,255)<<QColor(0,0,255,255);
	plot3DContourColour<<Qt::blue<<Qt::magenta<<Qt::green<<QColor(0,0,102)<< QColor(0,102,0)<<QColor(102,0,102)<<QColor(0,128,128)<<QColor(153,0,0)<<QColor(214,0,147)<<QColor(0,204,0)<< QColor(255,102,0)<<QColor(255,0,102)<<QColor(0,204,0)<<QColor(0,128,128)<<QColor(51,153,255)<<QColor(51,51,0)<< QColor(102,102,153)<<QColor(255,51,153)<<QColor(0,0,255);
	plot3DRasterColour<<Qt::darkRed<<Qt::red<<Qt::yellow<<Qt::darkYellow<<Qt::green<<Qt::darkGreen<<Qt::blue<< Qt::darkBlue<<Qt::cyan<<Qt::darkCyan<<Qt::magenta<<Qt::darkMagenta<<Qt::lightGray<<Qt::gray<<Qt::darkGray<<Qt::darkRed<< Qt::red<<Qt::yellow<<Qt::darkYellow<<Qt::green<<Qt::darkGreen<<Qt::blue<<Qt::darkBlue<<Qt::cyan<<Qt::darkCyan<<Qt::magenta<<Qt::darkMagenta<<Qt::lightGray<<Qt::gray<<Qt::darkGray;
}

blackScheme::blackScheme()
{
	name="black";
	backgroundColour=Qt::black;
	textColour=Qt::white;
	markerColour=Qt::red;
	axisColour=Qt::white;
	watchColour=Qt::darkBlue;
	rectBorderColour=QColor(0,255,255);
	rectFillColour=QColor(0,255,255,30);
	plot2DColour<<Qt::darkYellow<<QColor(0,102,0,255)<<QColor(102,0,102,255)<<QColor(0,128,128,255)<< QColor(153,0,0,255)<<QColor(214,0,147,255)<<QColor(0,204,0,255)<<QColor(255,102,0,255)<<QColor(255,0,102,255)<< QColor(0,204,0,255)<<QColor(0,128,128,255)<<QColor(51,153,255,255)<<QColor(51,51,0,255)<<QColor(102,102,153,255)<< QColor(255,51,153,255)<<QColor(0,0,255,255);
	plot3DContourColour<<Qt::blue<<Qt::magenta<<Qt::green<<QColor(0,0,102)<< QColor(0,102,0)<<QColor(102,0,102)<<QColor(0,128,128)<<QColor(153,0,0)<<QColor(214,0,147)<<QColor(0,204,0)<< QColor(255,102,0)<<QColor(255,0,102)<<QColor(0,204,0)<<QColor(0,128,128)<<QColor(51,153,255)<<QColor(51,51,0)<< QColor(102,102,153)<<QColor(255,51,153)<<QColor(0,0,255);
	plot3DRasterColour<<Qt::darkRed<<Qt::red<<Qt::yellow<<Qt::darkYellow<<Qt::green<<Qt::darkGreen<<Qt::blue<< Qt::darkBlue<<Qt::cyan<<Qt::darkCyan<<Qt::magenta<<Qt::darkMagenta<<Qt::lightGray<<Qt::gray<<Qt::darkGray<<Qt::darkRed<< Qt::red<<Qt::yellow<<Qt::darkYellow<<Qt::green<<Qt::darkGreen<<Qt::blue<<Qt::darkBlue<<Qt::cyan<<Qt::darkCyan<<Qt::magenta<<Qt::darkMagenta<<Qt::lightGray<<Qt::gray<<Qt::darkGray;
}

yellowScheme::yellowScheme()
{
	name="yellow";
	backgroundColour=QColor(255,255,221);
	textColour=Qt::black;
	markerColour=Qt::red;
	axisColour=Qt::black;
	watchColour=QColor(48,255,179);
	rectBorderColour=QColor(0,0,255);
	rectFillColour=QColor(0,0,255,30);
	plot2DColour<<QColor(0,0,102,255)<<QColor(0,102,0,255)<<QColor(102,0,102,255)<<QColor(0,128,128,255)<< QColor(153,0,0,255)<<QColor(214,0,147,255)<<QColor(0,204,0,255)<<QColor(255,102,0,255)<<QColor(255,0,102,255)<< QColor(0,204,0,255)<<QColor(0,128,128,255)<<QColor(51,153,255,255)<<QColor(51,51,0,255)<<QColor(102,102,153,255)<< QColor(255,51,153,255)<<QColor(0,0,255,255);
	plot3DContourColour<<Qt::blue<<Qt::magenta<<Qt::green<<QColor(0,0,102)<< QColor(0,102,0)<<QColor(102,0,102)<<QColor(0,128,128)<<QColor(153,0,0)<<QColor(214,0,147)<<QColor(0,204,0)<< QColor(255,102,0)<<QColor(255,0,102)<<QColor(0,204,0)<<QColor(0,128,128)<<QColor(51,153,255)<<QColor(51,51,0)<< QColor(102,102,153)<<QColor(255,51,153)<<QColor(0,0,255);
	plot3DRasterColour<<Qt::darkRed<<Qt::red<<Qt::yellow<<Qt::darkYellow<<Qt::green<<Qt::darkGreen<<Qt::blue<< Qt::darkBlue<<Qt::cyan<<Qt::darkCyan<<Qt::magenta<<Qt::darkMagenta<<Qt::lightGray<<Qt::gray<<Qt::darkGray<<Qt::darkRed<< Qt::red<<Qt::yellow<<Qt::darkYellow<<Qt::green<<Qt::darkGreen<<Qt::blue<<Qt::darkBlue<<Qt::cyan<<Qt::darkCyan<<Qt::magenta<<Qt::darkMagenta<<Qt::lightGray<<Qt::gray<<Qt::darkGray;
}
